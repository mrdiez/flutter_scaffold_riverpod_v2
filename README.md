# flutter_scaffold_riverpod_v2

A scaffold made with love :)

## PREREQUISITES
- If you have Android & Flutter installed 
Then you can run this app, you just have to add a mock "Configuration":
Please follow steps described in the "ENVIRONMENT CONFIGURATION" chapter
or you could also run the app by running the command: "flutter run --dart-define=MOCK=true"
- Else just install Android & Flutter then follow the chapters: "ANDROID STUDIO SETUP",
"ENVIRONMENT CONFIGURATION" and "PACKAGES & PLUGINS"

## ANDROID STUDIO SETUP

### Install mandatory and useful plugins
- Go to Preferences/Plugins/Marketplace
- And type Flutter in the search bar
- And install the Flutter plugin
- Optional: install Flutter intl, snippets, enhancement suite & asset auto completion

### Enable auto formatting
- Go to Preferences/Languages and frameworks/Flutter
- And select both Format code on save & Organize imports on save

### Increase line length format rule
- Go to Preferences/Editor/Code Style/Dart
- And increase line length to 120

### Hide generated files (OPTIONAL)
- Go to Preferences/Editor/File Types/Ignore files and folders
- And add "*.freezed.dart;*.g.dart;*.gr.dart;"

### Coding rules
- Configure your rules by editing ./analysis_options.yaml
- You can see that they're coming from the plugin flutter_lints
- And they're disabled for generated files
- You can add your own rules or add plugins like "pedantic"


## ENVIRONMENT CONFIGURATION

### Switch endpoints
The Environment class defines endpoints (url) where our app retrieve data from servers but while
developing we don't want to use productions servers in order to use them only when our
app is ready for prod.

These endpoints are managed into ./lib/config/environment.dart

### To use this project (MANDATORY)
You will need to add "MOCK" configuration (check the next point)
OR you can edit ./lib/constants/config/environment.dart and force Mock property to true

### Add a configuration

- Go to Run/Edit Configurations
- Click the + button then choose Flutter
- Into the "Dart entrypoint" field type the path of the ./lib/main.dart file
- Into the "Additional run args" field type some arguments if needed
- For example: "--dart-define=MOCK=true" to activate mocks
- Or "--dart-define=PROD=true" to use PROD endpoints


## PACKAGES & PLUGINS

All external dependencies needed to build and run this app are listed in
the ./pubspec.yaml file
In order to install them, just execute:
- flutter pub get


## CONTINUOUS INTEGRATION

### Version control
Manage your project with your favorite tool: Github, Gitlab, Bitbucket, etc...
Whatever if you use your terminal, your IDE or a dedicated software

### Automated Testing, Build & Deployment
I suggest you to use Codemagic that allow to test, build and publish apps
automatically without effort
Check the guide that I wrote here:
- ./doc/Flutter_Apps_&_Codemagic.pdf


## CODE GENERATION

In order to create immutable objects with functions like
copyWith, toJson, fromJson
And to add dynamic methods and properties for states and events
We need to write a lot of boilerplate code
This is why some code generations plugins are used here

### How to generate code (with freezed)
- Follow instructions here: https://pub.dev/packages/freezed
- Read this to understand why we'll use it: https://developer.school/how-to-use-freezed-with-flutter/
- Configure your generator by editing ./build.yaml
- Increase SDK version into ./pubspec.yaml: sdk: ">=2.13.0 <3.0.0"
- To generate code, execute: flutter pub run build_runner build --delete-conflicting-outputs
- You can also run a listener to generate code on file changes:
- flutter pub run build_runner watch --delete-conflicting-outputs

### Check other generated stuff
Please read below "Translations" and "Assets" chapters


## TRANSLATIONS

### Setup Intl package (for a new project)
- Go to Preferences/Plugins/Marketplace and install Flutter intl package
- Add these lines at the end of ./pubspec.yaml:
  "flutter_intl:
  enabled: false    # Will be set to true after initialization
  class_name: Translation   # The class name to access translations ("S" by default)
  arb_dir: lib/constants/translations/l10n  # The translations folder
  output_dir: lib/constants/translations/generated    # The generated translations folder"
- Go to Tools/Flutter Intl and click "Initialize for the project"
- Add these properties to your ./lib/main.dart MaterialApp widget:
  "localizationsDelegates: [
  Translation.delegate, // Translation is the class name defined at previous step
  GlobalMaterialLocalizations.delegate,
  GlobalWidgetsLocalizations.delegate,
  GlobalCupertinoLocalizations.delegate,
  ],
  supportedLocales: Translation.delegate.supportedLocales,"
- Then follow the plugin setup here: https://plugins.jetbrains.com/plugin/13666-flutter-intl
- Everytime you will edit and save a translation file the translation generator will be triggered


## ASSETS

### Generate dedicated static classes & getters for images, fonts, etc
In order to avoid to use asset path strings directly and to avoid to add them manually
in a dedicated class we'll use: https://pub.dev/packages/flutter_gen
This page also explain how to install it and how to use it.

Run this command to generate assets:
- flutter packages pub run build_runner build

### Setup icon and splashscreen
In order to edit/add the app icon and/or splashscreen you just have to replace icon.png and
splash.png in the assets/launcher folder with your files.

Then run these commands:
- flutter pub run flutter_launcher_icons:main
- flutter pub pub run flutter_native_splash:create


## SOFTWARE ARCHITECTURE

### Once upon a time
Flutter don't have a specific architecture and 4 years ago (2018) best practices did not
exist but fortunately we've seen some packages and plugins coming to solve this problem
Whatever the chosen pattern we can do the same product so the best pattern
is the one where you and your dev team are comfortable
So I started with the pattern MVC that has proven itself and with the Provider
package because it was the first one approved by the Flutter team. Also to reduce
boilerplate code I created my own plugin: mvcprovider

### Lessons learned
By starting new projects I questioned my approach and then I realised that an
MVVM architecture should accelerate my developments by avoiding
manual refreshes of the view from another screen. By coupling this pattern
with immutable states and objects I also don't have to be worried about who is
using my data and I improve performances

### Design Patterns
So I had two choices:
- I implement manually the pattern with Provider or maybe another dependency
  injection package like Mobx or Riverpod
- I follow the public opinion in wonderland and I use the BLOC pattern with
  the BLOC package that already implement this concept for me

### Chosen solution
I choose to implement the BLOC pattern but with Riverpod :D


## FILE STRUCTURE

### Traditional MVVM
In theory ViewModels or "Blocs" can be shared across multiple views this is why
traditional Bloc projects have a layered file structure likes this:
- lib/
    - viewModels
    - models
    - screens
    - ...
      This approach allow us to get our code and logic going into the folder
      corresponding to layers "presentation", "business logic" and "data access"
      Feature are scattered across different layers of the app, hence it could
      be laborious to extract this feature in order to reuse it elsewhere
      When app will grow these folders will overflow and it will be hard to maintain
      or just find a piece of code

### Modular approach
In order to resolve this problem and keep a layered architecture we will
split logic into features and/or modules:
- lib/
    - modules/
        - my_module/
            - logic
            - model (or view's state)
            - ui
        - my_feature/
            - logic
            - model (or view's state)
            - ui
        - ...
    - ...

### Our approach
To keep those things more concise we merged the view's state and the view in one file
and we also merged the logic's events and the logic.
Wait what ? The logic events ? In BLOC events are triggered by the view in order to call
the associated mapped method !
Yes, but it needs boilerplate code to map those events with logic's methods and we found that it's
useless so our view just call directly the logic methods. You will do the same and save a lot of
lines of code.
Also you'll often need to trigger an action from the logic like navigating to a new page or opening
a popup. So we sent an event to the view from the logic because we don't to manage view logic
inside our logic file and we don't want to trigger a useless rebuild by updating the view's state.
- lib/
    - modules/
        - my_module/
            - my_module_logic.dart  // Contains the logic and logic's event classes
            - my_module_view.dart   // Contains the view and view's state classes
        - my_feature/
            - my_feature_logic.dart // Contains the logic and logic's event classes
            - my_feature_view.dart  // Contains the view and view's state classes
        - ...
    - ...

### Project structure

- .dart_tool        // Folder, application dependencies location and version
- .git              // Hidden folder, created by git to manage versioning
- .idea             // Folder, Android Studio parameters dedicated to the project
- android           // Folder, Android application
- assets            // Folder, images, raw data, videos, etc.
- build             // Folder, dependencies and generated applications
- doc               // Folder, all documentation related to this project
- ios               // Folder, iOS application
- lib               // Folder, the application, this is where we develop in .dart
- linux             // Folder, Linux application
- macos             // Folder, MacOS application
- modules           // Folder, project's git submodules
- test              // File, unit tests and widget tests
- test_driver       // Folder, integration tests
- web               // Folder, Web application
- windows           // Folder, Windows application
- .gitignore        // File, content not to be sent to git
- .gitmodules       // File, list of git submodules
- .metadata         // File, info on the version of Flutter, allows the updates
- .packages         // File, application dependencies location and version
- analysis_options.yaml // File, code analyser configuration
- CONTRIBUTE.md     // File, documentation explaining how to contribute
- build.yaml        // File, code generation configuration
- project_name.iml  // File, Android Studio dependencies for this project
- Flutter_01.log    // File, Flutter log file
- pubspec.lock      // File, detailed application dependencies
- pubspec.yaml      // File, editable application dependencies
- README.md         // File, general application documentation

### Lib structure

- abstract          // Folder, interfaces, mixins, etc...
- config            // Folder, environment, routes, assets, etc...
    - config/translations     // Folder, translations an related generated classes
    - config/theme            // Folder, theming, sizing and colors
    - config/assets.gen.dart  // File, generated assets' getters
    - config/environment.dart // File, environment variables, endpoints...
    - config/routes.dart      // File, app's routes
- data              // Folder, dto, repositories, data apis...
- models            // Folder, Domain models
- modules           // Folder, main features of the app
- services          // Folder, isolated and/or common features, methods, use cases...
- utils             // Folder, practical utilities and type extensions
- widgets           // Folder, common UI components
- main.dart         // File, application root widget
