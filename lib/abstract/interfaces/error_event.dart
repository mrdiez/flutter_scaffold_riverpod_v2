/// This class defines a standard for events
/// By using an event which extends this class we are sure that it has a message property
abstract class ErrorEvent {
  String? get message;
}
