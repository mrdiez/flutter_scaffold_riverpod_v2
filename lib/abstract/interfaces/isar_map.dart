import 'dart:collection';
import 'dart:convert';

import 'package:dartx/dartx.dart';
import 'package:isar/isar.dart';
import 'package:flutter_scaffold_riverpod_v2/utils/date.dart';

part 'isar_map.g.dart';

// To generate @Embedded classes (in filename.g.dart) run:
// flutter pub run build_runner build --delete-conflicting-outputs

/// This class is a little hack allowing us to save classes with Map properties in our DB
/// Don't forget to add new types if needed to the methods _toJsonString & _fromJsonString below
@Embedded(inheritance: false)
class IsarMap<T1, T2> with MapMixin<T1, T2> {
  IsarMap();

  IsarMap.fromMap(this._map);
  Map<T1, T2> toMap() => _map;

  @ignore
  Map<T1, T2> _map = {};

  String get json {
    Map<dynamic, dynamic> result = _map;
    result = result.mapKeys((entry) => _toJsonString(entry.key));
    result = result.mapValues((entry) => _toJsonString(entry.value));
    return jsonEncode(result);
  }

  set json(String value) {
    Map<T1, T2> result = jsonDecode(value);
    result = result.mapKeys((entry) => _fromJsonString<T1>(entry.key));
    result = result.mapValues((entry) => _fromJsonString<T2>(entry.value));
    _map = result;
  }

  @override
  T2? operator [](Object? key) => _map[key];

  @override
  void operator []=(T1 key, T2 value) => _map[key] = value;

  @override
  void clear() => _map.clear();

  @ignore
  @override
  Iterable<T1> get keys => _map.keys;

  @override
  T2? remove(Object? key) => _map.remove(key);

  @override
  IsarMap<RK, RV> cast<RK, RV>() {
    Map<dynamic, dynamic> result = _map;
    result = result.mapKeys((entry) => _fromJsonString<RK>(entry.key));
    result = result.mapValues((entry) => _fromJsonString<RV>(entry.value));
    return IsarMap<RK, RV>.fromMap(result.cast<RK, RV>());
  }

  String _toJsonString(dynamic value) {
    switch (value.runtimeType) {
      case DateTime:
        return DateTimeUtils.toUtc(value);
      // TODO implements other types parsing if needed
      default:
        return value.toString();
    }
  }

  T _fromJsonString<T>(dynamic value) {
    switch (T) {
      case DateTime:
        return DateTime.parse(value) as T;
      case int:
        return int.parse(value) as T;
      // TODO implements other types parsing if needed
      default:
        return value.toString() as T;
    }
  }
}
