import 'package:isar/isar.dart';

/// This class defines a standard for classes that we want to save in our DB
/// By using a class which extends this one we are sure that it has a auto incremented id property
abstract class IsarModel {
  Id id = Isar.autoIncrement;
}

/// This class defines a standard for classes that we want to be unique in our DB
/// By using a class which extends this one we are sure that it has a fixed default id
abstract class IsarSingleEntityModel extends IsarModel {
  // Default id (as only 1 entity of this class is supposed to be stored)
  static const defaultId = 0;
  // Here we don't want the Isar Database to generate the id (as only 1 entity of this class is supposed to be stored)
  @override
  Id get id => defaultId;
}
