import 'dart:async';

import 'package:flutter_scaffold_riverpod_v2/data/access/cache_preferences_repository.dart';

/// This class defines a standard for repositories
/// By using a class which implements this one we are sure that it has all the methods that a repository should have
abstract class _Repository<T> {
  Stream<List<T>> watchAll();

  Stream<T?> watch(int id);

  Future<T?> get(int id);

  Future<List<T>> getAll();

  Future<int> insert(T entry);

  Future<List<int>> insertAll(List<T> entries);

  Future<int> update(T entry);

  Future<List<int>> updateAll(List<T> entries);

  Future<bool> delete(int id);

  Future<int> deleteAll(List<int> ids);
}

/// This class defines a standard for global repositories
/// By using a class which extends this one we are sure that it will fetch data though network and/or local DB
abstract class GlobalRepository<T> implements Repository<T> {
  GlobalRepository(this.networkRepository, this.localRepository);

  final Repository<T> networkRepository;
  final Repository<T> localRepository;

  @override
  Stream<List<T>> watchAll() =>
      localRepository.watchAll().asyncMap((entities) async => entities.isEmpty ? await getAll() : entities);

  @override
  Stream<T?> watch(int id) => localRepository.watch(id);

  @override
  Future<T?> get(int id) => localRepository.get(id);

  @override
  Future<List<T>> getAll([bool force = false]) async {
    final localEntities = await localRepository.getAll();
    if (!force && localEntities.isNotEmpty) {
      return localEntities;
    }
    // We retrieve data from the network
    final networkEntities = await networkRepository.getAll();
    // We store data locally
    await localRepository.insertAll(networkEntities);
    // We return locally saved data
    return localRepository.getAll();
  }

  @override
  Future<int> insert(T entity) => localRepository.insert(entity);

  @override
  Future<List<int>> insertAll(List<T> entities) => localRepository.insertAll(entities);

  @override
  Future<int> update(T entity) => localRepository.update(entity);

  @override
  Future<List<int>> updateAll(List<T> entities) => localRepository.updateAll(entities);

  @override
  Future<bool> delete(int id) => localRepository.delete(id);

  @override
  Future<int> deleteAll(List<int> ids) => localRepository.deleteAll(ids);
}

/// This class defines a standard for global repositories with a cache rule
/// By using a class which extends this one we are sure that it will force fetching data though network after cache expiration
abstract class GlobalCachedRepository<T> extends GlobalRepository<T> {
  GlobalCachedRepository(super.networkRepository, super.localRepository, this.cachePreferencesRepository);

  final CachePreferencesRepository cachePreferencesRepository;

  @override
  Stream<List<T>> watchAll() {
    // First we listen for any changes in our locally stored data
    return localRepository.watchAll().asyncMap((entities) async {
      // If this stream has no previous data we'll return locally stored data
      if (entities.isEmpty) return await getAll();
      // If this stream has previous data we'll retrieve cache preferences
      final cachePreferences = await cachePreferencesRepository.get();
      // We check if the defined cache duration is expired or not
      if (cachePreferences.isExpired<T>()) return await getAll(true);
      return entities;
    });
  }

  @override
  Future<List<T>> getAll([bool force = false]) async {
    // First we retrieve cache preferences
    final cachePreferences = await cachePreferencesRepository.get();
    // If the defined cache duration is not expired we'll return locally stored data
    if (!force && !cachePreferences.isExpired<T>()) {
      final localEntities = await localRepository.getAll();
      if (localEntities.isNotEmpty) {
        return localEntities;
      }
    }
    // We retrieve data from the network
    final networkEntities = await networkRepository.getAll();
    // We store data locally
    await localRepository.insertAll(networkEntities);
    // We save the current date time into our cache preferences
    cachePreferences.cacheDatesByType['$T'] = DateTime.now();
    await cachePreferencesRepository.insert(cachePreferences);
    // We return locally saved data
    return localRepository.getAll();
  }
}

/// This class defines a standard for repositories
/// By using a class which extends this one we are sure that it will throw an exception if an unimplemented method is called
abstract class Repository<T> implements _Repository<T> {
  @override
  Stream<List<T>> watchAll() => throw UnimplementedError('[$runtimeType] watchAll()');

  @override
  Stream<T?> watch(int id) => throw UnimplementedError('[$runtimeType] watch()');

  @override
  Future<T?> get(int id) => throw UnimplementedError('[$runtimeType] get()');

  @override
  Future<List<T>> getAll() => throw UnimplementedError('[$runtimeType] getAll()');

  @override
  Future<int> insert(T entry) => throw UnimplementedError('[$runtimeType] insert()');

  @override
  Future<List<int>> insertAll(List<T> entries) => throw UnimplementedError('[$runtimeType] insertAll()');

  @override
  Future<int> update(T entry) => throw UnimplementedError('[$runtimeType] update()');

  @override
  Future<List<int>> updateAll(List<T> entries) => throw UnimplementedError('[$runtimeType] updateAll()');

  @override
  Future<bool> delete(int id) => throw UnimplementedError('[$runtimeType] delete()');

  @override
  Future<int> deleteAll(List<int> ids) => throw UnimplementedError('[$runtimeType] deleteAll()');
}
