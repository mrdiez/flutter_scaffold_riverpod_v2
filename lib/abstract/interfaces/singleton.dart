import 'package:flutter/material.dart';

/// This class defines a standard singletons
/// By using a class which extends this one we are sure that it will be instantiated only once
class Singleton<T> {
  static final instances = <Type, Singleton>{};
  factory Singleton() => (instances[T] ?? (instances[T] = Singleton.constructor())) as Singleton<T>;
  Singleton.constructor() {
    debugPrint('[Singleton] created $runtimeType');
  }
}
