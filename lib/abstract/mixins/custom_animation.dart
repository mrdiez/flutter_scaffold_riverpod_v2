import 'package:flutter/material.dart';
import 'package:flutter_scaffold_riverpod_v2/config/theme/sizes.dart';

/// This mixin simplify the implementation of an animation
/// Use that mixin on a StatefulWidget's state like this:
/// class MyClassState extends State<MyClass> with SingleTickerProviderStateMixin, Animated<MyClass>
mixin CustomAnimationMixin<T extends StatefulWidget> on SingleTickerProviderStateMixin<T> {
  late final CustomAnimationController? _animationController;
  late final Animation<double> animation;

  double get begin => 0;
  double get end => 1;
  Duration get duration => $Duration.normal;
  CustomAnimationController getAnimationControllerInstance() => CustomAnimationController(
        vsync: this,
        duration: duration,
      );

  @override
  void initState() {
    _animationController = getAnimationControllerInstance()..addListener(_setState);
    animation = Tween<double>(begin: begin, end: end).animate(_animationController!);
  }

  @override
  void dispose() {
    _animationController!.removeListener(_setState);
    _animationController!.dispose();
    super.dispose();
  }

  void _setState() => setState(() {});

  TickerFuture toggle() => _animationController!.toggle();
  TickerFuture forward() => _animationController!.forward();
  TickerFuture reverse() => _animationController!.reverse();
}

/// This class is supposed to be used with CustomAnimationMixin
/// It controls the animation itself
class CustomAnimationController extends AnimationController {
  CustomAnimationController({required super.vsync, super.duration});

  bool _reversing = false;

  TickerFuture toggle() {
    if (isCompleted) {
      return reverse();
    } else if (isAnimating) {
      stop();
      if (_reversing) {
        return forward();
      } else {
        return reverse();
      }
    } else {
      return forward();
    }
  }

  @override
  TickerFuture forward({double? from}) {
    _reversing = false;
    return super.forward(from: from);
  }

  @override
  TickerFuture reverse({double? from}) {
    _reversing = true;
    return super.reverse(from: from);
  }
}
