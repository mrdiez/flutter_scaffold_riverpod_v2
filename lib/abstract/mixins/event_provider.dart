import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

/// This mixin simplify the implementation of event based classes
/// Use that mixin on any event class which is supposed to be streamed
/// class MyEvent with EventProvider<MyEvent>
mixin EventProvider<T> {
  dynamic get eventKey => _getDefaultKey<T>();

  /// This method will trigger an event though the stream controller provided with the given type & optionally the given key
  void trigger([dynamic key]) {
    key ??= _streamKeys[T]!.last;
    debugPrint('[${key != T ? '$T|$key' : T}] $this');
    _streamControllers[T]![key]!.add(this);
  }

  /// By default if no key have been provided then the event's type is used as key to store its stream's provider
  static _getDefaultKey<T>() => T;

  /// This method will start listening the event stream provided with the given type & optionally the given key
  static void listen<T>(
    WidgetRef ref, {
    dynamic key,
    required void Function(T) onEvent,
  }) {
    ref.listen(_getStream<T>()(key), (previous, next) => onEvent(next.value as T));
  }

  /// This property maps the event keys by event type
  static final Map<Type, List<dynamic>> _streamKeys = {};

  /// This property maps the event stream controllers by event type
  static final Map<Type, Map<dynamic, StreamController>?> _streamControllers = {};

  /// This property maps the event stream providers by event type
  static final Map<Type, AutoDisposeStreamProviderFamily?> _streamProviders = {};

  /// This property maps the riverpod's event providers by event type
  /// Its role is to instantiate and store the controller by the given type or key
  /// It will also binds event stream controllers which have a key with the default one if it exists
  /// To finish it will call our _onDispose method when the current WidgetRef will be disposed
  static AutoDisposeStreamProviderFamily<T, dynamic> _getStream<T>() {
    return (_streamProviders[T] ??= StreamProvider.autoDispose.family<T, dynamic>((ref, key) {
      _streamKeys[T] ??= [];
      final defaultKey = _getDefaultKey<T>();
      key ??= defaultKey;
      assert(!_streamKeys[T]!.contains(key));
      _streamKeys[T]!.add(key);
      debugPrint('[${key != T ? '$T|$key' : T}] Created event stream');
      _streamControllers[T] ??= {};
      _streamControllers[T]![key] = StreamController<T>.broadcast();
      if (key != defaultKey) {
        _streamControllers[T]![key]!.stream.listen(_streamControllers[T]![defaultKey]?.add);
      }
      ref.onDispose(() => _onDispose<T>(key));
      return _streamControllers[T]![key]!.stream as Stream<T>;
    })) as AutoDisposeStreamProviderFamily<T, dynamic>;
  }

  /// This method is used to automatically close the stream controller and to reset the key associated to a given type
  static _onDispose<T>(dynamic key) {
    debugPrint('[${key != T ? '$T|$key' : T}] Disposed event stream');
    _streamControllers[T]![key]!.close();
    _streamKeys[T]!.remove(key);
  }
}
