import 'dart:io';

import 'package:flutter/foundation.dart';

/// This class defines some environments' constants
class Environment {
  // true when flutter run or build commands are launched with "--dart-define=TEST=true" argument
  static const bool isTest = bool.fromEnvironment('TEST');

  // true when flutter run or build commands are launched with "--dart-define=MOCK=true" argument
  static const bool isMock = bool.fromEnvironment('MOCK');

  // true when "flutter run" or "build" commands are launched with "--dart-define=PROD=true" argument
  static const bool isProd = kReleaseMode || bool.fromEnvironment('PROD');

  static const bool isDev = !isTest && !isMock && !isProd;

  static const bool isWeb = kIsWeb;

  static final bool isMobile = !isWeb && (Platform.isAndroid || Platform.isIOS);

  static const baseUrl = isProd ? 'https://my-amazing-domain-name.com' : 'http://192.168.1.11:5000';

  static Directory? appDirectory;
}
