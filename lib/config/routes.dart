import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:flutter_scaffold_riverpod_v2/config/theme/sizes.dart';
import 'package:flutter_scaffold_riverpod_v2/models/user.dart';
import 'package:flutter_scaffold_riverpod_v2/modules/auth/login/auth_login_view.dart';
import 'package:flutter_scaffold_riverpod_v2/modules/auth/reset_password/views/auth_reset_password_step1_view.dart';
import 'package:flutter_scaffold_riverpod_v2/modules/auth/reset_password/views/auth_reset_password_step2_view.dart';
import 'package:flutter_scaffold_riverpod_v2/modules/auth/reset_password/views/auth_reset_password_step3_view.dart';
import 'package:flutter_scaffold_riverpod_v2/modules/auth/reset_password/views/auth_reset_password_step4_view.dart';
import 'package:flutter_scaffold_riverpod_v2/modules/auth/sign_up/views/auth_sign_up_step1_view.dart';
import 'package:flutter_scaffold_riverpod_v2/modules/auth/sign_up/views/auth_sign_up_step2_view.dart';
import 'package:flutter_scaffold_riverpod_v2/modules/home/home_view.dart';
import 'package:flutter_scaffold_riverpod_v2/modules/settings/settings_view.dart';
import 'package:flutter_scaffold_riverpod_v2/modules/user/details/user_details_view.dart';
import 'package:flutter_scaffold_riverpod_v2/modules/user/edit/user_edit_view.dart';
import 'package:flutter_scaffold_riverpod_v2/modules/user/list/user_list_view.dart';
import 'package:flutter_scaffold_riverpod_v2/widgets/layout/rounded_container.dart';
import 'package:flutter_scaffold_riverpod_v2/widgets/texts/section_title.dart';

/// This class defines all the routes names & associated page/widget builders of our app
class Routes {
  static const _step2 = 'step2';
  static const _step3 = 'step3';
  static const _step4 = 'step4';
  static const _signUp = 'sign_up';
  static const _resetPassword = 'reset_password';
  static const _settings = 'settings';
  static const _userList = 'users';
  static const _details = 'details';
  static const _edit = 'edit';

  static const auth = '/auth';

  static const home = '/';

  static const signUp = '$auth/$_signUp';
  static const signUpStep2 = '$signUp/$_step2';

  static const resetPassword = '$auth/$_resetPassword';
  static const resetPasswordStep2 = '$resetPassword/$_step2';
  static const resetPasswordStep3 = '$resetPassword/$_step3';
  static const resetPasswordStep4 = '$resetPassword/$_step4';

  static const settings = '$home$_settings';

  static const userList = '$home$_userList';
  static String userDetails(int id) => '$home$_userList/$_details/$id';
  static const userCreate = '$home$_userList/$_edit';
  static String userEdit(int id) => '${userDetails(id)}/$_edit';

  static final all = [
    // /auth -> Login page route
    GoRoute(
      path: auth,
      builder: (context, state) => const AuthLoginView(),
      routes: [
        // /sign_up -> Sign up page route
        GoRoute(
          path: _signUp,
          builder: (context, state) => const AuthSignUpStep1View(),
          routes: [
            // /sign_up/step2 -> Sign up step 2 page route
            GoRoute(
              path: _step2,
              builder: (context, state) => const AuthSignUpStep2View(),
            ),
          ],
        ),
        // /reset_password -> Reset password page route
        GoRoute(
          path: _resetPassword,
          builder: (context, state) {
            final extraParams = state.extra as Map<String, dynamic>;
            final email = extraParams['email']?.toString();
            return AuthResetPasswordStep1View(email);
          },
          routes: [
            // /reset_password/step2 -> Reset password step 2 page route
            GoRoute(
              path: _step2,
              builder: (context, state) {
                final extraParams = state.extra as Map<String, dynamic>;
                final methods = extraParams['methods'] as List<String>;
                return AuthResetPasswordStep2View(methods);
              },
            ),
            // /reset_password/step3 -> Reset password step 3 page route
            GoRoute(
              path: _step3,
              builder: (context, state) {
                final extraParams = state.extra as Map<String, dynamic>;
                final method = extraParams['method'] as String;
                return AuthResetPasswordStep3View(method);
              },
            ),
            // /reset_password/step4 -> Reset password step 4 page route
            GoRoute(
              path: _step4,
              builder: (context, state) {
                // final extraParams = state.extra as Map<String, dynamic>;
                // final method = extraParams['method'] as String;
                return const AuthResetPasswordStep4View();
              },
            ),
          ],
        ),
      ],
    ),
    // / -> Home page route
    GoRoute(
      path: home,
      builder: (context, state) => const HomeView(),
      routes: [
        // /settings -> Settings page route
        GoRoute(
          path: _settings,
          builder: (context, state) => const SettingsView(),
        ),
        // /users -> User list page route
        GoRoute(
          path: _userList,
          builder: (context, state) => const UserListView(),
          routes: [
            // /users/details/123 -> User details page route
            GoRoute(
              path: '$_details/:id',
              builder: (context, state) {
                final id = int.tryParse(state.pathParameters['id']!);
                final extraParams = state.extra as Map<String, dynamic>;
                final user = extraParams['user'] as User;
                if (id == null || id != user.id) return _notFoundView(context, state);
                return UserDetailsView(user);
              },
              routes: [
                // /users/details/123/edit -> User edition page route
                GoRoute(
                  path: _edit,
                  builder: (context, state) {
                    final id = int.tryParse(state.pathParameters['id']!);
                    final extraParams = state.extra as Map<String, dynamic>;
                    final user = extraParams['user'] as User;
                    if (id == null || id != user.id) return _notFoundView(context, state);
                    return UserEditView(user);
                  },
                ),
              ],
            ),
            // /users/edit -> User creation page route
            GoRoute(
              path: _edit,
              builder: (context, state) => const UserEditView(null),
            ),
          ],
        ),
      ],
    ),
  ];

  // The error 404 page content
  static MaterialPage notFound(BuildContext context, GoRouterState state) =>
      MaterialPage(child: _notFoundView(context, state));
  static Widget _notFoundView(BuildContext context, GoRouterState state) => Scaffold(
          body: Center(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all($Padding.m),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SectionTitle(text: '404'),
                $Gap.m,
                RoundedContainer(child: AutoSizeText(state.error.toString())),
              ],
            ),
          ),
        ),
      ));
}
