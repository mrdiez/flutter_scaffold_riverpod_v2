part of 'theme.dart';

// ignore_for_file: unused_element

// Specific colors
extension ThemeColors on ColorScheme {
  bool get isDark => brightness == Brightness.dark;

  Color get disabled => isDark ? _greyColor.dark : _greyColor;

  Color get positive => isDark ? _greenColor.dark : _greenColor;
  Color get negative => isDark ? _redColor.dark : _redColor;
  Color get warning => isDark ? _yellowColor.dark : _yellowColor;
  Color get informative => isDark ? _blueColor.dark : _blueColor;

  Color get shimmerBase => isDark ? primary.darker : primary.lighter;
  Color get shimmerHighlight => isDark ? primary.dark : primary.light;

  Color get transparent => _transparentColor;
}

// Main theme colors
const $primaryColorValue = 0xff8888EB;
const $secondaryColorValue = 0xffFCA311;
final _primaryColor = MsMaterialColor($primaryColorValue);
final _secondaryColor = MsMaterialColor($secondaryColorValue);

// Color palette
final _blueColor = MsMaterialColor(0xff8888EB);
final _yellowColor = MsMaterialColor(0xffFCA311);
final _redColor = MsMaterialColor(0xffBF4D69);
final _greenColor = MsMaterialColor(0xffBCCA6B);
final _pinkColor = MsMaterialColor(0xffC289B7);

// 50 shades of grey
const _blackColor = Colors.black;
final _darkColor = MsMaterialColor(0xff414141);
final _darkGreyColor = MsMaterialColor(0xff747474);
final _greyColor = MsMaterialColor(0xff909090);
final _lightGreyColor = MsMaterialColor(0xffA7A7A7);
final _lightColor = MsMaterialColor(0xffD9D9D9);
const _whiteColor = Colors.white;
const _transparentColor = Colors.transparent;

// 50 shades of a given color
extension ColorShades on Color {
  Color get darkest => darken(88);
  Color get darker => darken(77);
  Color get dark => darken(50);
  Color get light => lighten(50);
  Color get lighter => lighten(77);
  Color get lightest => lighten(88);
}
