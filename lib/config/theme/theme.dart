import 'package:flutter/material.dart';
import 'package:flutter_scaffold_riverpod_v2/config/theme/sizes.dart';
import 'package:flutter_scaffold_riverpod_v2/utils/color.dart';
import 'package:ms_material_color/ms_material_color.dart';

part 'colors.dart';

/// This property defines our app default theme
final defaultTheme = getTheme(_primaryColor, _secondaryColor);

/// This method returns a whole app theme based on just 2 colors
ThemeData getTheme(Color $primaryColor, Color $secondaryColor) {
  final primaryColor = MsMaterialColor($primaryColor.value);
  final secondaryColor = MsMaterialColor($secondaryColor.value);

  final colors = ColorScheme.fromSwatch(
    accentColor: secondaryColor,
    backgroundColor: primaryColor.lighter,
    brightness: Brightness.light,
    primarySwatch: primaryColor,
    cardColor: _whiteColor,
    primaryColorDark: primaryColor.dark,
  );

  return ThemeData(
    appBarTheme: const AppBarTheme(backgroundColor: _whiteColor),
    badgeTheme: BadgeThemeData(backgroundColor: secondaryColor),
    brightness: Brightness.light,
    bottomNavigationBarTheme: BottomNavigationBarThemeData(
      backgroundColor: _whiteColor,
      selectedItemColor: secondaryColor,
      unselectedItemColor: primaryColor.darker,
      selectedIconTheme: IconThemeData(color: secondaryColor, size: $Icon.s),
      unselectedIconTheme: IconThemeData(color: primaryColor, size: $Icon.s),
      selectedLabelStyle: const TextStyle(fontSize: $Font.xs, height: 1),
      unselectedLabelStyle: const TextStyle(fontSize: $Font.xs, height: 1),
    ),
    canvasColor: _whiteColor,
    checkboxTheme: CheckboxThemeData(
      fillColor: MaterialStateProperty.resolveWith<Color?>(
        (Set<MaterialState> states) {
          if (states.contains(MaterialState.disabled)) {
            return colors.disabled;
          }
          return primaryColor;
        },
      ),
    ),
    chipTheme: ChipThemeData(
      backgroundColor: primaryColor,
      deleteIconColor: _whiteColor,
    ),
    colorScheme: colors.copyWith(
      error: colors.negative,
    ),
    dialogTheme: DialogTheme(
      backgroundColor: primaryColor.withOpacity(0.95),
    ),
    disabledColor: colors.disabled,
    elevatedButtonTheme: ElevatedButtonThemeData(
        style: ButtonStyle(
      foregroundColor: MaterialStateProperty.resolveWith<Color?>(
        (Set<MaterialState> states) {
          return _whiteColor;
        },
      ),
      backgroundColor: MaterialStateProperty.resolveWith<Color?>(
        (Set<MaterialState> states) {
          if (states.contains(MaterialState.pressed)) {
            return secondaryColor.shade500;
          }
          if (states.contains(MaterialState.disabled)) {
            return colors.disabled;
          }
          return secondaryColor;
        },
      ),
    )),
    floatingActionButtonTheme: FloatingActionButtonThemeData(
      backgroundColor: secondaryColor,
    ),
    iconTheme: IconThemeData(color: primaryColor),
    inputDecorationTheme: InputDecorationTheme(
      isDense: true,
      enabledBorder: UnderlineInputBorder(
        borderSide: BorderSide(color: primaryColor.light, width: 1),
      ),
      focusedBorder: UnderlineInputBorder(
        borderSide: BorderSide(color: primaryColor, width: 1),
      ),
      contentPadding: const EdgeInsets.only(bottom: $Padding.xxxs),
      hintStyle: TextStyle(
        fontSize: $Font.s,
        fontWeight: FontWeight.w400,
        color: _greyColor,
        height: 1,
      ),
      errorStyle: const TextStyle(fontSize: $Font.xs),
    ),
    highlightColor: secondaryColor.withOpacity(0.15),
    navigationBarTheme: NavigationBarThemeData(indicatorColor: _darkGreyColor),
    primarySwatch: primaryColor,
    radioTheme: RadioThemeData(
      fillColor: MaterialStateProperty.resolveWith<Color?>(
        (Set<MaterialState> states) {
          return primaryColor.dark;
        },
      ),
    ),
    shadowColor: _lightGreyColor,
    snackBarTheme: SnackBarThemeData(backgroundColor: primaryColor.lightest),
    scaffoldBackgroundColor: primaryColor.lighter,
    splashColor: primaryColor.withOpacity(0.1),
    textTheme: TextTheme(
      displayLarge: TextStyle(
        fontSize: $Font.xxl,
        fontWeight: FontWeight.w900,
        color: primaryColor.dark,
        height: 1,
      ),
      displayMedium: TextStyle(
        fontSize: $Font.xxl,
        fontWeight: FontWeight.w500,
        color: primaryColor.dark,
        height: 1,
      ),
      displaySmall: TextStyle(
        fontSize: $Font.xxl,
        fontWeight: FontWeight.w300,
        color: primaryColor.dark,
        height: 1,
      ),
      headlineLarge: TextStyle(
        fontSize: $Font.l,
        fontWeight: FontWeight.w700,
        color: primaryColor.darker,
        height: 1,
      ),
      headlineMedium: TextStyle(
        fontSize: $Font.l,
        fontWeight: FontWeight.w500,
        color: primaryColor.darker,
        height: 1,
      ),
      headlineSmall: TextStyle(
        fontSize: $Font.l,
        fontWeight: FontWeight.w300,
        color: primaryColor.darker,
        height: 1,
      ),
      titleLarge: TextStyle(
        fontSize: $Font.m,
        fontWeight: FontWeight.w700,
        color: primaryColor.darker,
        height: 1,
      ),
      titleMedium: TextStyle(
        fontSize: $Font.m,
        fontWeight: FontWeight.w500,
        color: primaryColor.darker,
        height: 1,
      ),
      titleSmall: TextStyle(
        fontSize: $Font.m,
        fontWeight: FontWeight.w300,
        color: primaryColor.darker,
        height: 1,
      ),
      labelLarge: TextStyle(
        fontSize: $Font.s,
        fontWeight: FontWeight.w500,
        color: _greyColor,
        height: 1,
      ),
      labelMedium: TextStyle(
        fontSize: $Font.s,
        fontWeight: FontWeight.w400,
        color: _greyColor,
        height: 1,
      ),
      labelSmall: TextStyle(
        fontSize: $Font.xs,
        fontWeight: FontWeight.w400,
        color: _greyColor,
        height: 1,
      ),
      bodyLarge: TextStyle(
        fontSize: $Font.s,
        color: primaryColor.darker,
        height: 1,
      ),
      bodyMedium: TextStyle(
        fontSize: $Font.s,
        color: primaryColor.dark,
        height: 1,
      ),
      bodySmall: TextStyle(
        fontSize: $Font.xs,
        color: primaryColor.darker,
        height: 1,
      ),
    ),
    useMaterial3: true,
  );
}
