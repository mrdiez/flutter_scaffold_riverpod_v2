// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  static String m0(method) =>
      "Confirm your identity with the code you received by ${method}";

  static String m1(name) => "Your logged as ${name}";

  static String m2(method) => "Your new password have been send by ${method}";

  static String m3(login) => "${login} already exists";

  static String m4(login) => "${login} doesn\'t exist";

  static String m5(nb) => "${nb} characters minimum";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "Admin": MessageLookupByLibrary.simpleMessage("Admin"),
        "Cancel": MessageLookupByLibrary.simpleMessage("Cancel"),
        "ChooseAMethod": MessageLookupByLibrary.simpleMessage(
            "Choose a method to receive the password reset code"),
        "Code": MessageLookupByLibrary.simpleMessage("Code"),
        "Confirm": MessageLookupByLibrary.simpleMessage("Confirm"),
        "ConfirmPassword":
            MessageLookupByLibrary.simpleMessage("Confirm password"),
        "ConfirmYourIdentity": m0,
        "Delete": MessageLookupByLibrary.simpleMessage("Delete"),
        "Edit": MessageLookupByLibrary.simpleMessage("Edit"),
        "Email": MessageLookupByLibrary.simpleMessage("Email"),
        "Firstname": MessageLookupByLibrary.simpleMessage("Firstname"),
        "ForWhichAccount": MessageLookupByLibrary.simpleMessage(
            "For which account do you want to reset the password?"),
        "ForgotPassword":
            MessageLookupByLibrary.simpleMessage("Forgot password?"),
        "Hello": MessageLookupByLibrary.simpleMessage("Hello"),
        "HomePageTitle":
            MessageLookupByLibrary.simpleMessage("Home page title"),
        "IncorrectCredentials":
            MessageLookupByLibrary.simpleMessage("Incorrect credentials"),
        "InvalidEmail": MessageLookupByLibrary.simpleMessage("Invalid Email"),
        "Lastname": MessageLookupByLibrary.simpleMessage("Lastname"),
        "Leave": MessageLookupByLibrary.simpleMessage("Leave"),
        "LogIn": MessageLookupByLibrary.simpleMessage("Log in"),
        "LogOut": MessageLookupByLibrary.simpleMessage("Log out"),
        "MainInfo": MessageLookupByLibrary.simpleMessage("Main info"),
        "NetworkError": MessageLookupByLibrary.simpleMessage(
            "Network error, please check your connectivity"),
        "New": MessageLookupByLibrary.simpleMessage("New"),
        "NotAllowedCharacters": MessageLookupByLibrary.simpleMessage(
            "Some characters are not allowed"),
        "NotValidEmail":
            MessageLookupByLibrary.simpleMessage("This is not a valid email"),
        "OK": MessageLookupByLibrary.simpleMessage("OK"),
        "Password": MessageLookupByLibrary.simpleMessage("Password"),
        "PasswordsMustBeIdentical":
            MessageLookupByLibrary.simpleMessage("Passwords must be identical"),
        "PleaseConfirmYourRegistration": MessageLookupByLibrary.simpleMessage(
            "Please confirm your registration by clicking on the link you received by Email"),
        "Primary": MessageLookupByLibrary.simpleMessage("Primary"),
        "RememberMe": MessageLookupByLibrary.simpleMessage("Remember me"),
        "ResetCodeCantBeSent":
            MessageLookupByLibrary.simpleMessage("Reset code can\'t be sent"),
        "ResetCodeInvalid":
            MessageLookupByLibrary.simpleMessage("Reset code is not valid"),
        "Save": MessageLookupByLibrary.simpleMessage("Save"),
        "Search": MessageLookupByLibrary.simpleMessage("Search"),
        "Secondary": MessageLookupByLibrary.simpleMessage("Secondary"),
        "Settings": MessageLookupByLibrary.simpleMessage("Settings"),
        "SignUp": MessageLookupByLibrary.simpleMessage("Sign up"),
        "Sms": MessageLookupByLibrary.simpleMessage("SMS"),
        "SorryNeedsToBeImplemented": MessageLookupByLibrary.simpleMessage(
            "Sorry but actually nothing happened :s\nbecause this functionality has not really been implemented."),
        "Submit": MessageLookupByLibrary.simpleMessage("Submit"),
        "ThemeColors": MessageLookupByLibrary.simpleMessage("Theme colors"),
        "ThisFieldIsRequired":
            MessageLookupByLibrary.simpleMessage("This field is required"),
        "ThisValueWillBeStoredInGlobalPrefsTable":
            MessageLookupByLibrary.simpleMessage(
                "This value will be stored in the \"GlobalPreferences\" table."),
        "UnknownError":
            MessageLookupByLibrary.simpleMessage("Unknown error, no chance."),
        "User": MessageLookupByLibrary.simpleMessage("User"),
        "Username": MessageLookupByLibrary.simpleMessage("Username"),
        "Users": MessageLookupByLibrary.simpleMessage("Users"),
        "Welcome": MessageLookupByLibrary.simpleMessage("Welcome"),
        "YouWillLooseUnsavedModifications":
            MessageLookupByLibrary.simpleMessage(
                "You will loose all unsaved modifications"),
        "YourLoggedAs": m1,
        "YourNewPasswordHaveBeenSendBy": m2,
        "_____AUTH_ERRORS_____________________________":
            MessageLookupByLibrary.simpleMessage(""),
        "_____AUTH_LOGIN______________________________":
            MessageLookupByLibrary.simpleMessage(""),
        "_____AUTH_RESET_PASSWORD_____________________":
            MessageLookupByLibrary.simpleMessage(""),
        "_____AUTH_SIGN_UP____________________________":
            MessageLookupByLibrary.simpleMessage(""),
        "_____AUTH____________________________________":
            MessageLookupByLibrary.simpleMessage(""),
        "_____COMMON_ERRORS___________________________":
            MessageLookupByLibrary.simpleMessage(""),
        "_____COMMON___________________________________":
            MessageLookupByLibrary.simpleMessage(""),
        "_____HOME____________________________________":
            MessageLookupByLibrary.simpleMessage(""),
        "_____SETTINGS________________________________":
            MessageLookupByLibrary.simpleMessage(""),
        "_____USER____________________________________":
            MessageLookupByLibrary.simpleMessage(""),
        "_locale": MessageLookupByLibrary.simpleMessage("en"),
        "a": MessageLookupByLibrary.simpleMessage("a"),
        "at": MessageLookupByLibrary.simpleMessage("at"),
        "delete": MessageLookupByLibrary.simpleMessage("delete"),
        "edit": MessageLookupByLibrary.simpleMessage("edit"),
        "locale": MessageLookupByLibrary.simpleMessage("en"),
        "loginAlreadyExists": m3,
        "loginDoesNotExist": m4,
        "nbCharactersMinimum": m5,
        "selected": MessageLookupByLibrary.simpleMessage("selected"),
        "selectedPlural": MessageLookupByLibrary.simpleMessage("selected"),
        "user": MessageLookupByLibrary.simpleMessage("user")
      };
}
