// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a fr locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'fr';

  static String m0(method) =>
      "Confirmez votre identité avec le code que vous avez reçu par ${method}";

  static String m1(name) => "Bonjour ${name}";

  static String m2(method) =>
      "Votre nouveau mot de passe vous a été envoyé par ${method}";

  static String m3(login) => "${login} existe déjà";

  static String m4(login) => "${login} n\'existe pas";

  static String m5(nb) => "${nb} caractères minimum";

  static String m6(item) => "Aucun ${item} trouvé";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "Admin": MessageLookupByLibrary.simpleMessage("Administrateur"),
        "Cancel": MessageLookupByLibrary.simpleMessage("Annuler"),
        "ChooseAMethod": MessageLookupByLibrary.simpleMessage(
            "Choisissez une méthode pour recevoir le code de réinitialisation"),
        "Code": MessageLookupByLibrary.simpleMessage("Code"),
        "Confirm": MessageLookupByLibrary.simpleMessage("Confirmer"),
        "ConfirmPassword":
            MessageLookupByLibrary.simpleMessage("Confirmez le mot de passe"),
        "ConfirmYourIdentity": m0,
        "Delete": MessageLookupByLibrary.simpleMessage("Supprimer"),
        "Edit": MessageLookupByLibrary.simpleMessage("Modifier"),
        "Email": MessageLookupByLibrary.simpleMessage("E-mail"),
        "Firstname": MessageLookupByLibrary.simpleMessage("Prénom"),
        "ForWhichAccount": MessageLookupByLibrary.simpleMessage(
            "Pour quel compte voulez-vous réinitialiser le mot de passe ?"),
        "ForgotPassword":
            MessageLookupByLibrary.simpleMessage("Mot de passe oublié ?"),
        "Hello": MessageLookupByLibrary.simpleMessage("Bonjour"),
        "HomePageTitle":
            MessageLookupByLibrary.simpleMessage("Titre de la page d\'accueil"),
        "IncorrectCredentials":
            MessageLookupByLibrary.simpleMessage("Identifiants incorrects"),
        "InvalidEmail": MessageLookupByLibrary.simpleMessage("E-mail invalide"),
        "Lastname": MessageLookupByLibrary.simpleMessage("Nom"),
        "Leave": MessageLookupByLibrary.simpleMessage("Quitter"),
        "LogIn": MessageLookupByLibrary.simpleMessage("Connexion"),
        "LogOut": MessageLookupByLibrary.simpleMessage("Déconnexion"),
        "MainInfo":
            MessageLookupByLibrary.simpleMessage("Informations principales"),
        "NetworkError": MessageLookupByLibrary.simpleMessage(
            "Erreur réseau, merci de vérifier votre connexion."),
        "New": MessageLookupByLibrary.simpleMessage("Nouveau"),
        "NotAllowedCharacters": MessageLookupByLibrary.simpleMessage(
            "Certains caractères ne sont pas autorisés"),
        "NotValidEmail": MessageLookupByLibrary.simpleMessage("Email invalide"),
        "OK": MessageLookupByLibrary.simpleMessage("OK"),
        "Password": MessageLookupByLibrary.simpleMessage("Mot de passe"),
        "PasswordsMustBeIdentical": MessageLookupByLibrary.simpleMessage(
            "Les mots de passe doivent être identiques"),
        "PleaseConfirmYourRegistration": MessageLookupByLibrary.simpleMessage(
            "Merci de confirmer votre inscription en cliquant sur le lien qui vous a été envoyé par e-mail"),
        "Primary": MessageLookupByLibrary.simpleMessage("Primaire"),
        "RememberMe":
            MessageLookupByLibrary.simpleMessage("Se souvenir de moi"),
        "ResetCodeCantBeSent": MessageLookupByLibrary.simpleMessage(
            "Le code de réinitialisation n\'a pas pu être envoyé"),
        "ResetCodeInvalid": MessageLookupByLibrary.simpleMessage(
            "Le code de réinitialisation est invalide"),
        "Save": MessageLookupByLibrary.simpleMessage("Enregistrer"),
        "Search": MessageLookupByLibrary.simpleMessage("Rechercher"),
        "Secondary": MessageLookupByLibrary.simpleMessage("Secondaire"),
        "Settings": MessageLookupByLibrary.simpleMessage("Réglages"),
        "SignUp": MessageLookupByLibrary.simpleMessage("Inscription"),
        "Sms": MessageLookupByLibrary.simpleMessage("SMS"),
        "SorryNeedsToBeImplemented": MessageLookupByLibrary.simpleMessage(
            "Désolé mais en fait il ne s\'est rien passé :s\ncar cette fonctionnalité n\'a pas vraiment été implémentée."),
        "Submit": MessageLookupByLibrary.simpleMessage("Envoyer"),
        "ThemeColors":
            MessageLookupByLibrary.simpleMessage("Couleurs du thème"),
        "ThisFieldIsRequired":
            MessageLookupByLibrary.simpleMessage("Ce champ est requis"),
        "ThisValueWillBeStoredInGlobalPrefsTable":
            MessageLookupByLibrary.simpleMessage(
                "Cette valeur sera enregistrée dans la table \"GlobalPreferences\"."),
        "UnknownError": MessageLookupByLibrary.simpleMessage(
            "Erreur inconnue, pas de chance."),
        "User": MessageLookupByLibrary.simpleMessage("Utilisateur"),
        "Username": MessageLookupByLibrary.simpleMessage("Nom d\'utilisateur"),
        "Users": MessageLookupByLibrary.simpleMessage("Utilisateurs"),
        "Welcome": MessageLookupByLibrary.simpleMessage("Bienvenue"),
        "YouWillLooseUnsavedModifications":
            MessageLookupByLibrary.simpleMessage(
                "Vous allez perdre vos modifications"),
        "YourLoggedAs": m1,
        "YourNewPasswordHaveBeenSendBy": m2,
        "_____AUTH_ERRORS_____________________________":
            MessageLookupByLibrary.simpleMessage(""),
        "_____AUTH_LOGIN______________________________":
            MessageLookupByLibrary.simpleMessage(""),
        "_____AUTH_RESET_PASSWORD_____________________":
            MessageLookupByLibrary.simpleMessage(""),
        "_____AUTH_SIGN_UP____________________________":
            MessageLookupByLibrary.simpleMessage(""),
        "_____AUTH____________________________________":
            MessageLookupByLibrary.simpleMessage(""),
        "_____COMMON_ERRORS___________________________":
            MessageLookupByLibrary.simpleMessage(""),
        "_____COMMON___________________________________":
            MessageLookupByLibrary.simpleMessage(""),
        "_____HOME____________________________________":
            MessageLookupByLibrary.simpleMessage(""),
        "_____SETTINGS________________________________":
            MessageLookupByLibrary.simpleMessage(""),
        "_____USER____________________________________":
            MessageLookupByLibrary.simpleMessage(""),
        "_locale": MessageLookupByLibrary.simpleMessage("fr"),
        "a": MessageLookupByLibrary.simpleMessage("un"),
        "at": MessageLookupByLibrary.simpleMessage("à"),
        "delete": MessageLookupByLibrary.simpleMessage("supprimer"),
        "edit": MessageLookupByLibrary.simpleMessage("modifier"),
        "locale": MessageLookupByLibrary.simpleMessage("fr"),
        "loginAlreadyExists": m3,
        "loginDoesNotExist": m4,
        "nbCharactersMinimum": m5,
        "noItemFound": m6,
        "select": MessageLookupByLibrary.simpleMessage("Sélectionner"),
        "selected": MessageLookupByLibrary.simpleMessage("sélectionné"),
        "selectedPlural": MessageLookupByLibrary.simpleMessage("sélectionnés"),
        "user": MessageLookupByLibrary.simpleMessage("utilisateur")
      };
}
