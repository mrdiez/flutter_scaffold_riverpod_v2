// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values, avoid_escaping_inner_quotes

class Translation {
  Translation();

  static Translation? _current;

  static Translation get current {
    assert(_current != null,
        'No instance of Translation was loaded. Try to initialize the Translation delegate before accessing Translation.current.');
    return _current!;
  }

  static const AppLocalizationDelegate delegate = AppLocalizationDelegate();

  static Future<Translation> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false)
        ? locale.languageCode
        : locale.toString();
    final localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      final instance = Translation();
      Translation._current = instance;

      return instance;
    });
  }

  static Translation of(BuildContext context) {
    final instance = Translation.maybeOf(context);
    assert(instance != null,
        'No instance of Translation present in the widget tree. Did you add Translation.delegate in localizationsDelegates?');
    return instance!;
  }

  static Translation? maybeOf(BuildContext context) {
    return Localizations.of<Translation>(context, Translation);
  }

  /// `fr`
  String get _locale {
    return Intl.message(
      'fr',
      name: '_locale',
      desc: '',
      args: [],
    );
  }

  /// `fr`
  String get locale {
    return Intl.message(
      'fr',
      name: 'locale',
      desc: '',
      args: [],
    );
  }

  /// ``
  String get _____COMMON___________________________________ {
    return Intl.message(
      '',
      name: '_____COMMON___________________________________',
      desc: '',
      args: [],
    );
  }

  /// `Bonjour`
  String get Hello {
    return Intl.message(
      'Bonjour',
      name: 'Hello',
      desc: '',
      args: [],
    );
  }

  /// `Annuler`
  String get Cancel {
    return Intl.message(
      'Annuler',
      name: 'Cancel',
      desc: '',
      args: [],
    );
  }

  /// `Confirmer`
  String get Confirm {
    return Intl.message(
      'Confirmer',
      name: 'Confirm',
      desc: '',
      args: [],
    );
  }

  /// `Envoyer`
  String get Submit {
    return Intl.message(
      'Envoyer',
      name: 'Submit',
      desc: '',
      args: [],
    );
  }

  /// `Quitter`
  String get Leave {
    return Intl.message(
      'Quitter',
      name: 'Leave',
      desc: '',
      args: [],
    );
  }

  /// `Enregistrer`
  String get Save {
    return Intl.message(
      'Enregistrer',
      name: 'Save',
      desc: '',
      args: [],
    );
  }

  /// `Modifier`
  String get Edit {
    return Intl.message(
      'Modifier',
      name: 'Edit',
      desc: '',
      args: [],
    );
  }

  /// `modifier`
  String get edit {
    return Intl.message(
      'modifier',
      name: 'edit',
      desc: '',
      args: [],
    );
  }

  /// `Supprimer`
  String get Delete {
    return Intl.message(
      'Supprimer',
      name: 'Delete',
      desc: '',
      args: [],
    );
  }

  /// `supprimer`
  String get delete {
    return Intl.message(
      'supprimer',
      name: 'delete',
      desc: '',
      args: [],
    );
  }

  /// `OK`
  String get OK {
    return Intl.message(
      'OK',
      name: 'OK',
      desc: '',
      args: [],
    );
  }

  /// `à`
  String get at {
    return Intl.message(
      'à',
      name: 'at',
      desc: '',
      args: [],
    );
  }

  /// `Rechercher`
  String get Search {
    return Intl.message(
      'Rechercher',
      name: 'Search',
      desc: '',
      args: [],
    );
  }

  /// `Sélectionner`
  String get select {
    return Intl.message(
      'Sélectionner',
      name: 'select',
      desc: '',
      args: [],
    );
  }

  /// `sélectionné`
  String get selected {
    return Intl.message(
      'sélectionné',
      name: 'selected',
      desc: '',
      args: [],
    );
  }

  /// `sélectionnés`
  String get selectedPlural {
    return Intl.message(
      'sélectionnés',
      name: 'selectedPlural',
      desc: '',
      args: [],
    );
  }

  /// `Nouveau`
  String get New {
    return Intl.message(
      'Nouveau',
      name: 'New',
      desc: '',
      args: [],
    );
  }

  /// `un`
  String get a {
    return Intl.message(
      'un',
      name: 'a',
      desc: '',
      args: [],
    );
  }

  /// `E-mail`
  String get Email {
    return Intl.message(
      'E-mail',
      name: 'Email',
      desc: '',
      args: [],
    );
  }

  /// ``
  String get _____COMMON_ERRORS___________________________ {
    return Intl.message(
      '',
      name: '_____COMMON_ERRORS___________________________',
      desc: '',
      args: [],
    );
  }

  /// `Aucun {item} trouvé`
  String noItemFound(Object item) {
    return Intl.message(
      'Aucun $item trouvé',
      name: 'noItemFound',
      desc: '',
      args: [item],
    );
  }

  /// `{nb} caractères minimum`
  String nbCharactersMinimum(Object nb) {
    return Intl.message(
      '$nb caractères minimum',
      name: 'nbCharactersMinimum',
      desc: '',
      args: [nb],
    );
  }

  /// `Ce champ est requis`
  String get ThisFieldIsRequired {
    return Intl.message(
      'Ce champ est requis',
      name: 'ThisFieldIsRequired',
      desc: '',
      args: [],
    );
  }

  /// `Certains caractères ne sont pas autorisés`
  String get NotAllowedCharacters {
    return Intl.message(
      'Certains caractères ne sont pas autorisés',
      name: 'NotAllowedCharacters',
      desc: '',
      args: [],
    );
  }

  /// `Désolé mais en fait il ne s'est rien passé :s\ncar cette fonctionnalité n'a pas vraiment été implémentée.`
  String get SorryNeedsToBeImplemented {
    return Intl.message(
      'Désolé mais en fait il ne s\'est rien passé :s\ncar cette fonctionnalité n\'a pas vraiment été implémentée.',
      name: 'SorryNeedsToBeImplemented',
      desc: '',
      args: [],
    );
  }

  /// `Erreur réseau, merci de vérifier votre connexion.`
  String get NetworkError {
    return Intl.message(
      'Erreur réseau, merci de vérifier votre connexion.',
      name: 'NetworkError',
      desc: '',
      args: [],
    );
  }

  /// `Erreur inconnue, pas de chance.`
  String get UnknownError {
    return Intl.message(
      'Erreur inconnue, pas de chance.',
      name: 'UnknownError',
      desc: '',
      args: [],
    );
  }

  /// ``
  String get _____AUTH____________________________________ {
    return Intl.message(
      '',
      name: '_____AUTH____________________________________',
      desc: '',
      args: [],
    );
  }

  /// `Nom d'utilisateur`
  String get Username {
    return Intl.message(
      'Nom d\'utilisateur',
      name: 'Username',
      desc: '',
      args: [],
    );
  }

  /// `Mot de passe`
  String get Password {
    return Intl.message(
      'Mot de passe',
      name: 'Password',
      desc: '',
      args: [],
    );
  }

  /// `Mot de passe oublié ?`
  String get ForgotPassword {
    return Intl.message(
      'Mot de passe oublié ?',
      name: 'ForgotPassword',
      desc: '',
      args: [],
    );
  }

  /// `Inscription`
  String get SignUp {
    return Intl.message(
      'Inscription',
      name: 'SignUp',
      desc: '',
      args: [],
    );
  }

  /// ``
  String get _____AUTH_ERRORS_____________________________ {
    return Intl.message(
      '',
      name: '_____AUTH_ERRORS_____________________________',
      desc: '',
      args: [],
    );
  }

  /// `E-mail invalide`
  String get InvalidEmail {
    return Intl.message(
      'E-mail invalide',
      name: 'InvalidEmail',
      desc: '',
      args: [],
    );
  }

  /// `Les mots de passe doivent être identiques`
  String get PasswordsMustBeIdentical {
    return Intl.message(
      'Les mots de passe doivent être identiques',
      name: 'PasswordsMustBeIdentical',
      desc: '',
      args: [],
    );
  }

  /// `Identifiants incorrects`
  String get IncorrectCredentials {
    return Intl.message(
      'Identifiants incorrects',
      name: 'IncorrectCredentials',
      desc: '',
      args: [],
    );
  }

  /// `{login} n'existe pas`
  String loginDoesNotExist(Object login) {
    return Intl.message(
      '$login n\'existe pas',
      name: 'loginDoesNotExist',
      desc: '',
      args: [login],
    );
  }

  /// `Le code de réinitialisation n'a pas pu être envoyé`
  String get ResetCodeCantBeSent {
    return Intl.message(
      'Le code de réinitialisation n\'a pas pu être envoyé',
      name: 'ResetCodeCantBeSent',
      desc: '',
      args: [],
    );
  }

  /// `Le code de réinitialisation est invalide`
  String get ResetCodeInvalid {
    return Intl.message(
      'Le code de réinitialisation est invalide',
      name: 'ResetCodeInvalid',
      desc: '',
      args: [],
    );
  }

  /// `{login} existe déjà`
  String loginAlreadyExists(Object login) {
    return Intl.message(
      '$login existe déjà',
      name: 'loginAlreadyExists',
      desc: '',
      args: [login],
    );
  }

  /// ``
  String get _____AUTH_LOGIN______________________________ {
    return Intl.message(
      '',
      name: '_____AUTH_LOGIN______________________________',
      desc: '',
      args: [],
    );
  }

  /// `Connexion`
  String get LogIn {
    return Intl.message(
      'Connexion',
      name: 'LogIn',
      desc: '',
      args: [],
    );
  }

  /// `Se souvenir de moi`
  String get RememberMe {
    return Intl.message(
      'Se souvenir de moi',
      name: 'RememberMe',
      desc: '',
      args: [],
    );
  }

  /// ``
  String get _____AUTH_RESET_PASSWORD_____________________ {
    return Intl.message(
      '',
      name: '_____AUTH_RESET_PASSWORD_____________________',
      desc: '',
      args: [],
    );
  }

  /// `Code`
  String get Code {
    return Intl.message(
      'Code',
      name: 'Code',
      desc: '',
      args: [],
    );
  }

  /// `SMS`
  String get Sms {
    return Intl.message(
      'SMS',
      name: 'Sms',
      desc: '',
      args: [],
    );
  }

  /// `Pour quel compte voulez-vous réinitialiser le mot de passe ?`
  String get ForWhichAccount {
    return Intl.message(
      'Pour quel compte voulez-vous réinitialiser le mot de passe ?',
      name: 'ForWhichAccount',
      desc: '',
      args: [],
    );
  }

  /// `Choisissez une méthode pour recevoir le code de réinitialisation`
  String get ChooseAMethod {
    return Intl.message(
      'Choisissez une méthode pour recevoir le code de réinitialisation',
      name: 'ChooseAMethod',
      desc: '',
      args: [],
    );
  }

  /// `Confirmez votre identité avec le code que vous avez reçu par {method}`
  String ConfirmYourIdentity(Object method) {
    return Intl.message(
      'Confirmez votre identité avec le code que vous avez reçu par $method',
      name: 'ConfirmYourIdentity',
      desc: '',
      args: [method],
    );
  }

  /// `Votre nouveau mot de passe vous a été envoyé par {method}`
  String YourNewPasswordHaveBeenSendBy(Object method) {
    return Intl.message(
      'Votre nouveau mot de passe vous a été envoyé par $method',
      name: 'YourNewPasswordHaveBeenSendBy',
      desc: '',
      args: [method],
    );
  }

  /// ``
  String get _____AUTH_SIGN_UP____________________________ {
    return Intl.message(
      '',
      name: '_____AUTH_SIGN_UP____________________________',
      desc: '',
      args: [],
    );
  }

  /// `Confirmez le mot de passe`
  String get ConfirmPassword {
    return Intl.message(
      'Confirmez le mot de passe',
      name: 'ConfirmPassword',
      desc: '',
      args: [],
    );
  }

  /// `Merci de confirmer votre inscription en cliquant sur le lien qui vous a été envoyé par e-mail`
  String get PleaseConfirmYourRegistration {
    return Intl.message(
      'Merci de confirmer votre inscription en cliquant sur le lien qui vous a été envoyé par e-mail',
      name: 'PleaseConfirmYourRegistration',
      desc: '',
      args: [],
    );
  }

  /// ``
  String get _____HOME____________________________________ {
    return Intl.message(
      '',
      name: '_____HOME____________________________________',
      desc: '',
      args: [],
    );
  }

  /// `Bienvenue`
  String get Welcome {
    return Intl.message(
      'Bienvenue',
      name: 'Welcome',
      desc: '',
      args: [],
    );
  }

  /// `Réglages`
  String get Settings {
    return Intl.message(
      'Réglages',
      name: 'Settings',
      desc: '',
      args: [],
    );
  }

  /// `Utilisateurs`
  String get Users {
    return Intl.message(
      'Utilisateurs',
      name: 'Users',
      desc: '',
      args: [],
    );
  }

  /// `Déconnexion`
  String get LogOut {
    return Intl.message(
      'Déconnexion',
      name: 'LogOut',
      desc: '',
      args: [],
    );
  }

  /// `Bonjour {name}`
  String YourLoggedAs(Object name) {
    return Intl.message(
      'Bonjour $name',
      name: 'YourLoggedAs',
      desc: '',
      args: [name],
    );
  }

  /// ``
  String get _____SETTINGS________________________________ {
    return Intl.message(
      '',
      name: '_____SETTINGS________________________________',
      desc: '',
      args: [],
    );
  }

  /// `Titre de la page d'accueil`
  String get HomePageTitle {
    return Intl.message(
      'Titre de la page d\'accueil',
      name: 'HomePageTitle',
      desc: '',
      args: [],
    );
  }

  /// `Cette valeur sera enregistrée dans la table "GlobalPreferences".`
  String get ThisValueWillBeStoredInGlobalPrefsTable {
    return Intl.message(
      'Cette valeur sera enregistrée dans la table "GlobalPreferences".',
      name: 'ThisValueWillBeStoredInGlobalPrefsTable',
      desc: '',
      args: [],
    );
  }

  /// `Couleurs du thème`
  String get ThemeColors {
    return Intl.message(
      'Couleurs du thème',
      name: 'ThemeColors',
      desc: '',
      args: [],
    );
  }

  /// `Primaire`
  String get Primary {
    return Intl.message(
      'Primaire',
      name: 'Primary',
      desc: '',
      args: [],
    );
  }

  /// `Secondaire`
  String get Secondary {
    return Intl.message(
      'Secondaire',
      name: 'Secondary',
      desc: '',
      args: [],
    );
  }

  /// ``
  String get _____USER____________________________________ {
    return Intl.message(
      '',
      name: '_____USER____________________________________',
      desc: '',
      args: [],
    );
  }

  /// `utilisateur`
  String get user {
    return Intl.message(
      'utilisateur',
      name: 'user',
      desc: '',
      args: [],
    );
  }

  /// `Utilisateur`
  String get User {
    return Intl.message(
      'Utilisateur',
      name: 'User',
      desc: '',
      args: [],
    );
  }

  /// `Administrateur`
  String get Admin {
    return Intl.message(
      'Administrateur',
      name: 'Admin',
      desc: '',
      args: [],
    );
  }

  /// `Informations principales`
  String get MainInfo {
    return Intl.message(
      'Informations principales',
      name: 'MainInfo',
      desc: '',
      args: [],
    );
  }

  /// `Prénom`
  String get Firstname {
    return Intl.message(
      'Prénom',
      name: 'Firstname',
      desc: '',
      args: [],
    );
  }

  /// `Nom`
  String get Lastname {
    return Intl.message(
      'Nom',
      name: 'Lastname',
      desc: '',
      args: [],
    );
  }

  /// `Email invalide`
  String get NotValidEmail {
    return Intl.message(
      'Email invalide',
      name: 'NotValidEmail',
      desc: '',
      args: [],
    );
  }

  /// `Vous allez perdre vos modifications`
  String get YouWillLooseUnsavedModifications {
    return Intl.message(
      'Vous allez perdre vos modifications',
      name: 'YouWillLooseUnsavedModifications',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<Translation> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'fr'),
      Locale.fromSubtags(languageCode: 'en'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<Translation> load(Locale locale) => Translation.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    for (var supportedLocale in supportedLocales) {
      if (supportedLocale.languageCode == locale.languageCode) {
        return true;
      }
    }
    return false;
  }
}
