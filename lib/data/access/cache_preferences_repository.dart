import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:isar/isar.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:flutter_scaffold_riverpod_v2/abstract/interfaces/isar_model.dart';
import 'package:flutter_scaffold_riverpod_v2/abstract/interfaces/repository.dart';
import 'package:flutter_scaffold_riverpod_v2/models/cache_preferences.dart';
import 'package:flutter_scaffold_riverpod_v2/services/common_providers.dart';

part 'cache_preferences_repository.g.dart';

// To generate @riverpod methods (in filename.g.dart) run:
// flutter pub run build_runner build --delete-conflicting-outputs

/// This method defines a riverpod's provider to get our Repository instance
@riverpod
CachePreferencesRepository cachePreferencesRepository(CachePreferencesRepositoryRef ref) =>
    CachePreferencesRepository(ref.read(isarProvider));

/// This class is a Repository to retrieve data stored in our local DB
class CachePreferencesRepository extends Repository<CachePreferences> {
  CachePreferencesRepository(this._database);

  final Isar _database;

  @override
  Future<CachePreferences> get([int? id]) async {
    id ??= IsarSingleEntityModel.defaultId;
    CachePreferences? result = await _database.cachePreferences.get(id);
    if (result == null) {
      // If this object doesn't exist in our database we'll create it
      result = CachePreferences();
      await insert(result);
    }
    debugPrint('[$runtimeType] get($id) -> $result');
    return result;
  }

  @override
  Future<int> insert(CachePreferences entry) async {
    final result = await _database.writeTxn(() => _database.cachePreferences.put(entry));
    debugPrint('[$runtimeType] insert($entry) -> $result');
    return result;
  }

  @override
  Future<int> update(CachePreferences entry) {
    debugPrint('[$runtimeType] update($entry)');
    return insert(entry);
  }
}
