// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cache_preferences_repository.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$cachePreferencesRepositoryHash() =>
    r'8a8c0d56a367f3059b1fbfb210270d5ecd027605';

/// This method defines a riverpod's provider to get our Repository instance
///
/// Copied from [cachePreferencesRepository].
@ProviderFor(cachePreferencesRepository)
final cachePreferencesRepositoryProvider =
    AutoDisposeProvider<CachePreferencesRepository>.internal(
  cachePreferencesRepository,
  name: r'cachePreferencesRepositoryProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$cachePreferencesRepositoryHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef CachePreferencesRepositoryRef
    = AutoDisposeProviderRef<CachePreferencesRepository>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member
