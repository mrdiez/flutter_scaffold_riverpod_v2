import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:isar/isar.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:flutter_scaffold_riverpod_v2/abstract/interfaces/isar_model.dart';
import 'package:flutter_scaffold_riverpod_v2/abstract/interfaces/repository.dart';
import 'package:flutter_scaffold_riverpod_v2/models/global_preferences.dart';
import 'package:flutter_scaffold_riverpod_v2/services/common_providers.dart';

part 'global_preferences_repository.g.dart';

// To generate @riverpod methods (in filename.g.dart) run:
// flutter pub run build_runner build --delete-conflicting-outputs

/// This method defines a riverpod's provider to get our Repository instance
@riverpod
GlobalPreferencesRepository globalPreferencesRepository(GlobalPreferencesRepositoryRef ref) =>
    GlobalPreferencesRepository(ref.read(isarProvider));

/// This method defines a riverpod's provider to listen changes on data stored in our local DB
@riverpod
Stream<GlobalPreferences> globalPreferencesStream(GlobalPreferencesStreamRef ref) =>
    ref.read(globalPreferencesRepositoryProvider).watch();

/// This class is a Repository to retrieve data stored in our local DB
class GlobalPreferencesRepository extends Repository<GlobalPreferences> {
  GlobalPreferencesRepository(this._database);

  final Isar _database;

  @override
  Stream<GlobalPreferences> watch([int? id]) {
    id ??= IsarSingleEntityModel.defaultId;
    return _database.globalPreferences.watchObject(id, fireImmediately: true).asyncMap((entry) async {
      if (entry == null) {
        // If this object doesn't exist in our database we'll create it
        entry = GlobalPreferences();
        await insert(entry);
      }
      debugPrint('[$runtimeType] watch($id) -> $entry');
      return entry;
    });
  }

  @override
  Future<GlobalPreferences> get([int? id]) async {
    id ??= IsarSingleEntityModel.defaultId;
    final result = await _database.globalPreferences.get(id);
    debugPrint('[$runtimeType] get($id) -> $result');
    return result!;
  }

  @override
  Future<int> insert(GlobalPreferences entry) async {
    final result = await _database.writeTxn(() => _database.globalPreferences.put(entry));
    debugPrint('[$runtimeType] insert($entry) -> $result');
    return result;
  }

  @override
  Future<int> update(GlobalPreferences entry) {
    debugPrint('[$runtimeType] update($entry)');
    return insert(entry);
  }
}
