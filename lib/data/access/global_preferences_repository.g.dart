// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'global_preferences_repository.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$globalPreferencesRepositoryHash() =>
    r'b0aa44623fc06137dc1e4cdbaea54fc7af06be4b';

/// This method defines a riverpod's provider to get our Repository instance
///
/// Copied from [globalPreferencesRepository].
@ProviderFor(globalPreferencesRepository)
final globalPreferencesRepositoryProvider =
    AutoDisposeProvider<GlobalPreferencesRepository>.internal(
  globalPreferencesRepository,
  name: r'globalPreferencesRepositoryProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$globalPreferencesRepositoryHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef GlobalPreferencesRepositoryRef
    = AutoDisposeProviderRef<GlobalPreferencesRepository>;
String _$globalPreferencesStreamHash() =>
    r'af8dd7a834ff8ea49c360f910d5a3e84e6ee7fd6';

/// This method defines a riverpod's provider to listen changes on data stored in our local DB
///
/// Copied from [globalPreferencesStream].
@ProviderFor(globalPreferencesStream)
final globalPreferencesStreamProvider =
    AutoDisposeStreamProvider<GlobalPreferences>.internal(
  globalPreferencesStream,
  name: r'globalPreferencesStreamProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$globalPreferencesStreamHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef GlobalPreferencesStreamRef
    = AutoDisposeStreamProviderRef<GlobalPreferences>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member
