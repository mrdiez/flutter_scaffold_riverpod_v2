import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_scaffold_riverpod_v2/config/environment.dart';

/// This method defines a riverpod's provider to get our API instance
final networkApiProvider = Provider<NetworkApi>((ref) => NetworkApi());

/// This class is an HTTP API
class NetworkApi {
  final Dio client = Dio(BaseOptions(baseUrl: Environment.baseUrl));
  final _headers = {
    HttpHeaders.acceptHeader: 'json/application/json',
    HttpHeaders.contentTypeHeader: 'application/x-www-form-urlencoded'
  };

  Future<Map<String, dynamic>> get(
    String path, {
    Map<String, dynamic>? queryParameters,
    Map<String, dynamic> headers = const {},
  }) async {
    final target = '${Environment.baseUrl}$path';
    debugPrint('[$runtimeType] get $target');
    try {
      final response = await client.get(
        target,
        options: Options(headers: {..._headers, ...headers}),
        queryParameters: queryParameters,
      );
      _checkResponseStatus(response);
      Map<String, dynamic> responseData = response.data is String ? json.decode(response.data) : response.data;
      debugPrint('[$runtimeType] get response: $responseData');
      return responseData;
    } catch (e, stackTrace) {
      debugPrint('[$runtimeType] get $path ERROR:\n\n$e\n\n$stackTrace');
      rethrow;
    }
  }

  Future<List<Map<String, dynamic>>> getMany(
    String path, {
    Map<String, dynamic>? queryParameters,
    Map<String, dynamic> headers = const {},
  }) async {
    final target = '${Environment.baseUrl}$path';
    debugPrint('[$runtimeType] getMany\n$target');
    try {
      final response = await client.get(
        target,
        options: Options(headers: {..._headers, ...headers}),
        queryParameters: queryParameters,
      );
      _checkResponseStatus(response);
      List<dynamic> responseData = response.data is String ? json.decode(response.data) : response.data;
      return responseData as List<Map<String, dynamic>>;
    } catch (e, stackTrace) {
      debugPrint('[$runtimeType] getMany ERROR:\n\n$e\n\n$stackTrace');
      rethrow;
    }
  }

  Future<Map<String, dynamic>> post(
    String path,
    Map<String, dynamic> data, {
    Map<String, dynamic>? queryParameters,
    Map<String, dynamic> headers = const {},
  }) async {
    final target = '${Environment.baseUrl}$path';
    debugPrint('[$runtimeType] post: $data\n$target');
    try {
      final response = await client.post(
        target,
        data: data,
        options: Options(headers: {..._headers, ...headers}),
        queryParameters: queryParameters,
      );
      _checkResponseStatus(response);
      Map<String, dynamic> responseData = response.data is String ? json.decode(response.data) : response.data;
      return responseData;
    } catch (e, stackTrace) {
      debugPrint('[$runtimeType] post: $data ERROR:\n\n$e\n\n$stackTrace');
      rethrow;
    }
  }

  void _checkResponseStatus(Response response) {
    if (response.statusCode != 200) {
      throw 'Status: ${response.statusCode}\nResponse:${response.statusMessage}';
    }
    if (response.data is Map<String, dynamic> && response.data.containsKey('error')) {
      throw response.data['error'];
    }
  }
}
