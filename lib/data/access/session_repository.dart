import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter_scaffold_riverpod_v2/data/access/user_repository.dart';
import 'package:isar/isar.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:rxdart/rxdart.dart';
import 'package:flutter_scaffold_riverpod_v2/abstract/interfaces/isar_model.dart';
import 'package:flutter_scaffold_riverpod_v2/abstract/interfaces/repository.dart';
import 'package:flutter_scaffold_riverpod_v2/models/session.dart';
import 'package:flutter_scaffold_riverpod_v2/services/common_providers.dart';

part 'session_repository.g.dart';

// To generate @riverpod methods (in filename.g.dart) run:
// flutter pub run build_runner build --delete-conflicting-outputs

/// This method defines a riverpod's provider to get our Repository instance
@riverpod
SessionRepository sessionRepository(SessionRepositoryRef ref) => SessionRepository(
      ref.read(isarProvider),
      ref.read(userRepositoryProvider),
    );

/// This method defines a riverpod's provider to listen changes on data stored in our local DB
@riverpod
Stream<Session?> sessionStream(SessionStreamRef ref) => ref.read(sessionRepositoryProvider).watch();

/// This method defines a riverpod's provider to listen changes on data stored in our local DB while ignoring null results
@riverpod
Stream<Session> startedSessionStream(StartedSessionStreamRef ref) => ref.read(sessionRepositoryProvider).watchNonNull();

/// This class is a Repository to retrieve data stored in our local DB
class SessionRepository extends Repository<Session> {
  SessionRepository(this._database, this._userRepository);

  final Isar _database;
  final UserRepository _userRepository;
  Stream<Session?>? _watch;
  Stream<Session>? _watchNonNull;

  @override
  Stream<Session?> watch([int? id]) {
    id ??= IsarSingleEntityModel.defaultId;
    return _watch ??= _database.sessions.watchObject(id, fireImmediately: true).asyncMap((entry) async {
      debugPrint('[$runtimeType] watch($id) -> $entry');
      await entry?.user.load();
      return entry;
    }).switchMap((session) {
      if (session?.user.value != null) return _watchUserFromSession(session!);
      return Stream.value(session);
    });
  }

  Stream<Session> watchNonNull([int? id]) {
    id ??= IsarSingleEntityModel.defaultId;
    return _watchNonNull ??= _database.sessions.watchObject(id, fireImmediately: true).mapNotNull((entry) {
      if (entry != null) debugPrint('[$runtimeType] watchNonNull($id) -> $entry');
      return entry;
    }).switchMap(_watchUserFromSession);
  }

  /// This stream is used to watch any changes done on the logged user in our DB in order to update accordingly our Session.user's link
  Stream<Session> _watchUserFromSession(Session session) {
    // Each time the current logged user is updated in the Database
    return _userRepository.watch(session.user.value!.id).asyncMap((user) async {
      session.user.value = user;
      // We save the database links (we save the User into our Session object in DB)
      await _database.writeTxn(session.user.save);
      return session;
    });
  }

  @override
  Future<Session?> get([int? id]) async {
    id ??= IsarSingleEntityModel.defaultId;
    final result = await _database.sessions.get(id);
    debugPrint('[$runtimeType] get($id) -> $result');
    return result;
  }

  @override
  Future<int> insert(Session entry) async {
    assert(entry.user.value != null);
    late int result;
    // First we save our linked Object
    await _userRepository.update(entry.user.value!);
    // Then we save our main Object
    await _database.writeTxn(() async {
      result = await _database.sessions.put(entry);
      // Then we save the link itself
      await entry.user.save();
    });
    debugPrint('[$runtimeType] insert($entry) -> $result');
    return result;
  }

  @override
  Future<int> update(Session entry) {
    debugPrint('[$runtimeType] update($entry)');
    return insert(entry);
  }

  @override
  Future<bool> delete([int? id]) async {
    id ??= IsarSingleEntityModel.defaultId;
    final result = await _database.writeTxn(() => _database.sessions.delete(id!));
    debugPrint('[$runtimeType] delete($id)');
    return result;
  }
}
