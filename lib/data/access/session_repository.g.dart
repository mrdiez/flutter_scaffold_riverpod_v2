// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'session_repository.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$sessionRepositoryHash() => r'd7552195ce4e0d9051f5829021fa7230ad2b7bff';

/// This method defines a riverpod's provider to get our Repository instance
///
/// Copied from [sessionRepository].
@ProviderFor(sessionRepository)
final sessionRepositoryProvider =
    AutoDisposeProvider<SessionRepository>.internal(
  sessionRepository,
  name: r'sessionRepositoryProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$sessionRepositoryHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef SessionRepositoryRef = AutoDisposeProviderRef<SessionRepository>;
String _$sessionStreamHash() => r'c3998da5aa6ab092e70458547ea4ad36c3d2ce47';

/// This method defines a riverpod's provider to listen changes on data stored in our local DB
///
/// Copied from [sessionStream].
@ProviderFor(sessionStream)
final sessionStreamProvider = AutoDisposeStreamProvider<Session?>.internal(
  sessionStream,
  name: r'sessionStreamProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$sessionStreamHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef SessionStreamRef = AutoDisposeStreamProviderRef<Session?>;
String _$startedSessionStreamHash() =>
    r'56276982d90f9ff7aaae2b6874c6c8c3d193b14b';

/// This method defines a riverpod's provider to listen changes on data stored in our local DB while ignoring null results
///
/// Copied from [startedSessionStream].
@ProviderFor(startedSessionStream)
final startedSessionStreamProvider =
    AutoDisposeStreamProvider<Session>.internal(
  startedSessionStream,
  name: r'startedSessionStreamProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$startedSessionStreamHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef StartedSessionStreamRef = AutoDisposeStreamProviderRef<Session>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member
