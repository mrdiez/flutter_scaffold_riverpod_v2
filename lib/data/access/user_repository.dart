import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter_scaffold_riverpod_v2/models/user_preferences.dart';
import 'package:isar/isar.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:flutter_scaffold_riverpod_v2/abstract/interfaces/repository.dart';
import 'package:flutter_scaffold_riverpod_v2/data/access/cache_preferences_repository.dart';
import 'package:flutter_scaffold_riverpod_v2/data/access/network_api.dart';
import 'package:flutter_scaffold_riverpod_v2/models/user.dart';
import 'package:flutter_scaffold_riverpod_v2/services/common_providers.dart';
import 'package:flutter_scaffold_riverpod_v2/utils/isar.dart';

part 'user_repository.g.dart';

// To generate @riverpod methods (in filename.g.dart) run:
// flutter pub run build_runner build --delete-conflicting-outputs

/// This method defines a riverpod's provider to get our global Repository instance
@riverpod
UserRepository userRepository(UserRepositoryRef ref) => UserRepository(
      ref.read(userNetworkRepositoryProvider),
      ref.read(userLocalRepositoryProvider),
      ref.read(cachePreferencesRepositoryProvider),
    );

/// This method defines a riverpod's provider to get our local Repository instance
@riverpod
UserNetworkRepository userNetworkRepository(UserNetworkRepositoryRef ref) =>
    UserNetworkRepository(ref.read(networkApiProvider));

/// This method defines a riverpod's provider to get our network Repository instance
@riverpod
UserLocalRepository userLocalRepository(UserLocalRepositoryRef ref) => UserLocalRepository(ref.read(isarProvider));

/// This method defines a riverpod's provider to listen changes on data stored in our local DB
@riverpod
Stream<User?> userStream(UserStreamRef ref, {required int id}) => ref.read(userRepositoryProvider).watch(id);

/// This class is a GlobalCachedRepository to retrieve data stored in our local DB or thought the network if cache has expired
class UserRepository extends GlobalCachedRepository<User> {
  UserRepository(Repository<User> networkRepository, Repository<User> localRepository,
      CachePreferencesRepository cachePreferencesRepository)
      : super(networkRepository, localRepository, cachePreferencesRepository);
}

/// This class is a Repository to retrieve data stored in our local DB
class UserLocalRepository extends Repository<User> {
  UserLocalRepository(this._database);

  final Isar _database;

  @override
  Stream<List<User>> watchAll() {
    return _database.users.where().watch(fireImmediately: true).map((users) {
      debugPrint('[$runtimeType] watchAll() -> ${users.length} user${users.length > 1 ? 's' : ''}');
      return users;
    });
  }

  @override
  Stream<User?> watch(int id) {
    return _database.users.watchObject(id, fireImmediately: true).map((user) {
      debugPrint('[$runtimeType] watch($id) -> $user');
      return user;
    });
  }

  @override
  Future<User?> get(int id) async {
    final result = await _database.users.get(id);
    debugPrint('[$runtimeType] get($id) -> $result');
    return result;
  }

  @override
  Future<List<User>> getAll() async {
    final result = await _database.users.where().findAll();
    debugPrint('[$runtimeType] getAll() -> ${result.length} user${result.length > 1 ? 's' : ''}');
    return result;
  }

  @override
  Future<int> insert(User entry) async {
    entry.preferences.value ??= UserPreferences();
    late int result;
    await _database.writeTxn(() async {
      // Then we save our main Object
      result = await _database.users.put(entry);
      // Then we save our linked Object
      await _database.userPreferences.put(entry.preferences.value!);
      // Then we save the link itself
      await entry.preferences.save();
    });
    debugPrint('[$runtimeType] insert($entry) -> $result');
    return result;
  }

  @override
  Future<List<int>> insertAll(List<User> entries) async {
    final result = await _database.writeTxn(() => _database.users.putAll(entries));
    debugPrint('[$runtimeType] insertAll(${entries.ids}) -> ${result.length} row${result.length > 1 ? 's' : ''}');
    return result;
  }

  @override
  Future<int> update(User entry) {
    debugPrint('[$runtimeType] update($entry)');
    return insert(entry);
  }

  @override
  Future<List<int>> updateAll(List<User> entries) {
    debugPrint('[$runtimeType] updateAll(${entries.ids})');
    return insertAll(entries);
  }

  @override
  Future<bool> delete(int id) async {
    final result = await _database.writeTxn(() => _database.users.delete(id));
    debugPrint('[$runtimeType] delete($id) -> $result');
    return result;
  }

  @override
  Future<int> deleteAll(List<int> ids) async {
    final result = await _database.writeTxn(() => _database.users.deleteAll(ids));
    debugPrint('[$runtimeType] deleteAll($ids) -> $result');
    return result;
  }
}

/// This class is a Repository to retrieve data thought network
class UserNetworkRepository extends Repository<User> {
  static const path = '/users';

  UserNetworkRepository(this._networkApi);

  final NetworkApi _networkApi;

  @override
  Future<List<User>> getAll() async => (await _networkApi.getMany(path)).map(User.fromJson).toList();

  @override
  Future<int> insert(User entry) async {
    final response = await _networkApi.post(path, entry.toJson());
    return response.isNotEmpty ? 1 : 0;
  }
}
