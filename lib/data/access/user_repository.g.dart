// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_repository.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$userRepositoryHash() => r'aaf4484cc56f27fd2bec0ed143a5ef427afcda20';

/// This method defines a riverpod's provider to get our global Repository instance
///
/// Copied from [userRepository].
@ProviderFor(userRepository)
final userRepositoryProvider = AutoDisposeProvider<UserRepository>.internal(
  userRepository,
  name: r'userRepositoryProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$userRepositoryHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef UserRepositoryRef = AutoDisposeProviderRef<UserRepository>;
String _$userNetworkRepositoryHash() =>
    r'6ab71c5c1fee7990bd64c597b36c6f38ee519343';

/// This method defines a riverpod's provider to get our local Repository instance
///
/// Copied from [userNetworkRepository].
@ProviderFor(userNetworkRepository)
final userNetworkRepositoryProvider =
    AutoDisposeProvider<UserNetworkRepository>.internal(
  userNetworkRepository,
  name: r'userNetworkRepositoryProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$userNetworkRepositoryHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef UserNetworkRepositoryRef
    = AutoDisposeProviderRef<UserNetworkRepository>;
String _$userLocalRepositoryHash() =>
    r'295ae82af7df485ef29bf76502b81e44bc3c0515';

/// This method defines a riverpod's provider to get our network Repository instance
///
/// Copied from [userLocalRepository].
@ProviderFor(userLocalRepository)
final userLocalRepositoryProvider =
    AutoDisposeProvider<UserLocalRepository>.internal(
  userLocalRepository,
  name: r'userLocalRepositoryProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$userLocalRepositoryHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef UserLocalRepositoryRef = AutoDisposeProviderRef<UserLocalRepository>;
String _$userStreamHash() => r'2f63c936955ce38501fdf21fad09f63a012eab08';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

typedef UserStreamRef = AutoDisposeStreamProviderRef<User?>;

/// This method defines a riverpod's provider to listen changes on data stored in our local DB
///
/// Copied from [userStream].
@ProviderFor(userStream)
const userStreamProvider = UserStreamFamily();

/// This method defines a riverpod's provider to listen changes on data stored in our local DB
///
/// Copied from [userStream].
class UserStreamFamily extends Family<AsyncValue<User?>> {
  /// This method defines a riverpod's provider to listen changes on data stored in our local DB
  ///
  /// Copied from [userStream].
  const UserStreamFamily();

  /// This method defines a riverpod's provider to listen changes on data stored in our local DB
  ///
  /// Copied from [userStream].
  UserStreamProvider call({
    required int id,
  }) {
    return UserStreamProvider(
      id: id,
    );
  }

  @override
  UserStreamProvider getProviderOverride(
    covariant UserStreamProvider provider,
  ) {
    return call(
      id: provider.id,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'userStreamProvider';
}

/// This method defines a riverpod's provider to listen changes on data stored in our local DB
///
/// Copied from [userStream].
class UserStreamProvider extends AutoDisposeStreamProvider<User?> {
  /// This method defines a riverpod's provider to listen changes on data stored in our local DB
  ///
  /// Copied from [userStream].
  UserStreamProvider({
    required this.id,
  }) : super.internal(
          (ref) => userStream(
            ref,
            id: id,
          ),
          from: userStreamProvider,
          name: r'userStreamProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$userStreamHash,
          dependencies: UserStreamFamily._dependencies,
          allTransitiveDependencies:
              UserStreamFamily._allTransitiveDependencies,
        );

  final int id;

  @override
  bool operator ==(Object other) {
    return other is UserStreamProvider && other.id == id;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, id.hashCode);

    return _SystemHash.finish(hash);
  }
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member
