import 'package:dartx/dartx.dart';
import 'package:flutter/material.dart';
import 'package:flutter_scaffold_riverpod_v2/data/access/network_api.dart';

/// This class is a mocked version of our HTTP API
/// This way we can continue to work even when OVH is burning
class MockedNetworkApi extends NetworkApi {
  static final latency = 1.seconds;

  MockedNetworkApi(this.dataByPath);

  final Map<String, List<dynamic>> dataByPath;

  @override
  Future<Map<String, dynamic>> get(
    String path, {
    Map<String, dynamic>? queryParameters,
    Map<String, dynamic> headers = const {},
  }) async {
    debugPrint('[$runtimeType] get $path');
    await Future.delayed(latency);
    return dataByPath.containsKey(path) ? dataByPath[path]!.first as Map<String, dynamic> : {};
  }

  @override
  Future<List<Map<String, dynamic>>> getMany(
    String path, {
    Map<String, dynamic>? queryParameters,
    Map<String, dynamic> headers = const {},
  }) async {
    debugPrint('[$runtimeType] getMany $path');
    await Future.delayed(latency);
    return dataByPath.containsKey(path) ? dataByPath[path] as List<Map<String, dynamic>> : <Map<String, dynamic>>[];
  }

  @override
  Future<Map<String, dynamic>> post(
    String path,
    Map<String, dynamic> data, {
    Map<String, dynamic>? queryParameters,
    Map<String, dynamic> headers = const {},
  }) async {
    debugPrint('[$runtimeType] post $path\n$data');
    await Future.delayed(latency);
    dataByPath[path]!.add(data);
    return Future.value({});
  }
}
