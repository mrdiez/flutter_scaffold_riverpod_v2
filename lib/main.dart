import 'dart:math';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_scaffold_riverpod_v2/models/user_preferences.dart';
import 'package:go_router/go_router.dart';
import 'package:isar/isar.dart';
import 'package:flutter_scaffold_riverpod_v2/config/environment.dart';
import 'package:flutter_scaffold_riverpod_v2/config/routes.dart';
import 'package:flutter_scaffold_riverpod_v2/config/theme/sizes.dart';
import 'package:flutter_scaffold_riverpod_v2/config/theme/theme.dart';
import 'package:flutter_scaffold_riverpod_v2/config/translations/gen/l10n.dart';
import 'package:flutter_scaffold_riverpod_v2/data/access/network_api.dart';
import 'package:flutter_scaffold_riverpod_v2/data/access/session_repository.dart';
import 'package:flutter_scaffold_riverpod_v2/data/access/user_repository.dart';
import 'package:flutter_scaffold_riverpod_v2/data/mocks/mocked_network_api.dart';
import 'package:flutter_scaffold_riverpod_v2/data/mocks/mocked_users.dart';
import 'package:flutter_scaffold_riverpod_v2/models/cache_preferences.dart';
import 'package:flutter_scaffold_riverpod_v2/models/global_preferences.dart';
import 'package:flutter_scaffold_riverpod_v2/models/session.dart';
import 'package:flutter_scaffold_riverpod_v2/models/user.dart';
import 'package:flutter_scaffold_riverpod_v2/modules/auth/data/access/authentication_api.dart';
import 'package:flutter_scaffold_riverpod_v2/modules/auth/data/mocks/mocked_authentication_network_api.dart';
import 'package:flutter_scaffold_riverpod_v2/services/common_providers.dart';
import 'package:path_provider/path_provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // Retrieving current device's screen size
  final windowSize = MediaQueryData.fromView(PlatformDispatcher.instance.views.first).size;
  // Then lock rotation if device's screen is too small
  if ($Screen.size(min(windowSize.height, windowSize.width)) < xs) {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  }
  // Retrieve device's app directory (required to init our database)
  final dir = await getApplicationDocumentsDirectory();
  runApp(
    ProviderScope(
      overrides: [
        // Here we're replacing our unimplemented Isar database provider
        // by one that will return an instance of Isar database
        // configured with all our modules' schemas
        isarProvider.overrideWithValue(
          await Isar.open(
            [
              GlobalPreferencesSchema,
              CachePreferencesSchema,
              SessionSchema,
              UserSchema,
              UserPreferencesSchema,
            ],
            directory: dir.path,
          ),
        ),
        // If environment is mocked then provided NetworkApi instance
        // will be replaced with a MockedNetworkApi instance
        // which will provide corresponding data depending on the called path
        if (Environment.isMock) ...[
          networkApiProvider.overrideWithValue(
            MockedNetworkApi({
              UserNetworkRepository.path: mockedUsersData,
            }),
          ),
          authenticationApiProvider.overrideWith((ref) => MockedAuthenticationApi(ref.read(isarProvider).users)),
        ],
      ],
      child: const Root(),
    ),
  );
}

// This widget is the root of your application.
class Root extends StatelessWidget {
  const Root({super.key});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      // This line allow us to hide keyboard when tapping outside of a text field
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: Consumer(
        builder: (context, ref, child) {
          final preferences =
              ref.watch(sessionStreamProvider.select((session) => session.value?.user.value?.preferences.value));
          return MaterialApp(
            title: 'Keke\'s sandbox project', // Title of the app
            theme: preferences != null
                ? getTheme(preferences.primaryColor, preferences.secondaryColor)
                : defaultTheme, // Setting up the theme defined in lib/config/theme
            localizationsDelegates: const [
              Translation.delegate, // Setting up generated translations in lib/config/translations
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
              GlobalCupertinoLocalizations.delegate,
            ],
            supportedLocales: Translation.delegate.supportedLocales, // Setting up supported languages
            home: child,
          );
        },
        child: Material(
          // Having a Material widget as ancestor is mandatory for the theme to be applied correctly
          child: Consumer(builder: (context, ref, child) {
            final isLogged = ref.watch(sessionStreamProvider.select((session) => session.value?.started ?? false));
            // Setting up our app's routes
            final router = GoRouter(
              // Enable hero animations on page transitions
              observers: [HeroController()],
              // First route reached when opening the app
              initialLocation: isLogged ? Routes.home : Routes.auth,
              // All available routes
              routes: Routes.all,
              errorPageBuilder: (context, state) => Routes.notFound(context, state),
            );
            return Router(
              // Setting up our app's router
              routeInformationProvider: router.routeInformationProvider,
              routeInformationParser: router.routeInformationParser,
              routerDelegate: router.routerDelegate,
              backButtonDispatcher: RootBackButtonDispatcher(),
            );
          }),
        ),
      ),
    );
  }
}
