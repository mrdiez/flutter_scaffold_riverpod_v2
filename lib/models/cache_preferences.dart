import 'package:flutter/material.dart';
import 'package:isar/isar.dart';
import 'package:dartx/dartx.dart';
import 'package:flutter_scaffold_riverpod_v2/abstract/interfaces/isar_map.dart';
import 'package:flutter_scaffold_riverpod_v2/abstract/interfaces/isar_model.dart';
import 'package:flutter_scaffold_riverpod_v2/models/user.dart';

part 'cache_preferences.g.dart';

// To generate @collection classes (in filename.g.dart) run:
// flutter pub run build_runner build --delete-conflicting-outputs

/// This class defines cache preferences
/// It is supposed to be unique in our DB
/// Our GlobalCachedRepository depends on this one to know if data should be retrieved from the network or the local DB
@collection
class CachePreferences extends IsarSingleEntityModel {
  /// Main constructor that builds our object instances
  CachePreferences();

  /// Default cache durations (in seconds) mapped by stored types
  /// NOTE: you will need to clear app data (uninstall/reinstall) if you change this default value
  /// or you will need to update the CachePreferences value stored in the database
  IsarMap _cacheDurationByType = IsarMap<String, int>.fromMap({'$User': 300});

  // IsarMap abstract class allow us to save in database a Map<dynamic, dynamic>
  // but in order to be able to type them we need these setter & getter
  set cacheDurationByType(IsarMap value) => _cacheDurationByType = value.cast<String, int>();
  IsarMap<String, int> get cacheDurationByType => _cacheDurationByType as IsarMap<String, int>;

  /// Cache dates mapped by stored types
  IsarMap _cacheDatesByType = IsarMap<String, DateTime>();

  // IsarMap abstract class allow us to save in database a Map<dynamic, dynamic>
  // but in order to be able to type them we need these setter & getter
  set cacheDatesByType(IsarMap value) => _cacheDatesByType = value.cast<String, DateTime>();
  IsarMap<String, DateTime> get cacheDatesByType => _cacheDatesByType as IsarMap<String, DateTime>;

  /// Returns true if the stored cache date for the given type is expired or if there is no date stored
  bool isExpired<T>() {
    if (!cacheDatesByType.containsKey('$T')) {
      debugPrint('[$runtimeType] $T data has never been cached');
      return true;
    }
    final timeSinceDataCached = DateTime.now().difference(cacheDatesByType['$T']!);
    final cacheDuration = (cacheDurationByType['$T'] ?? 0).seconds;
    final result = timeSinceDataCached > cacheDuration;
    debugPrint(
        '[$runtimeType] $T cached data is ${result ? 'expired by ${timeSinceDataCached - cacheDuration}s' : 'not expired'}');
    return result;
  }

  /// An override of the toString() method (for more readable debugging when printing this object in the logs)
  @override
  String toString() =>
      '[$runtimeType] - ${cacheDurationByType.keys.map((key) => '$key cache duration: ${cacheDurationByType[key]}s, cached on ${cacheDatesByType[key]}')}';
}
