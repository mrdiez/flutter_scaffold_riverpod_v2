// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cache_preferences.dart';

// **************************************************************************
// IsarCollectionGenerator
// **************************************************************************

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters, always_specify_types

extension GetCachePreferencesCollection on Isar {
  IsarCollection<CachePreferences> get cachePreferences => this.collection();
}

const CachePreferencesSchema = CollectionSchema(
  name: r'CachePreferences',
  id: -7924034624569263078,
  properties: {
    r'cacheDatesByType': PropertySchema(
      id: 0,
      name: r'cacheDatesByType',
      type: IsarType.object,
      target: r'IsarMap',
    ),
    r'cacheDurationByType': PropertySchema(
      id: 1,
      name: r'cacheDurationByType',
      type: IsarType.object,
      target: r'IsarMap',
    )
  },
  estimateSize: _cachePreferencesEstimateSize,
  serialize: _cachePreferencesSerialize,
  deserialize: _cachePreferencesDeserialize,
  deserializeProp: _cachePreferencesDeserializeProp,
  idName: r'id',
  indexes: {},
  links: {},
  embeddedSchemas: {r'IsarMap': IsarMapSchema},
  getId: _cachePreferencesGetId,
  getLinks: _cachePreferencesGetLinks,
  attach: _cachePreferencesAttach,
  version: '3.1.0+1',
);

int _cachePreferencesEstimateSize(
  CachePreferences object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  bytesCount += 3 +
      IsarMapSchema.estimateSize(
          object.cacheDatesByType, allOffsets[IsarMap]!, allOffsets);
  bytesCount += 3 +
      IsarMapSchema.estimateSize(
          object.cacheDurationByType, allOffsets[IsarMap]!, allOffsets);
  return bytesCount;
}

void _cachePreferencesSerialize(
  CachePreferences object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeObject<IsarMap>(
    offsets[0],
    allOffsets,
    IsarMapSchema.serialize,
    object.cacheDatesByType,
  );
  writer.writeObject<IsarMap>(
    offsets[1],
    allOffsets,
    IsarMapSchema.serialize,
    object.cacheDurationByType,
  );
}

CachePreferences _cachePreferencesDeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = CachePreferences();
  object.cacheDatesByType = reader.readObjectOrNull<IsarMap>(
        offsets[0],
        IsarMapSchema.deserialize,
        allOffsets,
      ) ??
      IsarMap();
  object.cacheDurationByType = reader.readObjectOrNull<IsarMap>(
        offsets[1],
        IsarMapSchema.deserialize,
        allOffsets,
      ) ??
      IsarMap();
  return object;
}

P _cachePreferencesDeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (reader.readObjectOrNull<IsarMap>(
            offset,
            IsarMapSchema.deserialize,
            allOffsets,
          ) ??
          IsarMap()) as P;
    case 1:
      return (reader.readObjectOrNull<IsarMap>(
            offset,
            IsarMapSchema.deserialize,
            allOffsets,
          ) ??
          IsarMap()) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

Id _cachePreferencesGetId(CachePreferences object) {
  return object.id;
}

List<IsarLinkBase<dynamic>> _cachePreferencesGetLinks(CachePreferences object) {
  return [];
}

void _cachePreferencesAttach(
    IsarCollection<dynamic> col, Id id, CachePreferences object) {}

extension CachePreferencesQueryWhereSort
    on QueryBuilder<CachePreferences, CachePreferences, QWhere> {
  QueryBuilder<CachePreferences, CachePreferences, QAfterWhere> anyId() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(const IdWhereClause.any());
    });
  }
}

extension CachePreferencesQueryWhere
    on QueryBuilder<CachePreferences, CachePreferences, QWhereClause> {
  QueryBuilder<CachePreferences, CachePreferences, QAfterWhereClause> idEqualTo(
      Id id) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: id,
        upper: id,
      ));
    });
  }

  QueryBuilder<CachePreferences, CachePreferences, QAfterWhereClause>
      idNotEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            )
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            );
      } else {
        return query
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            )
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            );
      }
    });
  }

  QueryBuilder<CachePreferences, CachePreferences, QAfterWhereClause>
      idGreaterThan(Id id, {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.greaterThan(lower: id, includeLower: include),
      );
    });
  }

  QueryBuilder<CachePreferences, CachePreferences, QAfterWhereClause>
      idLessThan(Id id, {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.lessThan(upper: id, includeUpper: include),
      );
    });
  }

  QueryBuilder<CachePreferences, CachePreferences, QAfterWhereClause> idBetween(
    Id lowerId,
    Id upperId, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: lowerId,
        includeLower: includeLower,
        upper: upperId,
        includeUpper: includeUpper,
      ));
    });
  }
}

extension CachePreferencesQueryFilter
    on QueryBuilder<CachePreferences, CachePreferences, QFilterCondition> {
  QueryBuilder<CachePreferences, CachePreferences, QAfterFilterCondition>
      idEqualTo(Id value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<CachePreferences, CachePreferences, QAfterFilterCondition>
      idGreaterThan(
    Id value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<CachePreferences, CachePreferences, QAfterFilterCondition>
      idLessThan(
    Id value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<CachePreferences, CachePreferences, QAfterFilterCondition>
      idBetween(
    Id lower,
    Id upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'id',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }
}

extension CachePreferencesQueryObject
    on QueryBuilder<CachePreferences, CachePreferences, QFilterCondition> {
  QueryBuilder<CachePreferences, CachePreferences, QAfterFilterCondition>
      cacheDatesByType(FilterQuery<IsarMap> q) {
    return QueryBuilder.apply(this, (query) {
      return query.object(q, r'cacheDatesByType');
    });
  }

  QueryBuilder<CachePreferences, CachePreferences, QAfterFilterCondition>
      cacheDurationByType(FilterQuery<IsarMap> q) {
    return QueryBuilder.apply(this, (query) {
      return query.object(q, r'cacheDurationByType');
    });
  }
}

extension CachePreferencesQueryLinks
    on QueryBuilder<CachePreferences, CachePreferences, QFilterCondition> {}

extension CachePreferencesQuerySortBy
    on QueryBuilder<CachePreferences, CachePreferences, QSortBy> {}

extension CachePreferencesQuerySortThenBy
    on QueryBuilder<CachePreferences, CachePreferences, QSortThenBy> {
  QueryBuilder<CachePreferences, CachePreferences, QAfterSortBy> thenById() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.asc);
    });
  }

  QueryBuilder<CachePreferences, CachePreferences, QAfterSortBy>
      thenByIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.desc);
    });
  }
}

extension CachePreferencesQueryWhereDistinct
    on QueryBuilder<CachePreferences, CachePreferences, QDistinct> {}

extension CachePreferencesQueryProperty
    on QueryBuilder<CachePreferences, CachePreferences, QQueryProperty> {
  QueryBuilder<CachePreferences, int, QQueryOperations> idProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'id');
    });
  }

  QueryBuilder<CachePreferences, IsarMap, QQueryOperations>
      cacheDatesByTypeProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'cacheDatesByType');
    });
  }

  QueryBuilder<CachePreferences, IsarMap, QQueryOperations>
      cacheDurationByTypeProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'cacheDurationByType');
    });
  }
}
