import 'package:isar/isar.dart';
import 'package:flutter_scaffold_riverpod_v2/abstract/interfaces/isar_model.dart';
import 'package:flutter_scaffold_riverpod_v2/config/translations/gen/l10n.dart';

part 'global_preferences.g.dart';

// To generate @collection classes (in filename.g.dart) run:
// flutter pub run build_runner build --delete-conflicting-outputs

/// This class defines global preferences
/// It is supposed to be unique in our DB
/// These preferences will be applied to the whole app ignoring which user is using the app
@collection
class GlobalPreferences extends IsarSingleEntityModel {
  /// Main constructor that builds our object instance
  GlobalPreferences() : homeTitle = Translation.current.Welcome;

  // The property that defines the Home Page's title
  String homeTitle;

  // An override of the toString() method (for more readable debugging when printing this object in the logs)
  @override
  String toString() => '$runtimeType - homeTitle: "$homeTitle"';
}
