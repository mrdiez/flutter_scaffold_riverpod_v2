// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'global_preferences.dart';

// **************************************************************************
// IsarCollectionGenerator
// **************************************************************************

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters, always_specify_types

extension GetGlobalPreferencesCollection on Isar {
  IsarCollection<GlobalPreferences> get globalPreferences => this.collection();
}

const GlobalPreferencesSchema = CollectionSchema(
  name: r'GlobalPreferences',
  id: -7107980081722479728,
  properties: {
    r'homeTitle': PropertySchema(
      id: 0,
      name: r'homeTitle',
      type: IsarType.string,
    )
  },
  estimateSize: _globalPreferencesEstimateSize,
  serialize: _globalPreferencesSerialize,
  deserialize: _globalPreferencesDeserialize,
  deserializeProp: _globalPreferencesDeserializeProp,
  idName: r'id',
  indexes: {},
  links: {},
  embeddedSchemas: {},
  getId: _globalPreferencesGetId,
  getLinks: _globalPreferencesGetLinks,
  attach: _globalPreferencesAttach,
  version: '3.1.0+1',
);

int _globalPreferencesEstimateSize(
  GlobalPreferences object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  bytesCount += 3 + object.homeTitle.length * 3;
  return bytesCount;
}

void _globalPreferencesSerialize(
  GlobalPreferences object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeString(offsets[0], object.homeTitle);
}

GlobalPreferences _globalPreferencesDeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = GlobalPreferences();
  object.homeTitle = reader.readString(offsets[0]);
  return object;
}

P _globalPreferencesDeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (reader.readString(offset)) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

Id _globalPreferencesGetId(GlobalPreferences object) {
  return object.id;
}

List<IsarLinkBase<dynamic>> _globalPreferencesGetLinks(
    GlobalPreferences object) {
  return [];
}

void _globalPreferencesAttach(
    IsarCollection<dynamic> col, Id id, GlobalPreferences object) {}

extension GlobalPreferencesQueryWhereSort
    on QueryBuilder<GlobalPreferences, GlobalPreferences, QWhere> {
  QueryBuilder<GlobalPreferences, GlobalPreferences, QAfterWhere> anyId() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(const IdWhereClause.any());
    });
  }
}

extension GlobalPreferencesQueryWhere
    on QueryBuilder<GlobalPreferences, GlobalPreferences, QWhereClause> {
  QueryBuilder<GlobalPreferences, GlobalPreferences, QAfterWhereClause>
      idEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: id,
        upper: id,
      ));
    });
  }

  QueryBuilder<GlobalPreferences, GlobalPreferences, QAfterWhereClause>
      idNotEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            )
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            );
      } else {
        return query
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            )
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            );
      }
    });
  }

  QueryBuilder<GlobalPreferences, GlobalPreferences, QAfterWhereClause>
      idGreaterThan(Id id, {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.greaterThan(lower: id, includeLower: include),
      );
    });
  }

  QueryBuilder<GlobalPreferences, GlobalPreferences, QAfterWhereClause>
      idLessThan(Id id, {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.lessThan(upper: id, includeUpper: include),
      );
    });
  }

  QueryBuilder<GlobalPreferences, GlobalPreferences, QAfterWhereClause>
      idBetween(
    Id lowerId,
    Id upperId, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: lowerId,
        includeLower: includeLower,
        upper: upperId,
        includeUpper: includeUpper,
      ));
    });
  }
}

extension GlobalPreferencesQueryFilter
    on QueryBuilder<GlobalPreferences, GlobalPreferences, QFilterCondition> {
  QueryBuilder<GlobalPreferences, GlobalPreferences, QAfterFilterCondition>
      homeTitleEqualTo(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'homeTitle',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GlobalPreferences, GlobalPreferences, QAfterFilterCondition>
      homeTitleGreaterThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'homeTitle',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GlobalPreferences, GlobalPreferences, QAfterFilterCondition>
      homeTitleLessThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'homeTitle',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GlobalPreferences, GlobalPreferences, QAfterFilterCondition>
      homeTitleBetween(
    String lower,
    String upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'homeTitle',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GlobalPreferences, GlobalPreferences, QAfterFilterCondition>
      homeTitleStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'homeTitle',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GlobalPreferences, GlobalPreferences, QAfterFilterCondition>
      homeTitleEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'homeTitle',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GlobalPreferences, GlobalPreferences, QAfterFilterCondition>
      homeTitleContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'homeTitle',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GlobalPreferences, GlobalPreferences, QAfterFilterCondition>
      homeTitleMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'homeTitle',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GlobalPreferences, GlobalPreferences, QAfterFilterCondition>
      homeTitleIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'homeTitle',
        value: '',
      ));
    });
  }

  QueryBuilder<GlobalPreferences, GlobalPreferences, QAfterFilterCondition>
      homeTitleIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'homeTitle',
        value: '',
      ));
    });
  }

  QueryBuilder<GlobalPreferences, GlobalPreferences, QAfterFilterCondition>
      idEqualTo(Id value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<GlobalPreferences, GlobalPreferences, QAfterFilterCondition>
      idGreaterThan(
    Id value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<GlobalPreferences, GlobalPreferences, QAfterFilterCondition>
      idLessThan(
    Id value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<GlobalPreferences, GlobalPreferences, QAfterFilterCondition>
      idBetween(
    Id lower,
    Id upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'id',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }
}

extension GlobalPreferencesQueryObject
    on QueryBuilder<GlobalPreferences, GlobalPreferences, QFilterCondition> {}

extension GlobalPreferencesQueryLinks
    on QueryBuilder<GlobalPreferences, GlobalPreferences, QFilterCondition> {}

extension GlobalPreferencesQuerySortBy
    on QueryBuilder<GlobalPreferences, GlobalPreferences, QSortBy> {
  QueryBuilder<GlobalPreferences, GlobalPreferences, QAfterSortBy>
      sortByHomeTitle() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'homeTitle', Sort.asc);
    });
  }

  QueryBuilder<GlobalPreferences, GlobalPreferences, QAfterSortBy>
      sortByHomeTitleDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'homeTitle', Sort.desc);
    });
  }
}

extension GlobalPreferencesQuerySortThenBy
    on QueryBuilder<GlobalPreferences, GlobalPreferences, QSortThenBy> {
  QueryBuilder<GlobalPreferences, GlobalPreferences, QAfterSortBy>
      thenByHomeTitle() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'homeTitle', Sort.asc);
    });
  }

  QueryBuilder<GlobalPreferences, GlobalPreferences, QAfterSortBy>
      thenByHomeTitleDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'homeTitle', Sort.desc);
    });
  }

  QueryBuilder<GlobalPreferences, GlobalPreferences, QAfterSortBy> thenById() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.asc);
    });
  }

  QueryBuilder<GlobalPreferences, GlobalPreferences, QAfterSortBy>
      thenByIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.desc);
    });
  }
}

extension GlobalPreferencesQueryWhereDistinct
    on QueryBuilder<GlobalPreferences, GlobalPreferences, QDistinct> {
  QueryBuilder<GlobalPreferences, GlobalPreferences, QDistinct>
      distinctByHomeTitle({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'homeTitle', caseSensitive: caseSensitive);
    });
  }
}

extension GlobalPreferencesQueryProperty
    on QueryBuilder<GlobalPreferences, GlobalPreferences, QQueryProperty> {
  QueryBuilder<GlobalPreferences, int, QQueryOperations> idProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'id');
    });
  }

  QueryBuilder<GlobalPreferences, String, QQueryOperations>
      homeTitleProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'homeTitle');
    });
  }
}
