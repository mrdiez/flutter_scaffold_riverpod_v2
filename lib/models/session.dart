import 'package:isar/isar.dart';
import 'package:flutter_scaffold_riverpod_v2/abstract/interfaces/isar_model.dart';
import 'package:flutter_scaffold_riverpod_v2/models/user.dart';
import 'package:flutter_scaffold_riverpod_v2/models/user_preferences.dart';

part 'session.g.dart';

// To generate @collection classes (in filename.g.dart) run:
// flutter pub run build_runner build --delete-conflicting-outputs

/// This class defines a session
/// It is supposed to be unique in our DB
@collection
class Session extends IsarSingleEntityModel {
  /// Main constructor that builds our object instance
  Session();

  /// A factory that builds our object instance while populating its database link value
  factory Session.start(User user) => Session()..user.value = user;

  /// A getter to know if a session has been started
  bool get started => user.value != null;

  /// The property that defines the current logged user
  final user = IsarLink<User>();

  /// A getter to know if the current logged user is an admin
  bool get admin => user.value?.admin ?? false;

  /// A getter to access easily to user's prefs
  set preferences(UserPreferences value) => user.value!.preferences.value = value;
  @ignore
  UserPreferences get preferences => user.value!.preferences.value!;

  /// An override of the toString() method (for more readable debugging when printing this object in the logs)
  @override
  String toString() => '$runtimeType - user: "${user.value}"';
}
