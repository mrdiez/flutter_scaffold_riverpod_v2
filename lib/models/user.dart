import 'package:dartx/dartx.dart';
import 'package:isar/isar.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:flutter_scaffold_riverpod_v2/abstract/interfaces/isar_model.dart';
import 'package:flutter_scaffold_riverpod_v2/models/user_preferences.dart';

part 'user.g.dart';

// To generate @collection/@JsonSerializable classes (in filename.g.dart) run:
// flutter pub run build_runner build --delete-conflicting-outputs

/// This class defines our user
@collection
@JsonSerializable()
class User extends IsarModel {
  User({
    required this.firstName,
    required this.lastName,
    required this.email,
    required this.password,
    this.admin = false,
  });

  factory User.empty() => User(firstName: '', lastName: '', email: '', password: '');

  String firstName;
  String lastName;
  String email;
  String password;
  bool admin;

  /// User's preferences
  final preferences = IsarLink<UserPreferences>();

  String get name {
    String result = '';
    if (firstName.isNotBlank) {
      result += firstName;
      if (lastName.isNotBlank) result += ' $lastName';
    } else if (lastName.isNotBlank) {
      result += lastName;
    }
    return result;
  }

  String get initials {
    String result = '';
    if (firstName.isNotBlank) result += firstName[0].toUpperCase();
    if (lastName.isNotBlank) result += lastName[0].toUpperCase();
    return result;
  }

  @override
  String toString() => '$runtimeType $id: "$name", $email, ${preferences.value}';

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);
  Map<String, dynamic> toJson() => _$UserToJson(this);
}
