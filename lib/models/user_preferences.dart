import 'package:flutter/material.dart';
import 'package:flutter_scaffold_riverpod_v2/abstract/interfaces/isar_model.dart';
import 'package:flutter_scaffold_riverpod_v2/config/theme/theme.dart';
import 'package:isar/isar.dart';
import 'package:ms_material_color/ms_material_color.dart';

part 'user_preferences.g.dart';

// To generate @Embedded classes (in filename.g.dart) run:
// flutter pub run build_runner build --delete-conflicting-outputs

/// This class defines user's preferences
/// These preferences will be applied only after a user has been identified
@Collection(ignore: {'copyWith'})
class UserPreferences extends IsarModel {
  UserPreferences();

  int primaryColorValue = $primaryColorValue;
  int secondaryColorValue = $secondaryColorValue;

  set primaryColor(Color color) => primaryColorValue = color.value;
  set secondaryColor(Color color) => secondaryColorValue = color.value;

  // These getters are marked with @ignore because we don't want them to be generated for the Isar DB
  @ignore
  Color get primaryColor => MsMaterialColor(primaryColorValue);
  @ignore
  Color get secondaryColor => MsMaterialColor(secondaryColorValue);

  // An override of the toString() method (for more readable debugging when printing this object in the logs)
  @override
  String toString() => 'Theme colors: ($primaryColorValue, $secondaryColorValue)';
}
