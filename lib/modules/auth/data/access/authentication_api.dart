import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_scaffold_riverpod_v2/config/translations/gen/l10n.dart';
import 'package:flutter_scaffold_riverpod_v2/data/access/network_api.dart';
import 'package:flutter_scaffold_riverpod_v2/models/user.dart';

/// This method defines a riverpod's provider to get our API instance
final authenticationApiProvider = Provider<AuthenticationApi>((ref) => AuthenticationApi(ref.read(networkApiProvider)));

/// This class defines our authentication API
class AuthenticationApi {
  const AuthenticationApi(this._networkApi);

  final NetworkApi _networkApi;

  Future<User> logIn(String login, String password) async {
    final data = await _networkApi.get('/auth', queryParameters: {'login': login, 'password': password});
    return User.fromJson(data);
  }

  Future<List<String>> getResetPasswordMethods(String login) async {
    final data = await _networkApi.get('/reset_password_methods', queryParameters: {'login': login});
    if (data.isEmpty) {
      throw Exception(Translation.current.UnknownError);
    }
    return data['methods'] as List<String>;
  }

  Future<void> postResetPasswordMethod(String login, String method) =>
      _networkApi.post('/reset_password_methods', {'login': login, 'method': method});

  Future<void> resetPassword(String login, String confirmationCode) =>
      _networkApi.post('/reset_password', {'login': login, 'confirmationCode': confirmationCode});

  Future<User> signUp(String email, String password) async {
    final data = await _networkApi.post('/sign_up', {'email': email, 'password': password});
    return User.fromJson(data);
  }
}
