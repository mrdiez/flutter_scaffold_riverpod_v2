import 'package:dartx/dartx.dart';
import 'package:flutter/material.dart';
import 'package:isar/isar.dart';
import 'package:flutter_scaffold_riverpod_v2/config/translations/gen/l10n.dart';
import 'package:flutter_scaffold_riverpod_v2/data/mocks/mocked_users.dart';
import 'package:flutter_scaffold_riverpod_v2/models/user.dart';
import 'package:flutter_scaffold_riverpod_v2/modules/auth/data/access/authentication_api.dart';

/// This class defines a mocked version of our authentication API
/// This way we can continue to work even when OVH is burning
class MockedAuthenticationApi implements AuthenticationApi {
  static final latency = 1.seconds;

  const MockedAuthenticationApi(this._userCollection);

  final IsarCollection<User> _userCollection;

  @override
  Future<User> logIn(String login, String password) async {
    debugPrint('[$runtimeType] logIn $login');
    await Future.delayed(latency);
    final mockedUsers = mockedUsersData.map((data) => User.fromJson(data)).toList();
    final user = mockedUsers.firstOrNullWhere((user) => user.email == login && user.password == password);
    if (user != null) {
      final storedUser =
          await _userCollection.filter().emailEqualTo(user.email).passwordEqualTo(user.password).findFirst();
      return storedUser ?? user;
    }
    throw Translation.current.IncorrectCredentials;
  }

  @override
  Future<List<String>> getResetPasswordMethods(String login) async {
    debugPrint('[$runtimeType] getResetPasswordMethods $login');
    await Future.delayed(latency);
    return ['SMS', 'Email'];
  }

  @override
  Future<void> postResetPasswordMethod(String login, String method) async {
    debugPrint('[$runtimeType] postResetPasswordMethod $method');
    await Future.delayed(latency);
  }

  @override
  Future<void> resetPassword(String login, String confirmationCode) async {
    debugPrint('[$runtimeType] resetPassword $confirmationCode');
    await Future.delayed(latency);
  }

  @override
  Future<User> signUp(String email, String password) async {
    await Future.delayed(latency);
    return User(firstName: '', lastName: '', email: email, password: password);
  }
}
