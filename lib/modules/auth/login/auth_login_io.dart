import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter_scaffold_riverpod_v2/abstract/interfaces/error_event.dart';
import 'package:flutter_scaffold_riverpod_v2/abstract/mixins/event_provider.dart';
import 'package:flutter_scaffold_riverpod_v2/models/user.dart';

part 'auth_login_io.freezed.dart';

// To generate @freezed classes (in filename.freezed.dart) run:
// flutter pub run build_runner build --delete-conflicting-outputs

/// View states
@freezed
class AuthLoginState with _$AuthLoginState {
  const factory AuthLoginState({
    String? email,
    String? password,
    @Default(false) bool loading,
    @Default(false) bool error,
  }) = _AuthLoginState;
}

/// Logic events
/// Define here input/output events which are coming to/from this module's logic class
@freezed
class AuthLoginEvent with _$AuthLoginEvent, EventProvider<AuthLoginEvent> {
  AuthLoginEvent._() {
    trigger();
  }

  // Here is our logic's output events
  factory AuthLoginEvent.loggingIn(String? email, String? password) = AuthLoginEventLoggingIn;
  factory AuthLoginEvent.logIn(User user) = AuthLoginEventLogIn;

  // Error events can implement ErrorEvent in order to be able to check: if (event is ErrorEvent)
  @Implements<ErrorEvent>()
  factory AuthLoginEvent.incorrectCredentials([String? message]) = AuthLoginEventIncorrectCredentials;
  @Implements<ErrorEvent>()
  factory AuthLoginEvent.unknownError([String? message]) = AuthLoginEventUnknownError;
}
