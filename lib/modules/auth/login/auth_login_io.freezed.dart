// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'auth_login_io.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$AuthLoginState {
  String? get email => throw _privateConstructorUsedError;
  String? get password => throw _privateConstructorUsedError;
  bool get loading => throw _privateConstructorUsedError;
  bool get error => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $AuthLoginStateCopyWith<AuthLoginState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AuthLoginStateCopyWith<$Res> {
  factory $AuthLoginStateCopyWith(
          AuthLoginState value, $Res Function(AuthLoginState) then) =
      _$AuthLoginStateCopyWithImpl<$Res, AuthLoginState>;
  @useResult
  $Res call({String? email, String? password, bool loading, bool error});
}

/// @nodoc
class _$AuthLoginStateCopyWithImpl<$Res, $Val extends AuthLoginState>
    implements $AuthLoginStateCopyWith<$Res> {
  _$AuthLoginStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? email = freezed,
    Object? password = freezed,
    Object? loading = null,
    Object? error = null,
  }) {
    return _then(_value.copyWith(
      email: freezed == email
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String?,
      password: freezed == password
          ? _value.password
          : password // ignore: cast_nullable_to_non_nullable
              as String?,
      loading: null == loading
          ? _value.loading
          : loading // ignore: cast_nullable_to_non_nullable
              as bool,
      error: null == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_AuthLoginStateCopyWith<$Res>
    implements $AuthLoginStateCopyWith<$Res> {
  factory _$$_AuthLoginStateCopyWith(
          _$_AuthLoginState value, $Res Function(_$_AuthLoginState) then) =
      __$$_AuthLoginStateCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String? email, String? password, bool loading, bool error});
}

/// @nodoc
class __$$_AuthLoginStateCopyWithImpl<$Res>
    extends _$AuthLoginStateCopyWithImpl<$Res, _$_AuthLoginState>
    implements _$$_AuthLoginStateCopyWith<$Res> {
  __$$_AuthLoginStateCopyWithImpl(
      _$_AuthLoginState _value, $Res Function(_$_AuthLoginState) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? email = freezed,
    Object? password = freezed,
    Object? loading = null,
    Object? error = null,
  }) {
    return _then(_$_AuthLoginState(
      email: freezed == email
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String?,
      password: freezed == password
          ? _value.password
          : password // ignore: cast_nullable_to_non_nullable
              as String?,
      loading: null == loading
          ? _value.loading
          : loading // ignore: cast_nullable_to_non_nullable
              as bool,
      error: null == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$_AuthLoginState implements _AuthLoginState {
  const _$_AuthLoginState(
      {this.email, this.password, this.loading = false, this.error = false});

  @override
  final String? email;
  @override
  final String? password;
  @override
  @JsonKey()
  final bool loading;
  @override
  @JsonKey()
  final bool error;

  @override
  String toString() {
    return 'AuthLoginState(email: $email, password: $password, loading: $loading, error: $error)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_AuthLoginState &&
            (identical(other.email, email) || other.email == email) &&
            (identical(other.password, password) ||
                other.password == password) &&
            (identical(other.loading, loading) || other.loading == loading) &&
            (identical(other.error, error) || other.error == error));
  }

  @override
  int get hashCode => Object.hash(runtimeType, email, password, loading, error);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_AuthLoginStateCopyWith<_$_AuthLoginState> get copyWith =>
      __$$_AuthLoginStateCopyWithImpl<_$_AuthLoginState>(this, _$identity);
}

abstract class _AuthLoginState implements AuthLoginState {
  const factory _AuthLoginState(
      {final String? email,
      final String? password,
      final bool loading,
      final bool error}) = _$_AuthLoginState;

  @override
  String? get email;
  @override
  String? get password;
  @override
  bool get loading;
  @override
  bool get error;
  @override
  @JsonKey(ignore: true)
  _$$_AuthLoginStateCopyWith<_$_AuthLoginState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$AuthLoginEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String? email, String? password) loggingIn,
    required TResult Function(User user) logIn,
    required TResult Function(String? message) incorrectCredentials,
    required TResult Function(String? message) unknownError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String? email, String? password)? loggingIn,
    TResult? Function(User user)? logIn,
    TResult? Function(String? message)? incorrectCredentials,
    TResult? Function(String? message)? unknownError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String? email, String? password)? loggingIn,
    TResult Function(User user)? logIn,
    TResult Function(String? message)? incorrectCredentials,
    TResult Function(String? message)? unknownError,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(AuthLoginEventLoggingIn value) loggingIn,
    required TResult Function(AuthLoginEventLogIn value) logIn,
    required TResult Function(AuthLoginEventIncorrectCredentials value)
        incorrectCredentials,
    required TResult Function(AuthLoginEventUnknownError value) unknownError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(AuthLoginEventLoggingIn value)? loggingIn,
    TResult? Function(AuthLoginEventLogIn value)? logIn,
    TResult? Function(AuthLoginEventIncorrectCredentials value)?
        incorrectCredentials,
    TResult? Function(AuthLoginEventUnknownError value)? unknownError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(AuthLoginEventLoggingIn value)? loggingIn,
    TResult Function(AuthLoginEventLogIn value)? logIn,
    TResult Function(AuthLoginEventIncorrectCredentials value)?
        incorrectCredentials,
    TResult Function(AuthLoginEventUnknownError value)? unknownError,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AuthLoginEventCopyWith<$Res> {
  factory $AuthLoginEventCopyWith(
          AuthLoginEvent value, $Res Function(AuthLoginEvent) then) =
      _$AuthLoginEventCopyWithImpl<$Res, AuthLoginEvent>;
}

/// @nodoc
class _$AuthLoginEventCopyWithImpl<$Res, $Val extends AuthLoginEvent>
    implements $AuthLoginEventCopyWith<$Res> {
  _$AuthLoginEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$AuthLoginEventLoggingInCopyWith<$Res> {
  factory _$$AuthLoginEventLoggingInCopyWith(_$AuthLoginEventLoggingIn value,
          $Res Function(_$AuthLoginEventLoggingIn) then) =
      __$$AuthLoginEventLoggingInCopyWithImpl<$Res>;
  @useResult
  $Res call({String? email, String? password});
}

/// @nodoc
class __$$AuthLoginEventLoggingInCopyWithImpl<$Res>
    extends _$AuthLoginEventCopyWithImpl<$Res, _$AuthLoginEventLoggingIn>
    implements _$$AuthLoginEventLoggingInCopyWith<$Res> {
  __$$AuthLoginEventLoggingInCopyWithImpl(_$AuthLoginEventLoggingIn _value,
      $Res Function(_$AuthLoginEventLoggingIn) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? email = freezed,
    Object? password = freezed,
  }) {
    return _then(_$AuthLoginEventLoggingIn(
      freezed == email
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String?,
      freezed == password
          ? _value.password
          : password // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$AuthLoginEventLoggingIn extends AuthLoginEventLoggingIn {
  _$AuthLoginEventLoggingIn(this.email, this.password) : super._();

  @override
  final String? email;
  @override
  final String? password;

  @override
  String toString() {
    return 'AuthLoginEvent.loggingIn(email: $email, password: $password)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$AuthLoginEventLoggingIn &&
            (identical(other.email, email) || other.email == email) &&
            (identical(other.password, password) ||
                other.password == password));
  }

  @override
  int get hashCode => Object.hash(runtimeType, email, password);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$AuthLoginEventLoggingInCopyWith<_$AuthLoginEventLoggingIn> get copyWith =>
      __$$AuthLoginEventLoggingInCopyWithImpl<_$AuthLoginEventLoggingIn>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String? email, String? password) loggingIn,
    required TResult Function(User user) logIn,
    required TResult Function(String? message) incorrectCredentials,
    required TResult Function(String? message) unknownError,
  }) {
    return loggingIn(email, password);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String? email, String? password)? loggingIn,
    TResult? Function(User user)? logIn,
    TResult? Function(String? message)? incorrectCredentials,
    TResult? Function(String? message)? unknownError,
  }) {
    return loggingIn?.call(email, password);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String? email, String? password)? loggingIn,
    TResult Function(User user)? logIn,
    TResult Function(String? message)? incorrectCredentials,
    TResult Function(String? message)? unknownError,
    required TResult orElse(),
  }) {
    if (loggingIn != null) {
      return loggingIn(email, password);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(AuthLoginEventLoggingIn value) loggingIn,
    required TResult Function(AuthLoginEventLogIn value) logIn,
    required TResult Function(AuthLoginEventIncorrectCredentials value)
        incorrectCredentials,
    required TResult Function(AuthLoginEventUnknownError value) unknownError,
  }) {
    return loggingIn(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(AuthLoginEventLoggingIn value)? loggingIn,
    TResult? Function(AuthLoginEventLogIn value)? logIn,
    TResult? Function(AuthLoginEventIncorrectCredentials value)?
        incorrectCredentials,
    TResult? Function(AuthLoginEventUnknownError value)? unknownError,
  }) {
    return loggingIn?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(AuthLoginEventLoggingIn value)? loggingIn,
    TResult Function(AuthLoginEventLogIn value)? logIn,
    TResult Function(AuthLoginEventIncorrectCredentials value)?
        incorrectCredentials,
    TResult Function(AuthLoginEventUnknownError value)? unknownError,
    required TResult orElse(),
  }) {
    if (loggingIn != null) {
      return loggingIn(this);
    }
    return orElse();
  }
}

abstract class AuthLoginEventLoggingIn extends AuthLoginEvent {
  factory AuthLoginEventLoggingIn(final String? email, final String? password) =
      _$AuthLoginEventLoggingIn;
  AuthLoginEventLoggingIn._() : super._();

  String? get email;
  String? get password;
  @JsonKey(ignore: true)
  _$$AuthLoginEventLoggingInCopyWith<_$AuthLoginEventLoggingIn> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$AuthLoginEventLogInCopyWith<$Res> {
  factory _$$AuthLoginEventLogInCopyWith(_$AuthLoginEventLogIn value,
          $Res Function(_$AuthLoginEventLogIn) then) =
      __$$AuthLoginEventLogInCopyWithImpl<$Res>;
  @useResult
  $Res call({User user});
}

/// @nodoc
class __$$AuthLoginEventLogInCopyWithImpl<$Res>
    extends _$AuthLoginEventCopyWithImpl<$Res, _$AuthLoginEventLogIn>
    implements _$$AuthLoginEventLogInCopyWith<$Res> {
  __$$AuthLoginEventLogInCopyWithImpl(
      _$AuthLoginEventLogIn _value, $Res Function(_$AuthLoginEventLogIn) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? user = null,
  }) {
    return _then(_$AuthLoginEventLogIn(
      null == user
          ? _value.user
          : user // ignore: cast_nullable_to_non_nullable
              as User,
    ));
  }
}

/// @nodoc

class _$AuthLoginEventLogIn extends AuthLoginEventLogIn {
  _$AuthLoginEventLogIn(this.user) : super._();

  @override
  final User user;

  @override
  String toString() {
    return 'AuthLoginEvent.logIn(user: $user)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$AuthLoginEventLogIn &&
            (identical(other.user, user) || other.user == user));
  }

  @override
  int get hashCode => Object.hash(runtimeType, user);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$AuthLoginEventLogInCopyWith<_$AuthLoginEventLogIn> get copyWith =>
      __$$AuthLoginEventLogInCopyWithImpl<_$AuthLoginEventLogIn>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String? email, String? password) loggingIn,
    required TResult Function(User user) logIn,
    required TResult Function(String? message) incorrectCredentials,
    required TResult Function(String? message) unknownError,
  }) {
    return logIn(user);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String? email, String? password)? loggingIn,
    TResult? Function(User user)? logIn,
    TResult? Function(String? message)? incorrectCredentials,
    TResult? Function(String? message)? unknownError,
  }) {
    return logIn?.call(user);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String? email, String? password)? loggingIn,
    TResult Function(User user)? logIn,
    TResult Function(String? message)? incorrectCredentials,
    TResult Function(String? message)? unknownError,
    required TResult orElse(),
  }) {
    if (logIn != null) {
      return logIn(user);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(AuthLoginEventLoggingIn value) loggingIn,
    required TResult Function(AuthLoginEventLogIn value) logIn,
    required TResult Function(AuthLoginEventIncorrectCredentials value)
        incorrectCredentials,
    required TResult Function(AuthLoginEventUnknownError value) unknownError,
  }) {
    return logIn(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(AuthLoginEventLoggingIn value)? loggingIn,
    TResult? Function(AuthLoginEventLogIn value)? logIn,
    TResult? Function(AuthLoginEventIncorrectCredentials value)?
        incorrectCredentials,
    TResult? Function(AuthLoginEventUnknownError value)? unknownError,
  }) {
    return logIn?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(AuthLoginEventLoggingIn value)? loggingIn,
    TResult Function(AuthLoginEventLogIn value)? logIn,
    TResult Function(AuthLoginEventIncorrectCredentials value)?
        incorrectCredentials,
    TResult Function(AuthLoginEventUnknownError value)? unknownError,
    required TResult orElse(),
  }) {
    if (logIn != null) {
      return logIn(this);
    }
    return orElse();
  }
}

abstract class AuthLoginEventLogIn extends AuthLoginEvent {
  factory AuthLoginEventLogIn(final User user) = _$AuthLoginEventLogIn;
  AuthLoginEventLogIn._() : super._();

  User get user;
  @JsonKey(ignore: true)
  _$$AuthLoginEventLogInCopyWith<_$AuthLoginEventLogIn> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$AuthLoginEventIncorrectCredentialsCopyWith<$Res> {
  factory _$$AuthLoginEventIncorrectCredentialsCopyWith(
          _$AuthLoginEventIncorrectCredentials value,
          $Res Function(_$AuthLoginEventIncorrectCredentials) then) =
      __$$AuthLoginEventIncorrectCredentialsCopyWithImpl<$Res>;
  @useResult
  $Res call({String? message});
}

/// @nodoc
class __$$AuthLoginEventIncorrectCredentialsCopyWithImpl<$Res>
    extends _$AuthLoginEventCopyWithImpl<$Res,
        _$AuthLoginEventIncorrectCredentials>
    implements _$$AuthLoginEventIncorrectCredentialsCopyWith<$Res> {
  __$$AuthLoginEventIncorrectCredentialsCopyWithImpl(
      _$AuthLoginEventIncorrectCredentials _value,
      $Res Function(_$AuthLoginEventIncorrectCredentials) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? message = freezed,
  }) {
    return _then(_$AuthLoginEventIncorrectCredentials(
      freezed == message
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$AuthLoginEventIncorrectCredentials
    extends AuthLoginEventIncorrectCredentials {
  _$AuthLoginEventIncorrectCredentials([this.message]) : super._();

  @override
  final String? message;

  @override
  String toString() {
    return 'AuthLoginEvent.incorrectCredentials(message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$AuthLoginEventIncorrectCredentials &&
            (identical(other.message, message) || other.message == message));
  }

  @override
  int get hashCode => Object.hash(runtimeType, message);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$AuthLoginEventIncorrectCredentialsCopyWith<
          _$AuthLoginEventIncorrectCredentials>
      get copyWith => __$$AuthLoginEventIncorrectCredentialsCopyWithImpl<
          _$AuthLoginEventIncorrectCredentials>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String? email, String? password) loggingIn,
    required TResult Function(User user) logIn,
    required TResult Function(String? message) incorrectCredentials,
    required TResult Function(String? message) unknownError,
  }) {
    return incorrectCredentials(message);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String? email, String? password)? loggingIn,
    TResult? Function(User user)? logIn,
    TResult? Function(String? message)? incorrectCredentials,
    TResult? Function(String? message)? unknownError,
  }) {
    return incorrectCredentials?.call(message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String? email, String? password)? loggingIn,
    TResult Function(User user)? logIn,
    TResult Function(String? message)? incorrectCredentials,
    TResult Function(String? message)? unknownError,
    required TResult orElse(),
  }) {
    if (incorrectCredentials != null) {
      return incorrectCredentials(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(AuthLoginEventLoggingIn value) loggingIn,
    required TResult Function(AuthLoginEventLogIn value) logIn,
    required TResult Function(AuthLoginEventIncorrectCredentials value)
        incorrectCredentials,
    required TResult Function(AuthLoginEventUnknownError value) unknownError,
  }) {
    return incorrectCredentials(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(AuthLoginEventLoggingIn value)? loggingIn,
    TResult? Function(AuthLoginEventLogIn value)? logIn,
    TResult? Function(AuthLoginEventIncorrectCredentials value)?
        incorrectCredentials,
    TResult? Function(AuthLoginEventUnknownError value)? unknownError,
  }) {
    return incorrectCredentials?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(AuthLoginEventLoggingIn value)? loggingIn,
    TResult Function(AuthLoginEventLogIn value)? logIn,
    TResult Function(AuthLoginEventIncorrectCredentials value)?
        incorrectCredentials,
    TResult Function(AuthLoginEventUnknownError value)? unknownError,
    required TResult orElse(),
  }) {
    if (incorrectCredentials != null) {
      return incorrectCredentials(this);
    }
    return orElse();
  }
}

abstract class AuthLoginEventIncorrectCredentials extends AuthLoginEvent
    implements ErrorEvent {
  factory AuthLoginEventIncorrectCredentials([final String? message]) =
      _$AuthLoginEventIncorrectCredentials;
  AuthLoginEventIncorrectCredentials._() : super._();

  String? get message;
  @JsonKey(ignore: true)
  _$$AuthLoginEventIncorrectCredentialsCopyWith<
          _$AuthLoginEventIncorrectCredentials>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$AuthLoginEventUnknownErrorCopyWith<$Res> {
  factory _$$AuthLoginEventUnknownErrorCopyWith(
          _$AuthLoginEventUnknownError value,
          $Res Function(_$AuthLoginEventUnknownError) then) =
      __$$AuthLoginEventUnknownErrorCopyWithImpl<$Res>;
  @useResult
  $Res call({String? message});
}

/// @nodoc
class __$$AuthLoginEventUnknownErrorCopyWithImpl<$Res>
    extends _$AuthLoginEventCopyWithImpl<$Res, _$AuthLoginEventUnknownError>
    implements _$$AuthLoginEventUnknownErrorCopyWith<$Res> {
  __$$AuthLoginEventUnknownErrorCopyWithImpl(
      _$AuthLoginEventUnknownError _value,
      $Res Function(_$AuthLoginEventUnknownError) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? message = freezed,
  }) {
    return _then(_$AuthLoginEventUnknownError(
      freezed == message
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$AuthLoginEventUnknownError extends AuthLoginEventUnknownError {
  _$AuthLoginEventUnknownError([this.message]) : super._();

  @override
  final String? message;

  @override
  String toString() {
    return 'AuthLoginEvent.unknownError(message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$AuthLoginEventUnknownError &&
            (identical(other.message, message) || other.message == message));
  }

  @override
  int get hashCode => Object.hash(runtimeType, message);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$AuthLoginEventUnknownErrorCopyWith<_$AuthLoginEventUnknownError>
      get copyWith => __$$AuthLoginEventUnknownErrorCopyWithImpl<
          _$AuthLoginEventUnknownError>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String? email, String? password) loggingIn,
    required TResult Function(User user) logIn,
    required TResult Function(String? message) incorrectCredentials,
    required TResult Function(String? message) unknownError,
  }) {
    return unknownError(message);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String? email, String? password)? loggingIn,
    TResult? Function(User user)? logIn,
    TResult? Function(String? message)? incorrectCredentials,
    TResult? Function(String? message)? unknownError,
  }) {
    return unknownError?.call(message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String? email, String? password)? loggingIn,
    TResult Function(User user)? logIn,
    TResult Function(String? message)? incorrectCredentials,
    TResult Function(String? message)? unknownError,
    required TResult orElse(),
  }) {
    if (unknownError != null) {
      return unknownError(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(AuthLoginEventLoggingIn value) loggingIn,
    required TResult Function(AuthLoginEventLogIn value) logIn,
    required TResult Function(AuthLoginEventIncorrectCredentials value)
        incorrectCredentials,
    required TResult Function(AuthLoginEventUnknownError value) unknownError,
  }) {
    return unknownError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(AuthLoginEventLoggingIn value)? loggingIn,
    TResult? Function(AuthLoginEventLogIn value)? logIn,
    TResult? Function(AuthLoginEventIncorrectCredentials value)?
        incorrectCredentials,
    TResult? Function(AuthLoginEventUnknownError value)? unknownError,
  }) {
    return unknownError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(AuthLoginEventLoggingIn value)? loggingIn,
    TResult Function(AuthLoginEventLogIn value)? logIn,
    TResult Function(AuthLoginEventIncorrectCredentials value)?
        incorrectCredentials,
    TResult Function(AuthLoginEventUnknownError value)? unknownError,
    required TResult orElse(),
  }) {
    if (unknownError != null) {
      return unknownError(this);
    }
    return orElse();
  }
}

abstract class AuthLoginEventUnknownError extends AuthLoginEvent
    implements ErrorEvent {
  factory AuthLoginEventUnknownError([final String? message]) =
      _$AuthLoginEventUnknownError;
  AuthLoginEventUnknownError._() : super._();

  String? get message;
  @JsonKey(ignore: true)
  _$$AuthLoginEventUnknownErrorCopyWith<_$AuthLoginEventUnknownError>
      get copyWith => throw _privateConstructorUsedError;
}
