import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:flutter_scaffold_riverpod_v2/data/access/session_repository.dart';
import 'package:flutter_scaffold_riverpod_v2/models/session.dart';
import 'package:flutter_scaffold_riverpod_v2/modules/auth/data/access/authentication_api.dart';
import 'package:flutter_scaffold_riverpod_v2/modules/auth/login/auth_login_io.dart';
import 'package:flutter_scaffold_riverpod_v2/utils/form.dart';

part 'auth_login_logic.g.dart';

// To generate @riverpod classes (in filename.g.dart) run:
// flutter pub run build_runner build --delete-conflicting-outputs

// The Riverpod's generator will create a Provider for our logic class
// We can inject dependencies by using the "ref" property (Riverpod's WidgetRef)
// Note that our logic class will be automatically re-instantiated
// if we're using ref.watch and if this "watcher" is triggered

/// Logic class
@riverpod
class AuthLoginLogic extends _$AuthLoginLogic {
  // This method is called each time this logic class is built
  @override
  AuthLoginState build() {
    // We retrieve our api instance
    _authenticationApi = ref.read(authenticationApiProvider);
    // We retrieve our object's repository instance
    _sessionRepository = ref.read(sessionRepositoryProvider);
    // We return our default view's state
    return const AuthLoginState();
  }

  // The instance of the api which will handle network and/or local data
  late final AuthenticationApi _authenticationApi;

  // The instance of the repository which will handle network and/or local data
  late final SessionRepository _sessionRepository;

  // The form key to access form's state to perform a validation check on save
  final formKey = GlobalKey<FormBuilderState>();

  // Map inputs' values with their respective state's property
  void saveEmail(String? email) => state = state.copyWith(email: email?.trim());
  void savePassword(String? password) => state = state.copyWith(password: password?.trim());

  Future<void> submit() async {
    // Just to clean up unwanted spaces typed by the user
    formKey.currentState?.trim();
    // Check if all fields are correct
    if (!(formKey.currentState?.saveAndValidate() ?? false)) return;
    try {
      // We're informing our view that the authentication process is in progress
      state = state.copyWith(loading: true);
      AuthLoginEvent.loggingIn(state.email, state.password);
      final user = await _authenticationApi.logIn(state.email!, state.password!);
      await _sessionRepository.update(Session.start(user));
      AuthLoginEvent.logIn(user);
    } catch (e, stackTrace) {
      debugPrint('[$runtimeType] authentication ERROR:\n\n$e\n\n$stackTrace');
      state = state.copyWith(error: true);
      AuthLoginEvent.incorrectCredentials();
    }
    state = state.copyWith(loading: false);
  }
}
