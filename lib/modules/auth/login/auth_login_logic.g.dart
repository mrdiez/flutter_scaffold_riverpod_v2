// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'auth_login_logic.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$authLoginLogicHash() => r'229bf76d177a982f6cf4620e5839f759f76db11a';

/// Logic class
///
/// Copied from [AuthLoginLogic].
@ProviderFor(AuthLoginLogic)
final authLoginLogicProvider =
    AutoDisposeNotifierProvider<AuthLoginLogic, AuthLoginState>.internal(
  AuthLoginLogic.new,
  name: r'authLoginLogicProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$authLoginLogicHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$AuthLoginLogic = AutoDisposeNotifier<AuthLoginState>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member
