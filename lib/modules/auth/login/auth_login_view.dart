import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:flutter_scaffold_riverpod_v2/abstract/interfaces/error_event.dart';
import 'package:flutter_scaffold_riverpod_v2/abstract/mixins/event_provider.dart';
import 'package:flutter_scaffold_riverpod_v2/config/routes.dart';
import 'package:flutter_scaffold_riverpod_v2/config/theme/sizes.dart';
import 'package:flutter_scaffold_riverpod_v2/modules/auth/login/auth_login_io.dart';
import 'package:flutter_scaffold_riverpod_v2/modules/auth/login/auth_login_logic.dart';
import 'package:flutter_scaffold_riverpod_v2/modules/auth/widgets/auth_scaffold.dart';
import 'package:flutter_scaffold_riverpod_v2/utils/context.dart';
import 'package:flutter_scaffold_riverpod_v2/utils/form.dart';
import 'package:flutter_scaffold_riverpod_v2/widgets/buttons/common_button.dart';
import 'package:flutter_scaffold_riverpod_v2/widgets/buttons/common_text_button.dart';
import 'package:flutter_scaffold_riverpod_v2/widgets/inputs/animated_text_input.dart';
import 'package:flutter_scaffold_riverpod_v2/widgets/inputs/labelled_checkbox.dart';
import 'package:flutter_scaffold_riverpod_v2/widgets/misc/shake_on_trigger.dart';

/// View class
class AuthLoginView extends ConsumerStatefulWidget {
  const AuthLoginView({super.key});

  @override
  ConsumerState<AuthLoginView> createState() => _AuthLoginViewState();
}

class _AuthLoginViewState extends ConsumerState<AuthLoginView> {
  // Some key to access specific widgets' state
  final shakeOnTriggerKey = GlobalKey<ShakeOnTriggerState>();

  // Our text fields controllers
  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  // Our text fields focus nodes (to auto focus next field on submit)
  final emailFocusNode = FocusNode();
  final passwordFocusNode = FocusNode();

  // Handle logic's output's events
  void onEvent(AuthLoginEvent e, BuildContext context) {
    if (e is AuthLoginEventLogIn) return context.go(Routes.home);
    if (e is ErrorEvent) {
      String message = context.translation.UnknownError;
      if (e is AuthLoginEventIncorrectCredentials) {
        shakeOnTriggerKey.currentState?.toggle();
        message = context.translation.IncorrectCredentials;
      }
      return context.showErrorSnackBar(message);
    }
  }

  @override
  Widget build(BuildContext context) {
    // Here we retrieve our logic's instance
    final logic = ref.read(authLoginLogicProvider.notifier);
    // Starts listening our logic's output events
    EventProvider.listen<AuthLoginEvent>(ref, onEvent: (e) => onEvent(e, context));

    return AuthScaffold(
      title: context.translation.Welcome,
      child: FormBuilder(
        key: logic.formKey,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ShakeOnTrigger(
              key: shakeOnTriggerKey,
              child: Column(
                children: [
                  AnimatedTextInput(
                    controller: emailController,
                    focusNode: emailFocusNode,
                    validator: FormValidators.email,
                    decoration: InputDecoration(labelText: context.translation.Email),
                    onSaved: logic.saveEmail,
                    onFieldSubmitted: (_) => passwordFocusNode.requestFocus(),
                  ),
                  $Gap.xxs,
                  AnimatedTextInput(
                    obscureText: true,
                    controller: passwordController,
                    focusNode: passwordFocusNode,
                    validator: FormValidators.password,
                    decoration: InputDecoration(labelText: context.translation.Password),
                    onSaved: logic.savePassword,
                    onFieldSubmitted: (_) => logic.submit(),
                  ),
                ],
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Flexible(
                  child: LabelledCheckbox(
                    labelText: context.translation.RememberMe,
                    onChanged: (_) => context.showInformationSnackBar(context.translation.SorryNeedsToBeImplemented),
                  ),
                ),
                Flexible(
                  child: CommonTextButton(
                    onTap: () => context.go(Routes.resetPassword, extra: {'email': emailController.text}),
                    text: context.translation.ForgotPassword,
                  ),
                ),
              ],
            ),
            Consumer(builder: (context, ref, child) {
              final isLoading = ref.watch(authLoginLogicProvider.select((state) => state.loading));
              return Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Flexible(
                    child: CommonButton(
                      onTap: !isLoading ? () => context.go(Routes.signUp) : null,
                      text: context.translation.SignUp,
                    ),
                  ),
                  Flexible(
                    child: CommonButton(
                      onTap: !isLoading ? logic.submit : null,
                      text: context.translation.LogIn,
                    ),
                  ),
                ],
              );
            }),
          ],
        ),
      ),
    );
  }
}
