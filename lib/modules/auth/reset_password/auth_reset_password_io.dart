import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter_scaffold_riverpod_v2/abstract/interfaces/error_event.dart';
import 'package:flutter_scaffold_riverpod_v2/abstract/mixins/event_provider.dart';

part 'auth_reset_password_io.freezed.dart';

// To generate @freezed classes (in filename.freezed.dart) run:
// flutter pub run build_runner build --delete-conflicting-outputs

/// View states
@freezed
class AuthResetPasswordState with _$AuthResetPasswordState {
  const factory AuthResetPasswordState({
    String? email,
    @Default([]) List<String> methods,
    String? method,
    String? confirmationCode,
    @Default(false) bool loading,
    @Default(false) bool error,
  }) = _AuthResetPasswordState;
}

/// Logic events
/// Define here input/output events which are coming to/from this module's logic class
@freezed
class AuthResetPasswordEvent with _$AuthResetPasswordEvent, EventProvider<AuthResetPasswordEvent> {
  AuthResetPasswordEvent._() {
    trigger();
  }

  // Here is our logic's output events
  factory AuthResetPasswordEvent.step1Success(List<String> methods) = AuthResetPasswordEventStep1Success;
  factory AuthResetPasswordEvent.step2Success(String method) = AuthResetPasswordEventStep2Success;
  factory AuthResetPasswordEvent.step3Success() = AuthResetPasswordEventStep3Success;

  // Error events can implement ErrorEvent in order to be able to check: if (event is ErrorEvent)
  @Implements<ErrorEvent>()
  factory AuthResetPasswordEvent.unknownError([String? message]) = AuthResetPasswordEventUnknownError;
}
