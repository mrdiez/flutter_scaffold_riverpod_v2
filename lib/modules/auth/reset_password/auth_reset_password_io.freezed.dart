// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'auth_reset_password_io.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$AuthResetPasswordState {
  String? get email => throw _privateConstructorUsedError;
  List<String> get methods => throw _privateConstructorUsedError;
  String? get method => throw _privateConstructorUsedError;
  String? get confirmationCode => throw _privateConstructorUsedError;
  bool get loading => throw _privateConstructorUsedError;
  bool get error => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $AuthResetPasswordStateCopyWith<AuthResetPasswordState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AuthResetPasswordStateCopyWith<$Res> {
  factory $AuthResetPasswordStateCopyWith(AuthResetPasswordState value,
          $Res Function(AuthResetPasswordState) then) =
      _$AuthResetPasswordStateCopyWithImpl<$Res, AuthResetPasswordState>;
  @useResult
  $Res call(
      {String? email,
      List<String> methods,
      String? method,
      String? confirmationCode,
      bool loading,
      bool error});
}

/// @nodoc
class _$AuthResetPasswordStateCopyWithImpl<$Res,
        $Val extends AuthResetPasswordState>
    implements $AuthResetPasswordStateCopyWith<$Res> {
  _$AuthResetPasswordStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? email = freezed,
    Object? methods = null,
    Object? method = freezed,
    Object? confirmationCode = freezed,
    Object? loading = null,
    Object? error = null,
  }) {
    return _then(_value.copyWith(
      email: freezed == email
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String?,
      methods: null == methods
          ? _value.methods
          : methods // ignore: cast_nullable_to_non_nullable
              as List<String>,
      method: freezed == method
          ? _value.method
          : method // ignore: cast_nullable_to_non_nullable
              as String?,
      confirmationCode: freezed == confirmationCode
          ? _value.confirmationCode
          : confirmationCode // ignore: cast_nullable_to_non_nullable
              as String?,
      loading: null == loading
          ? _value.loading
          : loading // ignore: cast_nullable_to_non_nullable
              as bool,
      error: null == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_AuthResetPasswordStateCopyWith<$Res>
    implements $AuthResetPasswordStateCopyWith<$Res> {
  factory _$$_AuthResetPasswordStateCopyWith(_$_AuthResetPasswordState value,
          $Res Function(_$_AuthResetPasswordState) then) =
      __$$_AuthResetPasswordStateCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String? email,
      List<String> methods,
      String? method,
      String? confirmationCode,
      bool loading,
      bool error});
}

/// @nodoc
class __$$_AuthResetPasswordStateCopyWithImpl<$Res>
    extends _$AuthResetPasswordStateCopyWithImpl<$Res,
        _$_AuthResetPasswordState>
    implements _$$_AuthResetPasswordStateCopyWith<$Res> {
  __$$_AuthResetPasswordStateCopyWithImpl(_$_AuthResetPasswordState _value,
      $Res Function(_$_AuthResetPasswordState) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? email = freezed,
    Object? methods = null,
    Object? method = freezed,
    Object? confirmationCode = freezed,
    Object? loading = null,
    Object? error = null,
  }) {
    return _then(_$_AuthResetPasswordState(
      email: freezed == email
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String?,
      methods: null == methods
          ? _value._methods
          : methods // ignore: cast_nullable_to_non_nullable
              as List<String>,
      method: freezed == method
          ? _value.method
          : method // ignore: cast_nullable_to_non_nullable
              as String?,
      confirmationCode: freezed == confirmationCode
          ? _value.confirmationCode
          : confirmationCode // ignore: cast_nullable_to_non_nullable
              as String?,
      loading: null == loading
          ? _value.loading
          : loading // ignore: cast_nullable_to_non_nullable
              as bool,
      error: null == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$_AuthResetPasswordState implements _AuthResetPasswordState {
  const _$_AuthResetPasswordState(
      {this.email,
      final List<String> methods = const [],
      this.method,
      this.confirmationCode,
      this.loading = false,
      this.error = false})
      : _methods = methods;

  @override
  final String? email;
  final List<String> _methods;
  @override
  @JsonKey()
  List<String> get methods {
    if (_methods is EqualUnmodifiableListView) return _methods;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_methods);
  }

  @override
  final String? method;
  @override
  final String? confirmationCode;
  @override
  @JsonKey()
  final bool loading;
  @override
  @JsonKey()
  final bool error;

  @override
  String toString() {
    return 'AuthResetPasswordState(email: $email, methods: $methods, method: $method, confirmationCode: $confirmationCode, loading: $loading, error: $error)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_AuthResetPasswordState &&
            (identical(other.email, email) || other.email == email) &&
            const DeepCollectionEquality().equals(other._methods, _methods) &&
            (identical(other.method, method) || other.method == method) &&
            (identical(other.confirmationCode, confirmationCode) ||
                other.confirmationCode == confirmationCode) &&
            (identical(other.loading, loading) || other.loading == loading) &&
            (identical(other.error, error) || other.error == error));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      email,
      const DeepCollectionEquality().hash(_methods),
      method,
      confirmationCode,
      loading,
      error);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_AuthResetPasswordStateCopyWith<_$_AuthResetPasswordState> get copyWith =>
      __$$_AuthResetPasswordStateCopyWithImpl<_$_AuthResetPasswordState>(
          this, _$identity);
}

abstract class _AuthResetPasswordState implements AuthResetPasswordState {
  const factory _AuthResetPasswordState(
      {final String? email,
      final List<String> methods,
      final String? method,
      final String? confirmationCode,
      final bool loading,
      final bool error}) = _$_AuthResetPasswordState;

  @override
  String? get email;
  @override
  List<String> get methods;
  @override
  String? get method;
  @override
  String? get confirmationCode;
  @override
  bool get loading;
  @override
  bool get error;
  @override
  @JsonKey(ignore: true)
  _$$_AuthResetPasswordStateCopyWith<_$_AuthResetPasswordState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$AuthResetPasswordEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<String> methods) step1Success,
    required TResult Function(String method) step2Success,
    required TResult Function() step3Success,
    required TResult Function(String? message) unknownError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(List<String> methods)? step1Success,
    TResult? Function(String method)? step2Success,
    TResult? Function()? step3Success,
    TResult? Function(String? message)? unknownError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<String> methods)? step1Success,
    TResult Function(String method)? step2Success,
    TResult Function()? step3Success,
    TResult Function(String? message)? unknownError,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(AuthResetPasswordEventStep1Success value)
        step1Success,
    required TResult Function(AuthResetPasswordEventStep2Success value)
        step2Success,
    required TResult Function(AuthResetPasswordEventStep3Success value)
        step3Success,
    required TResult Function(AuthResetPasswordEventUnknownError value)
        unknownError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(AuthResetPasswordEventStep1Success value)? step1Success,
    TResult? Function(AuthResetPasswordEventStep2Success value)? step2Success,
    TResult? Function(AuthResetPasswordEventStep3Success value)? step3Success,
    TResult? Function(AuthResetPasswordEventUnknownError value)? unknownError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(AuthResetPasswordEventStep1Success value)? step1Success,
    TResult Function(AuthResetPasswordEventStep2Success value)? step2Success,
    TResult Function(AuthResetPasswordEventStep3Success value)? step3Success,
    TResult Function(AuthResetPasswordEventUnknownError value)? unknownError,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AuthResetPasswordEventCopyWith<$Res> {
  factory $AuthResetPasswordEventCopyWith(AuthResetPasswordEvent value,
          $Res Function(AuthResetPasswordEvent) then) =
      _$AuthResetPasswordEventCopyWithImpl<$Res, AuthResetPasswordEvent>;
}

/// @nodoc
class _$AuthResetPasswordEventCopyWithImpl<$Res,
        $Val extends AuthResetPasswordEvent>
    implements $AuthResetPasswordEventCopyWith<$Res> {
  _$AuthResetPasswordEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$AuthResetPasswordEventStep1SuccessCopyWith<$Res> {
  factory _$$AuthResetPasswordEventStep1SuccessCopyWith(
          _$AuthResetPasswordEventStep1Success value,
          $Res Function(_$AuthResetPasswordEventStep1Success) then) =
      __$$AuthResetPasswordEventStep1SuccessCopyWithImpl<$Res>;
  @useResult
  $Res call({List<String> methods});
}

/// @nodoc
class __$$AuthResetPasswordEventStep1SuccessCopyWithImpl<$Res>
    extends _$AuthResetPasswordEventCopyWithImpl<$Res,
        _$AuthResetPasswordEventStep1Success>
    implements _$$AuthResetPasswordEventStep1SuccessCopyWith<$Res> {
  __$$AuthResetPasswordEventStep1SuccessCopyWithImpl(
      _$AuthResetPasswordEventStep1Success _value,
      $Res Function(_$AuthResetPasswordEventStep1Success) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? methods = null,
  }) {
    return _then(_$AuthResetPasswordEventStep1Success(
      null == methods
          ? _value._methods
          : methods // ignore: cast_nullable_to_non_nullable
              as List<String>,
    ));
  }
}

/// @nodoc

class _$AuthResetPasswordEventStep1Success
    extends AuthResetPasswordEventStep1Success {
  _$AuthResetPasswordEventStep1Success(final List<String> methods)
      : _methods = methods,
        super._();

  final List<String> _methods;
  @override
  List<String> get methods {
    if (_methods is EqualUnmodifiableListView) return _methods;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_methods);
  }

  @override
  String toString() {
    return 'AuthResetPasswordEvent.step1Success(methods: $methods)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$AuthResetPasswordEventStep1Success &&
            const DeepCollectionEquality().equals(other._methods, _methods));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(_methods));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$AuthResetPasswordEventStep1SuccessCopyWith<
          _$AuthResetPasswordEventStep1Success>
      get copyWith => __$$AuthResetPasswordEventStep1SuccessCopyWithImpl<
          _$AuthResetPasswordEventStep1Success>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<String> methods) step1Success,
    required TResult Function(String method) step2Success,
    required TResult Function() step3Success,
    required TResult Function(String? message) unknownError,
  }) {
    return step1Success(methods);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(List<String> methods)? step1Success,
    TResult? Function(String method)? step2Success,
    TResult? Function()? step3Success,
    TResult? Function(String? message)? unknownError,
  }) {
    return step1Success?.call(methods);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<String> methods)? step1Success,
    TResult Function(String method)? step2Success,
    TResult Function()? step3Success,
    TResult Function(String? message)? unknownError,
    required TResult orElse(),
  }) {
    if (step1Success != null) {
      return step1Success(methods);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(AuthResetPasswordEventStep1Success value)
        step1Success,
    required TResult Function(AuthResetPasswordEventStep2Success value)
        step2Success,
    required TResult Function(AuthResetPasswordEventStep3Success value)
        step3Success,
    required TResult Function(AuthResetPasswordEventUnknownError value)
        unknownError,
  }) {
    return step1Success(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(AuthResetPasswordEventStep1Success value)? step1Success,
    TResult? Function(AuthResetPasswordEventStep2Success value)? step2Success,
    TResult? Function(AuthResetPasswordEventStep3Success value)? step3Success,
    TResult? Function(AuthResetPasswordEventUnknownError value)? unknownError,
  }) {
    return step1Success?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(AuthResetPasswordEventStep1Success value)? step1Success,
    TResult Function(AuthResetPasswordEventStep2Success value)? step2Success,
    TResult Function(AuthResetPasswordEventStep3Success value)? step3Success,
    TResult Function(AuthResetPasswordEventUnknownError value)? unknownError,
    required TResult orElse(),
  }) {
    if (step1Success != null) {
      return step1Success(this);
    }
    return orElse();
  }
}

abstract class AuthResetPasswordEventStep1Success
    extends AuthResetPasswordEvent {
  factory AuthResetPasswordEventStep1Success(final List<String> methods) =
      _$AuthResetPasswordEventStep1Success;
  AuthResetPasswordEventStep1Success._() : super._();

  List<String> get methods;
  @JsonKey(ignore: true)
  _$$AuthResetPasswordEventStep1SuccessCopyWith<
          _$AuthResetPasswordEventStep1Success>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$AuthResetPasswordEventStep2SuccessCopyWith<$Res> {
  factory _$$AuthResetPasswordEventStep2SuccessCopyWith(
          _$AuthResetPasswordEventStep2Success value,
          $Res Function(_$AuthResetPasswordEventStep2Success) then) =
      __$$AuthResetPasswordEventStep2SuccessCopyWithImpl<$Res>;
  @useResult
  $Res call({String method});
}

/// @nodoc
class __$$AuthResetPasswordEventStep2SuccessCopyWithImpl<$Res>
    extends _$AuthResetPasswordEventCopyWithImpl<$Res,
        _$AuthResetPasswordEventStep2Success>
    implements _$$AuthResetPasswordEventStep2SuccessCopyWith<$Res> {
  __$$AuthResetPasswordEventStep2SuccessCopyWithImpl(
      _$AuthResetPasswordEventStep2Success _value,
      $Res Function(_$AuthResetPasswordEventStep2Success) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? method = null,
  }) {
    return _then(_$AuthResetPasswordEventStep2Success(
      null == method
          ? _value.method
          : method // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$AuthResetPasswordEventStep2Success
    extends AuthResetPasswordEventStep2Success {
  _$AuthResetPasswordEventStep2Success(this.method) : super._();

  @override
  final String method;

  @override
  String toString() {
    return 'AuthResetPasswordEvent.step2Success(method: $method)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$AuthResetPasswordEventStep2Success &&
            (identical(other.method, method) || other.method == method));
  }

  @override
  int get hashCode => Object.hash(runtimeType, method);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$AuthResetPasswordEventStep2SuccessCopyWith<
          _$AuthResetPasswordEventStep2Success>
      get copyWith => __$$AuthResetPasswordEventStep2SuccessCopyWithImpl<
          _$AuthResetPasswordEventStep2Success>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<String> methods) step1Success,
    required TResult Function(String method) step2Success,
    required TResult Function() step3Success,
    required TResult Function(String? message) unknownError,
  }) {
    return step2Success(method);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(List<String> methods)? step1Success,
    TResult? Function(String method)? step2Success,
    TResult? Function()? step3Success,
    TResult? Function(String? message)? unknownError,
  }) {
    return step2Success?.call(method);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<String> methods)? step1Success,
    TResult Function(String method)? step2Success,
    TResult Function()? step3Success,
    TResult Function(String? message)? unknownError,
    required TResult orElse(),
  }) {
    if (step2Success != null) {
      return step2Success(method);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(AuthResetPasswordEventStep1Success value)
        step1Success,
    required TResult Function(AuthResetPasswordEventStep2Success value)
        step2Success,
    required TResult Function(AuthResetPasswordEventStep3Success value)
        step3Success,
    required TResult Function(AuthResetPasswordEventUnknownError value)
        unknownError,
  }) {
    return step2Success(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(AuthResetPasswordEventStep1Success value)? step1Success,
    TResult? Function(AuthResetPasswordEventStep2Success value)? step2Success,
    TResult? Function(AuthResetPasswordEventStep3Success value)? step3Success,
    TResult? Function(AuthResetPasswordEventUnknownError value)? unknownError,
  }) {
    return step2Success?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(AuthResetPasswordEventStep1Success value)? step1Success,
    TResult Function(AuthResetPasswordEventStep2Success value)? step2Success,
    TResult Function(AuthResetPasswordEventStep3Success value)? step3Success,
    TResult Function(AuthResetPasswordEventUnknownError value)? unknownError,
    required TResult orElse(),
  }) {
    if (step2Success != null) {
      return step2Success(this);
    }
    return orElse();
  }
}

abstract class AuthResetPasswordEventStep2Success
    extends AuthResetPasswordEvent {
  factory AuthResetPasswordEventStep2Success(final String method) =
      _$AuthResetPasswordEventStep2Success;
  AuthResetPasswordEventStep2Success._() : super._();

  String get method;
  @JsonKey(ignore: true)
  _$$AuthResetPasswordEventStep2SuccessCopyWith<
          _$AuthResetPasswordEventStep2Success>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$AuthResetPasswordEventStep3SuccessCopyWith<$Res> {
  factory _$$AuthResetPasswordEventStep3SuccessCopyWith(
          _$AuthResetPasswordEventStep3Success value,
          $Res Function(_$AuthResetPasswordEventStep3Success) then) =
      __$$AuthResetPasswordEventStep3SuccessCopyWithImpl<$Res>;
}

/// @nodoc
class __$$AuthResetPasswordEventStep3SuccessCopyWithImpl<$Res>
    extends _$AuthResetPasswordEventCopyWithImpl<$Res,
        _$AuthResetPasswordEventStep3Success>
    implements _$$AuthResetPasswordEventStep3SuccessCopyWith<$Res> {
  __$$AuthResetPasswordEventStep3SuccessCopyWithImpl(
      _$AuthResetPasswordEventStep3Success _value,
      $Res Function(_$AuthResetPasswordEventStep3Success) _then)
      : super(_value, _then);
}

/// @nodoc

class _$AuthResetPasswordEventStep3Success
    extends AuthResetPasswordEventStep3Success {
  _$AuthResetPasswordEventStep3Success() : super._();

  @override
  String toString() {
    return 'AuthResetPasswordEvent.step3Success()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$AuthResetPasswordEventStep3Success);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<String> methods) step1Success,
    required TResult Function(String method) step2Success,
    required TResult Function() step3Success,
    required TResult Function(String? message) unknownError,
  }) {
    return step3Success();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(List<String> methods)? step1Success,
    TResult? Function(String method)? step2Success,
    TResult? Function()? step3Success,
    TResult? Function(String? message)? unknownError,
  }) {
    return step3Success?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<String> methods)? step1Success,
    TResult Function(String method)? step2Success,
    TResult Function()? step3Success,
    TResult Function(String? message)? unknownError,
    required TResult orElse(),
  }) {
    if (step3Success != null) {
      return step3Success();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(AuthResetPasswordEventStep1Success value)
        step1Success,
    required TResult Function(AuthResetPasswordEventStep2Success value)
        step2Success,
    required TResult Function(AuthResetPasswordEventStep3Success value)
        step3Success,
    required TResult Function(AuthResetPasswordEventUnknownError value)
        unknownError,
  }) {
    return step3Success(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(AuthResetPasswordEventStep1Success value)? step1Success,
    TResult? Function(AuthResetPasswordEventStep2Success value)? step2Success,
    TResult? Function(AuthResetPasswordEventStep3Success value)? step3Success,
    TResult? Function(AuthResetPasswordEventUnknownError value)? unknownError,
  }) {
    return step3Success?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(AuthResetPasswordEventStep1Success value)? step1Success,
    TResult Function(AuthResetPasswordEventStep2Success value)? step2Success,
    TResult Function(AuthResetPasswordEventStep3Success value)? step3Success,
    TResult Function(AuthResetPasswordEventUnknownError value)? unknownError,
    required TResult orElse(),
  }) {
    if (step3Success != null) {
      return step3Success(this);
    }
    return orElse();
  }
}

abstract class AuthResetPasswordEventStep3Success
    extends AuthResetPasswordEvent {
  factory AuthResetPasswordEventStep3Success() =
      _$AuthResetPasswordEventStep3Success;
  AuthResetPasswordEventStep3Success._() : super._();
}

/// @nodoc
abstract class _$$AuthResetPasswordEventUnknownErrorCopyWith<$Res> {
  factory _$$AuthResetPasswordEventUnknownErrorCopyWith(
          _$AuthResetPasswordEventUnknownError value,
          $Res Function(_$AuthResetPasswordEventUnknownError) then) =
      __$$AuthResetPasswordEventUnknownErrorCopyWithImpl<$Res>;
  @useResult
  $Res call({String? message});
}

/// @nodoc
class __$$AuthResetPasswordEventUnknownErrorCopyWithImpl<$Res>
    extends _$AuthResetPasswordEventCopyWithImpl<$Res,
        _$AuthResetPasswordEventUnknownError>
    implements _$$AuthResetPasswordEventUnknownErrorCopyWith<$Res> {
  __$$AuthResetPasswordEventUnknownErrorCopyWithImpl(
      _$AuthResetPasswordEventUnknownError _value,
      $Res Function(_$AuthResetPasswordEventUnknownError) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? message = freezed,
  }) {
    return _then(_$AuthResetPasswordEventUnknownError(
      freezed == message
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$AuthResetPasswordEventUnknownError
    extends AuthResetPasswordEventUnknownError {
  _$AuthResetPasswordEventUnknownError([this.message]) : super._();

  @override
  final String? message;

  @override
  String toString() {
    return 'AuthResetPasswordEvent.unknownError(message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$AuthResetPasswordEventUnknownError &&
            (identical(other.message, message) || other.message == message));
  }

  @override
  int get hashCode => Object.hash(runtimeType, message);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$AuthResetPasswordEventUnknownErrorCopyWith<
          _$AuthResetPasswordEventUnknownError>
      get copyWith => __$$AuthResetPasswordEventUnknownErrorCopyWithImpl<
          _$AuthResetPasswordEventUnknownError>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<String> methods) step1Success,
    required TResult Function(String method) step2Success,
    required TResult Function() step3Success,
    required TResult Function(String? message) unknownError,
  }) {
    return unknownError(message);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(List<String> methods)? step1Success,
    TResult? Function(String method)? step2Success,
    TResult? Function()? step3Success,
    TResult? Function(String? message)? unknownError,
  }) {
    return unknownError?.call(message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<String> methods)? step1Success,
    TResult Function(String method)? step2Success,
    TResult Function()? step3Success,
    TResult Function(String? message)? unknownError,
    required TResult orElse(),
  }) {
    if (unknownError != null) {
      return unknownError(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(AuthResetPasswordEventStep1Success value)
        step1Success,
    required TResult Function(AuthResetPasswordEventStep2Success value)
        step2Success,
    required TResult Function(AuthResetPasswordEventStep3Success value)
        step3Success,
    required TResult Function(AuthResetPasswordEventUnknownError value)
        unknownError,
  }) {
    return unknownError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(AuthResetPasswordEventStep1Success value)? step1Success,
    TResult? Function(AuthResetPasswordEventStep2Success value)? step2Success,
    TResult? Function(AuthResetPasswordEventStep3Success value)? step3Success,
    TResult? Function(AuthResetPasswordEventUnknownError value)? unknownError,
  }) {
    return unknownError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(AuthResetPasswordEventStep1Success value)? step1Success,
    TResult Function(AuthResetPasswordEventStep2Success value)? step2Success,
    TResult Function(AuthResetPasswordEventStep3Success value)? step3Success,
    TResult Function(AuthResetPasswordEventUnknownError value)? unknownError,
    required TResult orElse(),
  }) {
    if (unknownError != null) {
      return unknownError(this);
    }
    return orElse();
  }
}

abstract class AuthResetPasswordEventUnknownError extends AuthResetPasswordEvent
    implements ErrorEvent {
  factory AuthResetPasswordEventUnknownError([final String? message]) =
      _$AuthResetPasswordEventUnknownError;
  AuthResetPasswordEventUnknownError._() : super._();

  String? get message;
  @JsonKey(ignore: true)
  _$$AuthResetPasswordEventUnknownErrorCopyWith<
          _$AuthResetPasswordEventUnknownError>
      get copyWith => throw _privateConstructorUsedError;
}
