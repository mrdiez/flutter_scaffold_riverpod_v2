import 'dart:async';

import 'package:dartx/dartx.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:flutter_scaffold_riverpod_v2/config/translations/gen/l10n.dart';
import 'package:flutter_scaffold_riverpod_v2/modules/auth/data/access/authentication_api.dart';
import 'package:flutter_scaffold_riverpod_v2/modules/auth/reset_password/auth_reset_password_io.dart';
import 'package:flutter_scaffold_riverpod_v2/utils/form.dart';

part 'auth_reset_password_logic.g.dart';

// To generate @riverpod classes (in filename.g.dart) run:
// flutter pub run build_runner build --delete-conflicting-outputs

// The Riverpod's generator will create a Provider for our logic class
// We can inject dependencies by using the "ref" property (Riverpod's WidgetRef)
// Note that our logic class will be automatically re-instantiated
// if we're using ref.watch and if this "watcher" is triggered

/// Logic class
@riverpod
class AuthResetPasswordLogic extends _$AuthResetPasswordLogic {
  // This method is called each time this logic class is built
  @override
  AuthResetPasswordState build() {
    // We tell our logic to not be disposed automatically
    // This way the state is saved and can be shared across multiple views that share same logic
    _link = ref.keepAlive();
    // We retrieve our object's repository instance
    _authenticationApi = ref.read(authenticationApiProvider);
    // We return our default view's state
    return const AuthResetPasswordState();
  }

  // The link that keeps our logic's state alive
  late final KeepAliveLink _link;

  // The instance of the api which will handle network and/or local data
  late final AuthenticationApi _authenticationApi;

  // The form key to access form's state to perform a validation check on save
  final step1FormKey = GlobalKey<FormBuilderState>();
  final step2FormKey = GlobalKey<FormBuilderState>();
  final step3FormKey = GlobalKey<FormBuilderState>();

  // Map inputs' values with their respective state's property
  void saveEmail(String? email) => state = state.copyWith(email: email?.trim());
  void saveMethod(String method) => state = state.copyWith(method: method);
  void saveConfirmationCode(String? confirmationCode) =>
      state = state.copyWith(confirmationCode: confirmationCode?.trim());

  Future<void> submitLogin() async {
    // Just to clean up unwanted spaces typed by the user
    step1FormKey.currentState?.trim();
    // Check if all fields are correct
    if (!(step1FormKey.currentState?.saveAndValidate() ?? false)) return;
    try {
      state = state.copyWith(loading: true);
      final methods = await _authenticationApi.getResetPasswordMethods(state.email!);
      AuthResetPasswordEvent.step1Success(methods);
    } catch (e, stackTrace) {
      debugPrint('[$runtimeType] ERROR\n$e\n$stackTrace');
      state = state.copyWith(error: true);
      AuthResetPasswordEvent.unknownError();
    }
    state = state.copyWith(loading: false);
  }

  Future<void> submitMethod() async {
    if (!(step2FormKey.currentState?.saveAndValidate() ?? false) || state.method.isNullOrBlank) return;
    try {
      state = state.copyWith(loading: true);
      await _authenticationApi.postResetPasswordMethod(state.email!, state.method!);
      AuthResetPasswordEvent.step2Success(state.method!);
    } catch (e, stackTrace) {
      debugPrint('[$runtimeType] ERROR\n$e\n$stackTrace');
      state = state.copyWith(error: true);
      AuthResetPasswordEvent.unknownError();
    }
    state = state.copyWith(loading: false);
  }

  Future<void> submitCode() async {
    // Just to clean up unwanted spaces typed by the user
    step3FormKey.currentState?.trim();
    // Check if all fields are correct
    if (!(step3FormKey.currentState?.saveAndValidate() ?? false)) return;
    try {
      state = state.copyWith(loading: true);
      await _authenticationApi.resetPassword(state.email!, state.confirmationCode!);
      AuthResetPasswordEvent.step3Success();
    } catch (e, stackTrace) {
      debugPrint('[$runtimeType] ERROR\n$e\n$stackTrace');
      state = state.copyWith(error: true);
      AuthResetPasswordEvent.unknownError();
    }
    state = state.copyWith(loading: false);
  }

  String? resetPasswordMethodValidator(String? method) {
    if (method.isNullOrBlank) return Translation.current.ThisFieldIsRequired;
    return null;
  }

  String? confirmationCodeValidator(String? code) {
    if (code.isNullOrBlank) return Translation.current.ThisFieldIsRequired;
    if (code!.length < 4) return Translation.current.nbCharactersMinimum(4);
    return null;
  }

  // We call this method when we want to clear the saved state
  void clear() {
    _link.close();
  }
}
