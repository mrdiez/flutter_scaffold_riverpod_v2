// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'auth_reset_password_logic.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$authResetPasswordLogicHash() =>
    r'b0248438a660481b8508e4012957ad8499a0e925';

/// Logic class
///
/// Copied from [AuthResetPasswordLogic].
@ProviderFor(AuthResetPasswordLogic)
final authResetPasswordLogicProvider = AutoDisposeNotifierProvider<
    AuthResetPasswordLogic, AuthResetPasswordState>.internal(
  AuthResetPasswordLogic.new,
  name: r'authResetPasswordLogicProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$authResetPasswordLogicHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$AuthResetPasswordLogic = AutoDisposeNotifier<AuthResetPasswordState>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member
