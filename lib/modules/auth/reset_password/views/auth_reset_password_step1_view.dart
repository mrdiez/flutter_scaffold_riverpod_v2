import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:flutter_scaffold_riverpod_v2/abstract/interfaces/error_event.dart';
import 'package:flutter_scaffold_riverpod_v2/abstract/mixins/event_provider.dart';
import 'package:flutter_scaffold_riverpod_v2/config/routes.dart';
import 'package:flutter_scaffold_riverpod_v2/config/theme/sizes.dart';
import 'package:flutter_scaffold_riverpod_v2/modules/auth/reset_password/auth_reset_password_io.dart';
import 'package:flutter_scaffold_riverpod_v2/modules/auth/reset_password/auth_reset_password_logic.dart';
import 'package:flutter_scaffold_riverpod_v2/modules/auth/widgets/auth_scaffold.dart';
import 'package:flutter_scaffold_riverpod_v2/utils/context.dart';
import 'package:flutter_scaffold_riverpod_v2/utils/form.dart';
import 'package:flutter_scaffold_riverpod_v2/widgets/buttons/common_button.dart';
import 'package:flutter_scaffold_riverpod_v2/widgets/buttons/common_text_button.dart';
import 'package:flutter_scaffold_riverpod_v2/widgets/inputs/animated_text_input.dart';

/// View class
class AuthResetPasswordStep1View extends ConsumerStatefulWidget {
  const AuthResetPasswordStep1View(this.email, {super.key});

  final String? email;

  @override
  ConsumerState<AuthResetPasswordStep1View> createState() => _AuthResetPasswordStep1ViewState();
}

class _AuthResetPasswordStep1ViewState extends ConsumerState<AuthResetPasswordStep1View> {
  // Our text fields controllers
  late final emailController = TextEditingController(text: widget.email);

  // Handle logic's output's events
  void onEvent(AuthResetPasswordEvent e) {
    e.maybeWhen(
      step1Success: (methods) => context.push(Routes.resetPasswordStep2, extra: {'methods': methods}),
      orElse: () {
        if (e is ErrorEvent) {
          return context.showErrorSnackBar((e as ErrorEvent).message ?? context.translation.UnknownError);
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    // Here we retrieve our logic's instance
    final logic = ref.read(authResetPasswordLogicProvider.notifier);
    // Starts listening our logic's output events
    EventProvider.listen<AuthResetPasswordEvent>(ref, onEvent: onEvent);

    return WillPopScope(
      // This method is called when the user tap the android's back button
      onWillPop: () async {
        // We dispose our logic class in order to clear any saved state
        logic.clear();
        return true;
      },
      child: AuthScaffold(
        title: context.translation.ForgotPassword,
        child: FormBuilder(
          key: logic.step1FormKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              AutoSizeText(context.translation.ForWhichAccount),
              $Gap.s,
              Consumer(
                builder: (context, ref, child) {
                  final isLoading = ref.watch(authResetPasswordLogicProvider.select((state) => state.loading));
                  return AnimatedTextInput(
                    controller: emailController,
                    validator: FormValidators.email,
                    decoration: InputDecoration(labelText: context.translation.Email),
                    onSaved: logic.saveEmail,
                    onFieldSubmitted: !isLoading ? (_) => logic.submitLogin() : null,
                  );
                },
              ),
              $Gap.s,
              Consumer(
                builder: (context, ref, child) {
                  final isLoading = ref.watch(authResetPasswordLogicProvider.select((state) => state.loading));
                  return Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Flexible(
                        child: CommonTextButton(
                          onTap: !isLoading
                              ? () {
                                  // We dispose our logic class in order to clear any saved state
                                  logic.clear();
                                  context.pop();
                                }
                              : null,
                          text: context.translation.Cancel,
                        ),
                      ),
                      Flexible(
                        child: CommonButton(
                          onTap: !isLoading ? logic.submitLogin : null,
                          text: context.translation.Submit,
                        ),
                      ),
                    ],
                  );
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
