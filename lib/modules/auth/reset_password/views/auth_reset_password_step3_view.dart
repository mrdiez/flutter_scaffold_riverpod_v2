import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:flutter_scaffold_riverpod_v2/abstract/interfaces/error_event.dart';
import 'package:flutter_scaffold_riverpod_v2/abstract/mixins/event_provider.dart';
import 'package:flutter_scaffold_riverpod_v2/config/routes.dart';
import 'package:flutter_scaffold_riverpod_v2/config/theme/sizes.dart';
import 'package:flutter_scaffold_riverpod_v2/modules/auth/reset_password/auth_reset_password_io.dart';
import 'package:flutter_scaffold_riverpod_v2/modules/auth/reset_password/auth_reset_password_logic.dart';
import 'package:flutter_scaffold_riverpod_v2/modules/auth/widgets/auth_scaffold.dart';
import 'package:flutter_scaffold_riverpod_v2/utils/context.dart';
import 'package:flutter_scaffold_riverpod_v2/widgets/buttons/common_button.dart';
import 'package:flutter_scaffold_riverpod_v2/widgets/buttons/common_text_button.dart';
import 'package:flutter_scaffold_riverpod_v2/widgets/inputs/animated_text_input.dart';

/// View class
class AuthResetPasswordStep3View extends ConsumerStatefulWidget {
  const AuthResetPasswordStep3View(this.method, {super.key});

  final String method;

  @override
  ConsumerState<AuthResetPasswordStep3View> createState() => _AuthResetPasswordStep3ViewState();
}

class _AuthResetPasswordStep3ViewState extends ConsumerState<AuthResetPasswordStep3View> {
  // Our text fields controllers
  final confirmationCodeController = TextEditingController();

  // Handle logic's output's events
  void onEvent(AuthResetPasswordEvent e, BuildContext context) {
    e.maybeWhen(
      step3Success: () => context.push(Routes.resetPasswordStep4),
      orElse: () {
        if (e is ErrorEvent) {
          return context.showErrorSnackBar((e as ErrorEvent).message ?? context.translation.UnknownError);
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    // Here we retrieve our logic's instance
    final logic = ref.watch(authResetPasswordLogicProvider.notifier);
    // Starts listening our logic's output events
    EventProvider.listen<AuthResetPasswordEvent>(ref, onEvent: (e) => onEvent(e, context));

    return AuthScaffold(
      title: context.translation.ForgotPassword,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          AutoSizeText(context.translation.ConfirmYourIdentity(widget.method)),
          $Gap.s,
          FormBuilder(
            key: logic.step3FormKey,
            child: Consumer(builder: (context, ref, child) {
              final isLoading = ref.watch(authResetPasswordLogicProvider.select((state) => state.loading));
              return AnimatedTextInput(
                controller: confirmationCodeController,
                keyboard: TextInputType.number,
                validator: logic.confirmationCodeValidator,
                onSaved: logic.saveConfirmationCode,
                onFieldSubmitted: !isLoading ? (_) => logic.submitCode() : null,
              );
            }),
          ),
          $Gap.s,
          Consumer(builder: (context, ref, child) {
            final isLoading = ref.watch(authResetPasswordLogicProvider.select((state) => state.loading));
            return Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Flexible(
                  child: CommonTextButton(
                    text: context.translation.Cancel,
                    onTap: !isLoading ? context.pop : null,
                  ),
                ),
                Flexible(
                  child: CommonButton(
                    text: context.translation.Submit,
                    onTap: !isLoading ? logic.submitCode : null,
                  ),
                ),
              ],
            );
          })
        ],
      ),
    );
  }
}
