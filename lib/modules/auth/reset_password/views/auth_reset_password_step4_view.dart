import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:flutter_scaffold_riverpod_v2/config/routes.dart';
import 'package:flutter_scaffold_riverpod_v2/config/theme/sizes.dart';
import 'package:flutter_scaffold_riverpod_v2/modules/auth/reset_password/auth_reset_password_logic.dart';
import 'package:flutter_scaffold_riverpod_v2/modules/auth/widgets/auth_scaffold.dart';
import 'package:flutter_scaffold_riverpod_v2/utils/context.dart';
import 'package:flutter_scaffold_riverpod_v2/widgets/buttons/common_button.dart';

/// View class
class AuthResetPasswordStep4View extends ConsumerWidget {
  const AuthResetPasswordStep4View({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final logic = ref.watch(authResetPasswordLogicProvider.notifier);
    final method = ref.read(authResetPasswordLogicProvider.select((state) => state.method));
    return AuthScaffold(
      title: context.translation.ForgotPassword,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          AutoSizeText(context.translation.YourNewPasswordHaveBeenSendBy(method!)),
          $Gap.s,
          CommonButton(
            text: context.translation.OK,
            onTap: () {
              context.showInformationSnackBar(context.translation.SorryNeedsToBeImplemented);
              // We dispose our logic class in order to clear any saved state
              logic.clear();
              context.go(Routes.auth);
            },
          ),
        ],
      ),
    );
  }
}
