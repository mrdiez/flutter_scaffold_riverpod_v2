import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter_scaffold_riverpod_v2/abstract/interfaces/error_event.dart';
import 'package:flutter_scaffold_riverpod_v2/abstract/mixins/event_provider.dart';

part 'auth_sign_up_io.freezed.dart';

// To generate @freezed classes (in filename.freezed.dart) run:
// flutter pub run build_runner build --delete-conflicting-outputs

/// View states
@freezed
class AuthSignUpState with _$AuthSignUpState {
  const factory AuthSignUpState({
    String? email,
    String? password,
    @Default(false) bool loading,
    @Default(false) bool error,
  }) = _AuthSignUpState;
}

/// Logic events
/// Define here input/output events which are coming to/from this module's logic class
@freezed
class AuthSignUpEvent with _$AuthSignUpEvent, EventProvider<AuthSignUpEvent> {
  AuthSignUpEvent._() {
    trigger();
  }

  // Here is our logic's output events
  factory AuthSignUpEvent.step1Success() = AuthSignUpEventStep1Success;
  factory AuthSignUpEvent.step2Success() = AuthSignUpEventStep2Success;

  // Error events can implement ErrorEvent in order to be able to check: if (event is ErrorEvent)
  @Implements<ErrorEvent>()
  factory AuthSignUpEvent.unknownError([String? message]) = AuthSignUpEventUnknownError;
}
