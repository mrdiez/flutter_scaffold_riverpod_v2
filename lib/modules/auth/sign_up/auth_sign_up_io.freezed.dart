// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'auth_sign_up_io.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$AuthSignUpState {
  String? get email => throw _privateConstructorUsedError;
  String? get password => throw _privateConstructorUsedError;
  bool get loading => throw _privateConstructorUsedError;
  bool get error => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $AuthSignUpStateCopyWith<AuthSignUpState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AuthSignUpStateCopyWith<$Res> {
  factory $AuthSignUpStateCopyWith(
          AuthSignUpState value, $Res Function(AuthSignUpState) then) =
      _$AuthSignUpStateCopyWithImpl<$Res, AuthSignUpState>;
  @useResult
  $Res call({String? email, String? password, bool loading, bool error});
}

/// @nodoc
class _$AuthSignUpStateCopyWithImpl<$Res, $Val extends AuthSignUpState>
    implements $AuthSignUpStateCopyWith<$Res> {
  _$AuthSignUpStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? email = freezed,
    Object? password = freezed,
    Object? loading = null,
    Object? error = null,
  }) {
    return _then(_value.copyWith(
      email: freezed == email
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String?,
      password: freezed == password
          ? _value.password
          : password // ignore: cast_nullable_to_non_nullable
              as String?,
      loading: null == loading
          ? _value.loading
          : loading // ignore: cast_nullable_to_non_nullable
              as bool,
      error: null == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_AuthSignUpStateCopyWith<$Res>
    implements $AuthSignUpStateCopyWith<$Res> {
  factory _$$_AuthSignUpStateCopyWith(
          _$_AuthSignUpState value, $Res Function(_$_AuthSignUpState) then) =
      __$$_AuthSignUpStateCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String? email, String? password, bool loading, bool error});
}

/// @nodoc
class __$$_AuthSignUpStateCopyWithImpl<$Res>
    extends _$AuthSignUpStateCopyWithImpl<$Res, _$_AuthSignUpState>
    implements _$$_AuthSignUpStateCopyWith<$Res> {
  __$$_AuthSignUpStateCopyWithImpl(
      _$_AuthSignUpState _value, $Res Function(_$_AuthSignUpState) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? email = freezed,
    Object? password = freezed,
    Object? loading = null,
    Object? error = null,
  }) {
    return _then(_$_AuthSignUpState(
      email: freezed == email
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String?,
      password: freezed == password
          ? _value.password
          : password // ignore: cast_nullable_to_non_nullable
              as String?,
      loading: null == loading
          ? _value.loading
          : loading // ignore: cast_nullable_to_non_nullable
              as bool,
      error: null == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$_AuthSignUpState implements _AuthSignUpState {
  const _$_AuthSignUpState(
      {this.email, this.password, this.loading = false, this.error = false});

  @override
  final String? email;
  @override
  final String? password;
  @override
  @JsonKey()
  final bool loading;
  @override
  @JsonKey()
  final bool error;

  @override
  String toString() {
    return 'AuthSignUpState(email: $email, password: $password, loading: $loading, error: $error)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_AuthSignUpState &&
            (identical(other.email, email) || other.email == email) &&
            (identical(other.password, password) ||
                other.password == password) &&
            (identical(other.loading, loading) || other.loading == loading) &&
            (identical(other.error, error) || other.error == error));
  }

  @override
  int get hashCode => Object.hash(runtimeType, email, password, loading, error);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_AuthSignUpStateCopyWith<_$_AuthSignUpState> get copyWith =>
      __$$_AuthSignUpStateCopyWithImpl<_$_AuthSignUpState>(this, _$identity);
}

abstract class _AuthSignUpState implements AuthSignUpState {
  const factory _AuthSignUpState(
      {final String? email,
      final String? password,
      final bool loading,
      final bool error}) = _$_AuthSignUpState;

  @override
  String? get email;
  @override
  String? get password;
  @override
  bool get loading;
  @override
  bool get error;
  @override
  @JsonKey(ignore: true)
  _$$_AuthSignUpStateCopyWith<_$_AuthSignUpState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$AuthSignUpEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() step1Success,
    required TResult Function() step2Success,
    required TResult Function(String? message) unknownError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? step1Success,
    TResult? Function()? step2Success,
    TResult? Function(String? message)? unknownError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? step1Success,
    TResult Function()? step2Success,
    TResult Function(String? message)? unknownError,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(AuthSignUpEventStep1Success value) step1Success,
    required TResult Function(AuthSignUpEventStep2Success value) step2Success,
    required TResult Function(AuthSignUpEventUnknownError value) unknownError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(AuthSignUpEventStep1Success value)? step1Success,
    TResult? Function(AuthSignUpEventStep2Success value)? step2Success,
    TResult? Function(AuthSignUpEventUnknownError value)? unknownError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(AuthSignUpEventStep1Success value)? step1Success,
    TResult Function(AuthSignUpEventStep2Success value)? step2Success,
    TResult Function(AuthSignUpEventUnknownError value)? unknownError,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AuthSignUpEventCopyWith<$Res> {
  factory $AuthSignUpEventCopyWith(
          AuthSignUpEvent value, $Res Function(AuthSignUpEvent) then) =
      _$AuthSignUpEventCopyWithImpl<$Res, AuthSignUpEvent>;
}

/// @nodoc
class _$AuthSignUpEventCopyWithImpl<$Res, $Val extends AuthSignUpEvent>
    implements $AuthSignUpEventCopyWith<$Res> {
  _$AuthSignUpEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$AuthSignUpEventStep1SuccessCopyWith<$Res> {
  factory _$$AuthSignUpEventStep1SuccessCopyWith(
          _$AuthSignUpEventStep1Success value,
          $Res Function(_$AuthSignUpEventStep1Success) then) =
      __$$AuthSignUpEventStep1SuccessCopyWithImpl<$Res>;
}

/// @nodoc
class __$$AuthSignUpEventStep1SuccessCopyWithImpl<$Res>
    extends _$AuthSignUpEventCopyWithImpl<$Res, _$AuthSignUpEventStep1Success>
    implements _$$AuthSignUpEventStep1SuccessCopyWith<$Res> {
  __$$AuthSignUpEventStep1SuccessCopyWithImpl(
      _$AuthSignUpEventStep1Success _value,
      $Res Function(_$AuthSignUpEventStep1Success) _then)
      : super(_value, _then);
}

/// @nodoc

class _$AuthSignUpEventStep1Success extends AuthSignUpEventStep1Success {
  _$AuthSignUpEventStep1Success() : super._();

  @override
  String toString() {
    return 'AuthSignUpEvent.step1Success()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$AuthSignUpEventStep1Success);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() step1Success,
    required TResult Function() step2Success,
    required TResult Function(String? message) unknownError,
  }) {
    return step1Success();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? step1Success,
    TResult? Function()? step2Success,
    TResult? Function(String? message)? unknownError,
  }) {
    return step1Success?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? step1Success,
    TResult Function()? step2Success,
    TResult Function(String? message)? unknownError,
    required TResult orElse(),
  }) {
    if (step1Success != null) {
      return step1Success();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(AuthSignUpEventStep1Success value) step1Success,
    required TResult Function(AuthSignUpEventStep2Success value) step2Success,
    required TResult Function(AuthSignUpEventUnknownError value) unknownError,
  }) {
    return step1Success(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(AuthSignUpEventStep1Success value)? step1Success,
    TResult? Function(AuthSignUpEventStep2Success value)? step2Success,
    TResult? Function(AuthSignUpEventUnknownError value)? unknownError,
  }) {
    return step1Success?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(AuthSignUpEventStep1Success value)? step1Success,
    TResult Function(AuthSignUpEventStep2Success value)? step2Success,
    TResult Function(AuthSignUpEventUnknownError value)? unknownError,
    required TResult orElse(),
  }) {
    if (step1Success != null) {
      return step1Success(this);
    }
    return orElse();
  }
}

abstract class AuthSignUpEventStep1Success extends AuthSignUpEvent {
  factory AuthSignUpEventStep1Success() = _$AuthSignUpEventStep1Success;
  AuthSignUpEventStep1Success._() : super._();
}

/// @nodoc
abstract class _$$AuthSignUpEventStep2SuccessCopyWith<$Res> {
  factory _$$AuthSignUpEventStep2SuccessCopyWith(
          _$AuthSignUpEventStep2Success value,
          $Res Function(_$AuthSignUpEventStep2Success) then) =
      __$$AuthSignUpEventStep2SuccessCopyWithImpl<$Res>;
}

/// @nodoc
class __$$AuthSignUpEventStep2SuccessCopyWithImpl<$Res>
    extends _$AuthSignUpEventCopyWithImpl<$Res, _$AuthSignUpEventStep2Success>
    implements _$$AuthSignUpEventStep2SuccessCopyWith<$Res> {
  __$$AuthSignUpEventStep2SuccessCopyWithImpl(
      _$AuthSignUpEventStep2Success _value,
      $Res Function(_$AuthSignUpEventStep2Success) _then)
      : super(_value, _then);
}

/// @nodoc

class _$AuthSignUpEventStep2Success extends AuthSignUpEventStep2Success {
  _$AuthSignUpEventStep2Success() : super._();

  @override
  String toString() {
    return 'AuthSignUpEvent.step2Success()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$AuthSignUpEventStep2Success);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() step1Success,
    required TResult Function() step2Success,
    required TResult Function(String? message) unknownError,
  }) {
    return step2Success();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? step1Success,
    TResult? Function()? step2Success,
    TResult? Function(String? message)? unknownError,
  }) {
    return step2Success?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? step1Success,
    TResult Function()? step2Success,
    TResult Function(String? message)? unknownError,
    required TResult orElse(),
  }) {
    if (step2Success != null) {
      return step2Success();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(AuthSignUpEventStep1Success value) step1Success,
    required TResult Function(AuthSignUpEventStep2Success value) step2Success,
    required TResult Function(AuthSignUpEventUnknownError value) unknownError,
  }) {
    return step2Success(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(AuthSignUpEventStep1Success value)? step1Success,
    TResult? Function(AuthSignUpEventStep2Success value)? step2Success,
    TResult? Function(AuthSignUpEventUnknownError value)? unknownError,
  }) {
    return step2Success?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(AuthSignUpEventStep1Success value)? step1Success,
    TResult Function(AuthSignUpEventStep2Success value)? step2Success,
    TResult Function(AuthSignUpEventUnknownError value)? unknownError,
    required TResult orElse(),
  }) {
    if (step2Success != null) {
      return step2Success(this);
    }
    return orElse();
  }
}

abstract class AuthSignUpEventStep2Success extends AuthSignUpEvent {
  factory AuthSignUpEventStep2Success() = _$AuthSignUpEventStep2Success;
  AuthSignUpEventStep2Success._() : super._();
}

/// @nodoc
abstract class _$$AuthSignUpEventUnknownErrorCopyWith<$Res> {
  factory _$$AuthSignUpEventUnknownErrorCopyWith(
          _$AuthSignUpEventUnknownError value,
          $Res Function(_$AuthSignUpEventUnknownError) then) =
      __$$AuthSignUpEventUnknownErrorCopyWithImpl<$Res>;
  @useResult
  $Res call({String? message});
}

/// @nodoc
class __$$AuthSignUpEventUnknownErrorCopyWithImpl<$Res>
    extends _$AuthSignUpEventCopyWithImpl<$Res, _$AuthSignUpEventUnknownError>
    implements _$$AuthSignUpEventUnknownErrorCopyWith<$Res> {
  __$$AuthSignUpEventUnknownErrorCopyWithImpl(
      _$AuthSignUpEventUnknownError _value,
      $Res Function(_$AuthSignUpEventUnknownError) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? message = freezed,
  }) {
    return _then(_$AuthSignUpEventUnknownError(
      freezed == message
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$AuthSignUpEventUnknownError extends AuthSignUpEventUnknownError {
  _$AuthSignUpEventUnknownError([this.message]) : super._();

  @override
  final String? message;

  @override
  String toString() {
    return 'AuthSignUpEvent.unknownError(message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$AuthSignUpEventUnknownError &&
            (identical(other.message, message) || other.message == message));
  }

  @override
  int get hashCode => Object.hash(runtimeType, message);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$AuthSignUpEventUnknownErrorCopyWith<_$AuthSignUpEventUnknownError>
      get copyWith => __$$AuthSignUpEventUnknownErrorCopyWithImpl<
          _$AuthSignUpEventUnknownError>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() step1Success,
    required TResult Function() step2Success,
    required TResult Function(String? message) unknownError,
  }) {
    return unknownError(message);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? step1Success,
    TResult? Function()? step2Success,
    TResult? Function(String? message)? unknownError,
  }) {
    return unknownError?.call(message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? step1Success,
    TResult Function()? step2Success,
    TResult Function(String? message)? unknownError,
    required TResult orElse(),
  }) {
    if (unknownError != null) {
      return unknownError(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(AuthSignUpEventStep1Success value) step1Success,
    required TResult Function(AuthSignUpEventStep2Success value) step2Success,
    required TResult Function(AuthSignUpEventUnknownError value) unknownError,
  }) {
    return unknownError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(AuthSignUpEventStep1Success value)? step1Success,
    TResult? Function(AuthSignUpEventStep2Success value)? step2Success,
    TResult? Function(AuthSignUpEventUnknownError value)? unknownError,
  }) {
    return unknownError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(AuthSignUpEventStep1Success value)? step1Success,
    TResult Function(AuthSignUpEventStep2Success value)? step2Success,
    TResult Function(AuthSignUpEventUnknownError value)? unknownError,
    required TResult orElse(),
  }) {
    if (unknownError != null) {
      return unknownError(this);
    }
    return orElse();
  }
}

abstract class AuthSignUpEventUnknownError extends AuthSignUpEvent
    implements ErrorEvent {
  factory AuthSignUpEventUnknownError([final String? message]) =
      _$AuthSignUpEventUnknownError;
  AuthSignUpEventUnknownError._() : super._();

  String? get message;
  @JsonKey(ignore: true)
  _$$AuthSignUpEventUnknownErrorCopyWith<_$AuthSignUpEventUnknownError>
      get copyWith => throw _privateConstructorUsedError;
}
