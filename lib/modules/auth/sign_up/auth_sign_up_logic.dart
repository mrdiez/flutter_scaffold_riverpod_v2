import 'dart:async';

import 'package:dartx/dartx.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:flutter_scaffold_riverpod_v2/config/translations/gen/l10n.dart';
import 'package:flutter_scaffold_riverpod_v2/modules/auth/data/access/authentication_api.dart';
import 'package:flutter_scaffold_riverpod_v2/modules/auth/sign_up/auth_sign_up_io.dart';
import 'package:flutter_scaffold_riverpod_v2/utils/form.dart';

part 'auth_sign_up_logic.g.dart';

// To generate @riverpod classes (in filename.g.dart) run:
// flutter pub run build_runner build --delete-conflicting-outputs

// The Riverpod's generator will create a Provider for our logic class
// We can inject dependencies by using the "ref" property (Riverpod's WidgetRef)
// Note that our logic class will be automatically re-instantiated
// if we're using ref.watch and if this "watcher" is triggered

/// Logic class
@riverpod
class AuthSignUpLogic extends _$AuthSignUpLogic {
  // This method is called each time this logic class is built
  @override
  AuthSignUpState build() {
    // We retrieve our object's repository instance
    _authenticationApi = ref.read(authenticationApiProvider);
    // We return our default view's state
    return const AuthSignUpState();
  }

  // The instance of the api which will handle network and/or local data
  late final AuthenticationApi _authenticationApi;

  // Map inputs' values with their respective state's property
  void onEmailChanged(String? email) => state = state.copyWith(email: email?.trim());
  void onPasswordChanged(String? password) => state = state.copyWith(password: password?.trim());

  Future<void> submit(FormBuilderState? formState) async {
    // Just to clean up unwanted spaces typed by the user
    formState?.trim();
    // Check if all fields are correct
    if (!(formState?.validate() ?? false)) return;
    try {
      state = state.copyWith(loading: true);
      await _authenticationApi.signUp(state.email!, state.password!);
      state = state.copyWith(loading: false);
      AuthSignUpEvent.step1Success();
    } catch (e, stackTrace) {
      debugPrint('[$runtimeType] ERROR\n$e\n$stackTrace');
      state = state.copyWith(loading: false, error: true);
      AuthSignUpEvent.unknownError();
    }
  }

  String? confirmPasswordValidator(String? confirmPassword) {
    if (confirmPassword.isNullOrBlank) return Translation.current.ThisFieldIsRequired;
    if (confirmPassword != state.password) return Translation.current.PasswordsMustBeIdentical;
    return null;
  }
}
