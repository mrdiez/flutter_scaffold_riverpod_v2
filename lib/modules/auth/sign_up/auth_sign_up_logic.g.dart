// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'auth_sign_up_logic.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$authSignUpLogicHash() => r'57750a3753178719751c502ce349e1599fce945b';

/// Logic class
///
/// Copied from [AuthSignUpLogic].
@ProviderFor(AuthSignUpLogic)
final authSignUpLogicProvider =
    AutoDisposeNotifierProvider<AuthSignUpLogic, AuthSignUpState>.internal(
  AuthSignUpLogic.new,
  name: r'authSignUpLogicProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$authSignUpLogicHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$AuthSignUpLogic = AutoDisposeNotifier<AuthSignUpState>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member
