import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:flutter_scaffold_riverpod_v2/abstract/interfaces/error_event.dart';
import 'package:flutter_scaffold_riverpod_v2/abstract/mixins/event_provider.dart';
import 'package:flutter_scaffold_riverpod_v2/config/routes.dart';
import 'package:flutter_scaffold_riverpod_v2/config/theme/sizes.dart';
import 'package:flutter_scaffold_riverpod_v2/modules/auth/sign_up/auth_sign_up_io.dart';
import 'package:flutter_scaffold_riverpod_v2/modules/auth/sign_up/auth_sign_up_logic.dart';
import 'package:flutter_scaffold_riverpod_v2/modules/auth/widgets/auth_scaffold.dart';
import 'package:flutter_scaffold_riverpod_v2/utils/context.dart';
import 'package:flutter_scaffold_riverpod_v2/utils/form.dart';
import 'package:flutter_scaffold_riverpod_v2/widgets/buttons/common_button.dart';
import 'package:flutter_scaffold_riverpod_v2/widgets/buttons/common_text_button.dart';
import 'package:flutter_scaffold_riverpod_v2/widgets/inputs/animated_text_input.dart';

/// View class
class AuthSignUpStep1View extends ConsumerStatefulWidget {
  const AuthSignUpStep1View({super.key});

  @override
  ConsumerState<AuthSignUpStep1View> createState() => _AuthSignUpStep1ViewState();
}

class _AuthSignUpStep1ViewState extends ConsumerState<AuthSignUpStep1View> {
  // The form key to access form's state to perform a validation check on save
  final formKey = GlobalKey<FormBuilderState>();

  // Our text fields controllers
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  final confirmPasswordController = TextEditingController();

  // Our text fields focus nodes (to auto focus next field on submit)
  final passwordFocusNode = FocusNode();
  final confirmPasswordFocusNode = FocusNode();

  void onEvent(AuthSignUpEvent e, BuildContext context) {
    e.maybeWhen(
      step1Success: () => context.go(Routes.signUpStep2),
      orElse: () {
        if (e is ErrorEvent) {
          return context.showErrorSnackBar((e as ErrorEvent).message ?? context.translation.UnknownError);
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    // Here we retrieve our logic's instance
    final logic = ref.watch(authSignUpLogicProvider.notifier);
    // Starts listening our logic's output events
    EventProvider.listen<AuthSignUpEvent>(ref, onEvent: (e) => onEvent(e, context));

    return AuthScaffold(
      title: context.translation.SignUp,
      child: FormBuilder(
        key: formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            AnimatedTextInput(
              controller: emailController,
              validator: FormValidators.email,
              decoration: InputDecoration(labelText: context.translation.Email),
              onChanged: logic.onEmailChanged,
              onFieldSubmitted: (_) => passwordFocusNode.requestFocus(),
            ),
            $Gap.xs,
            AnimatedTextInput(
              controller: passwordController,
              obscureText: true,
              focusNode: passwordFocusNode,
              validator: FormValidators.password,
              decoration: InputDecoration(labelText: context.translation.Password),
              onChanged: logic.onPasswordChanged,
              onFieldSubmitted: (_) => confirmPasswordFocusNode.requestFocus(),
            ),
            $Gap.xs,
            Consumer(
              builder: (context, ref, child) {
                final isLoading = ref.watch(authSignUpLogicProvider.select((state) => state.loading));
                return AnimatedTextInput(
                  controller: confirmPasswordController,
                  obscureText: true,
                  focusNode: confirmPasswordFocusNode,
                  validator: logic.confirmPasswordValidator,
                  decoration: InputDecoration(labelText: context.translation.ConfirmPassword),
                  onFieldSubmitted: !isLoading ? (_) => logic.submit(formKey.currentState) : null,
                );
              },
            ),
            $Gap.s,
            Consumer(
              builder: (context, ref, child) {
                final isLoading = ref.watch(authSignUpLogicProvider.select((state) => state.loading));
                return Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Flexible(
                      child: CommonTextButton(
                        onTap: !isLoading ? context.pop : null,
                        text: context.translation.Cancel,
                      ),
                    ),
                    Flexible(
                      child: CommonButton(
                        onTap: !isLoading ? () => logic.submit(formKey.currentState) : null,
                        text: context.translation.Submit,
                      ),
                    ),
                  ],
                );
              },
            )
          ],
        ),
      ),
    );
  }
}
