import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:flutter_scaffold_riverpod_v2/config/routes.dart';
import 'package:flutter_scaffold_riverpod_v2/config/theme/sizes.dart';
import 'package:flutter_scaffold_riverpod_v2/modules/auth/widgets/auth_scaffold.dart';
import 'package:flutter_scaffold_riverpod_v2/utils/context.dart';
import 'package:flutter_scaffold_riverpod_v2/widgets/buttons/common_button.dart';

/// View class
class AuthSignUpStep2View extends StatelessWidget {
  const AuthSignUpStep2View({super.key});

  @override
  Widget build(BuildContext context) {
    return AuthScaffold(
      title: context.translation.SignUp,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          AutoSizeText(context.translation.PleaseConfirmYourRegistration),
          $Gap.s,
          CommonButton(
            text: context.translation.OK,
            onTap: () {
              context.showInformationSnackBar(context.translation.SorryNeedsToBeImplemented);
              context.go(Routes.auth);
            },
          ),
        ],
      ),
    );
  }
}
