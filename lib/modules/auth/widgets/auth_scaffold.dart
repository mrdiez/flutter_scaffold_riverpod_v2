import 'package:flutter/material.dart';
import 'package:flutter_scaffold_riverpod_v2/config/assets.gen.dart';
import 'package:flutter_scaffold_riverpod_v2/config/theme/sizes.dart';
import 'package:flutter_scaffold_riverpod_v2/utils/context.dart';
import 'package:flutter_scaffold_riverpod_v2/widgets/layout/rounded_container.dart';
import 'package:flutter_scaffold_riverpod_v2/widgets/texts/section_title.dart';

/// A root widget for all auth views
class AuthScaffold extends StatelessWidget {
  const AuthScaffold({super.key, required this.title, required this.child});

  final String title;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Stack(
          alignment: Alignment.topCenter,
          children: [
            Padding(
              padding: const EdgeInsets.all($Padding.m),
              child: Hero(
                tag: 'auth_logo',
                child: ClipRRect(
                  borderRadius: const BorderRadius.all($Radius.m),
                  child: Assets.logos.logoVerticalColor.image(width: $Image.s),
                ),
              ),
            ),
            Center(
              child: Hero(
                tag: 'auth_rounded_container',
                child: Container(
                  decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: context.theme.colorScheme.background,
                        spreadRadius: 0,
                        blurRadius: $ShadowSize.xxl,
                      )
                    ],
                  ),
                  child: SingleChildScrollView(
                    child: Padding(
                      padding: const EdgeInsets.all($Padding.m),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SectionTitle(text: title),
                          $Gap.m,
                          RoundedContainer(child: child),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
