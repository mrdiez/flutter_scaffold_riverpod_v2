import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter_scaffold_riverpod_v2/abstract/interfaces/error_event.dart';
import 'package:flutter_scaffold_riverpod_v2/abstract/mixins/event_provider.dart';

part 'home_io.freezed.dart';

// To generate @freezed classes (in filename.freezed.dart) run:
// flutter pub run build_runner build --delete-conflicting-outputs

/// Logic events
/// Define here input/output events which are coming to/from this module's logic class
@freezed
class HomeEvent with _$HomeEvent, EventProvider<HomeEvent> {
  HomeEvent._() {
    trigger();
  }

  // Here is our logic's output events
  factory HomeEvent.loggedOut() = HomeEventLoggedOut;

  // Error events can implement ErrorEvent in order to be able to check: if (event is ErrorEvent)
  @Implements<ErrorEvent>()
  factory HomeEvent.unknownError([String? message]) = HomeEventUnknownError;
}
