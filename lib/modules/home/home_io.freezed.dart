// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'home_io.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$HomeEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loggedOut,
    required TResult Function(String? message) unknownError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? loggedOut,
    TResult? Function(String? message)? unknownError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loggedOut,
    TResult Function(String? message)? unknownError,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(HomeEventLoggedOut value) loggedOut,
    required TResult Function(HomeEventUnknownError value) unknownError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(HomeEventLoggedOut value)? loggedOut,
    TResult? Function(HomeEventUnknownError value)? unknownError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(HomeEventLoggedOut value)? loggedOut,
    TResult Function(HomeEventUnknownError value)? unknownError,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $HomeEventCopyWith<$Res> {
  factory $HomeEventCopyWith(HomeEvent value, $Res Function(HomeEvent) then) =
      _$HomeEventCopyWithImpl<$Res, HomeEvent>;
}

/// @nodoc
class _$HomeEventCopyWithImpl<$Res, $Val extends HomeEvent>
    implements $HomeEventCopyWith<$Res> {
  _$HomeEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$HomeEventLoggedOutCopyWith<$Res> {
  factory _$$HomeEventLoggedOutCopyWith(_$HomeEventLoggedOut value,
          $Res Function(_$HomeEventLoggedOut) then) =
      __$$HomeEventLoggedOutCopyWithImpl<$Res>;
}

/// @nodoc
class __$$HomeEventLoggedOutCopyWithImpl<$Res>
    extends _$HomeEventCopyWithImpl<$Res, _$HomeEventLoggedOut>
    implements _$$HomeEventLoggedOutCopyWith<$Res> {
  __$$HomeEventLoggedOutCopyWithImpl(
      _$HomeEventLoggedOut _value, $Res Function(_$HomeEventLoggedOut) _then)
      : super(_value, _then);
}

/// @nodoc

class _$HomeEventLoggedOut extends HomeEventLoggedOut {
  _$HomeEventLoggedOut() : super._();

  @override
  String toString() {
    return 'HomeEvent.loggedOut()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$HomeEventLoggedOut);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loggedOut,
    required TResult Function(String? message) unknownError,
  }) {
    return loggedOut();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? loggedOut,
    TResult? Function(String? message)? unknownError,
  }) {
    return loggedOut?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loggedOut,
    TResult Function(String? message)? unknownError,
    required TResult orElse(),
  }) {
    if (loggedOut != null) {
      return loggedOut();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(HomeEventLoggedOut value) loggedOut,
    required TResult Function(HomeEventUnknownError value) unknownError,
  }) {
    return loggedOut(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(HomeEventLoggedOut value)? loggedOut,
    TResult? Function(HomeEventUnknownError value)? unknownError,
  }) {
    return loggedOut?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(HomeEventLoggedOut value)? loggedOut,
    TResult Function(HomeEventUnknownError value)? unknownError,
    required TResult orElse(),
  }) {
    if (loggedOut != null) {
      return loggedOut(this);
    }
    return orElse();
  }
}

abstract class HomeEventLoggedOut extends HomeEvent {
  factory HomeEventLoggedOut() = _$HomeEventLoggedOut;
  HomeEventLoggedOut._() : super._();
}

/// @nodoc
abstract class _$$HomeEventUnknownErrorCopyWith<$Res> {
  factory _$$HomeEventUnknownErrorCopyWith(_$HomeEventUnknownError value,
          $Res Function(_$HomeEventUnknownError) then) =
      __$$HomeEventUnknownErrorCopyWithImpl<$Res>;
  @useResult
  $Res call({String? message});
}

/// @nodoc
class __$$HomeEventUnknownErrorCopyWithImpl<$Res>
    extends _$HomeEventCopyWithImpl<$Res, _$HomeEventUnknownError>
    implements _$$HomeEventUnknownErrorCopyWith<$Res> {
  __$$HomeEventUnknownErrorCopyWithImpl(_$HomeEventUnknownError _value,
      $Res Function(_$HomeEventUnknownError) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? message = freezed,
  }) {
    return _then(_$HomeEventUnknownError(
      freezed == message
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$HomeEventUnknownError extends HomeEventUnknownError {
  _$HomeEventUnknownError([this.message]) : super._();

  @override
  final String? message;

  @override
  String toString() {
    return 'HomeEvent.unknownError(message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$HomeEventUnknownError &&
            (identical(other.message, message) || other.message == message));
  }

  @override
  int get hashCode => Object.hash(runtimeType, message);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$HomeEventUnknownErrorCopyWith<_$HomeEventUnknownError> get copyWith =>
      __$$HomeEventUnknownErrorCopyWithImpl<_$HomeEventUnknownError>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loggedOut,
    required TResult Function(String? message) unknownError,
  }) {
    return unknownError(message);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? loggedOut,
    TResult? Function(String? message)? unknownError,
  }) {
    return unknownError?.call(message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loggedOut,
    TResult Function(String? message)? unknownError,
    required TResult orElse(),
  }) {
    if (unknownError != null) {
      return unknownError(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(HomeEventLoggedOut value) loggedOut,
    required TResult Function(HomeEventUnknownError value) unknownError,
  }) {
    return unknownError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(HomeEventLoggedOut value)? loggedOut,
    TResult? Function(HomeEventUnknownError value)? unknownError,
  }) {
    return unknownError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(HomeEventLoggedOut value)? loggedOut,
    TResult Function(HomeEventUnknownError value)? unknownError,
    required TResult orElse(),
  }) {
    if (unknownError != null) {
      return unknownError(this);
    }
    return orElse();
  }
}

abstract class HomeEventUnknownError extends HomeEvent implements ErrorEvent {
  factory HomeEventUnknownError([final String? message]) =
      _$HomeEventUnknownError;
  HomeEventUnknownError._() : super._();

  String? get message;
  @JsonKey(ignore: true)
  _$$HomeEventUnknownErrorCopyWith<_$HomeEventUnknownError> get copyWith =>
      throw _privateConstructorUsedError;
}
