import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:flutter_scaffold_riverpod_v2/data/access/session_repository.dart';
import 'package:flutter_scaffold_riverpod_v2/modules/home/home_io.dart';

part 'home_logic.g.dart';

// To generate @riverpod classes (in filename.g.dart) run:
// flutter pub run build_runner build --delete-conflicting-outputs

// The Riverpod's generator will create a Provider for our logic class
// We can inject dependencies by using the "ref" property (Riverpod's WidgetRef)
// Note that our logic class will be automatically re-instantiated
// if we're using ref.watch and if this "watcher" is triggered

/// Logic class
@riverpod
class HomeLogic extends _$HomeLogic {
  // This method is called each time this logic class is built
  @override
  void build() {
    // We retrieve our object's repository instance
    sessionRepository = ref.watch(sessionRepositoryProvider);
  }

  // The instance of the repository which will handle network and/or local data
  late final SessionRepository sessionRepository;

  Future<void> logOut() async {
    await sessionRepository.delete();
    HomeEventLoggedOut();
  }
}
