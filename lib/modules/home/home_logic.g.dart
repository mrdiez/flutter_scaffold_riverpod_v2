// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_logic.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$homeLogicHash() => r'5448587ee1b4905acc3047a034b05b7841ea49c0';

/// Logic class
///
/// Copied from [HomeLogic].
@ProviderFor(HomeLogic)
final homeLogicProvider = AutoDisposeNotifierProvider<HomeLogic, void>.internal(
  HomeLogic.new,
  name: r'homeLogicProvider',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$homeLogicHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$HomeLogic = AutoDisposeNotifier<void>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member
