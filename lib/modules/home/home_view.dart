import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:flutter_scaffold_riverpod_v2/abstract/interfaces/error_event.dart';
import 'package:flutter_scaffold_riverpod_v2/abstract/mixins/event_provider.dart';
import 'package:flutter_scaffold_riverpod_v2/config/routes.dart';
import 'package:flutter_scaffold_riverpod_v2/config/theme/sizes.dart';
import 'package:flutter_scaffold_riverpod_v2/data/access/global_preferences_repository.dart';
import 'package:flutter_scaffold_riverpod_v2/data/access/session_repository.dart';
import 'package:flutter_scaffold_riverpod_v2/modules/home/home_io.dart';
import 'package:flutter_scaffold_riverpod_v2/modules/home/home_logic.dart';
import 'package:flutter_scaffold_riverpod_v2/utils/context.dart';
import 'package:flutter_scaffold_riverpod_v2/widgets/buttons/hero_button.dart';
import 'package:flutter_scaffold_riverpod_v2/widgets/misc/stream_provider_consumer.dart';
import 'package:flutter_scaffold_riverpod_v2/widgets/texts/section_title.dart';

/// View class
class HomeView extends ConsumerWidget {
  const HomeView({super.key});

  // Handle logic's output's events
  void onEvent(HomeEvent e, BuildContext context) {
    e.maybeWhen(
      loggedOut: () => context.go(Routes.auth),
      orElse: () {
        if (e is ErrorEvent) {
          return context.showErrorSnackBar((e as ErrorEvent).message ?? context.translation.UnknownError);
        }
      },
    );
  }

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    // Starts listening our logic's output events
    EventProvider.listen<HomeEvent>(ref, onEvent: (e) => onEvent(e, context));

    return Scaffold(
      appBar: AppBar(
        title: StreamProviderConsumer(
          globalPreferencesStreamProvider,
          builder: (context, state) => AutoSizeText(state.homeTitle, maxLines: 1),
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.all($Padding.m),
            child: StreamProviderConsumer(
              startedSessionStreamProvider,
              builder: (context, state) => SectionTitle(
                text: context.translation.YourLoggedAs(state.user.value!.name),
              ),
            ),
          ),
          Expanded(
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  HeroButton(
                    onTap: () => context.go(Routes.settings),
                    text: context.translation.Settings,
                  ),
                  HeroButton(
                    onTap: () => context.go(Routes.userList),
                    text: context.translation.Users,
                  ),
                  Consumer(
                    builder: (context, ref, child) {
                      final logic = ref.read(homeLogicProvider.notifier);
                      return HeroButton(
                        onTap: logic.logOut,
                        text: context.translation.LogOut,
                      );
                    },
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
