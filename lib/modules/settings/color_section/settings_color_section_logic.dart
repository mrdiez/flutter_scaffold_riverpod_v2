import 'dart:ui';

import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:flutter_scaffold_riverpod_v2/data/access/session_repository.dart';
import 'package:flutter_scaffold_riverpod_v2/data/access/user_repository.dart';

part 'settings_color_section_logic.g.dart';

// To generate @riverpod classes (in filename.g.dart) run:
// flutter pub run build_runner build --delete-conflicting-outputs

// The Riverpod's generator will create a Provider for our logic class
// We can inject dependencies by using the "ref" property (Riverpod's WidgetRef)
// Note that our logic class will be automatically re-instantiated
// if we're using ref.watch and if this "watcher" is triggered

/// Logic class
@riverpod
class SettingsColorSectionLogic extends _$SettingsColorSectionLogic {
  // This method is called each time this logic class is built
  @override
  void build() {
    // We retrieve our object's repository instance
    sessionRepository = ref.read(sessionRepositoryProvider);
    userRepository = ref.read(userRepositoryProvider);
  }

  // Instances of repositories which will handle network and/or local data
  late final SessionRepository sessionRepository;
  late final UserRepository userRepository;

  /// A method to update and save all our objects
  /// Note: here changes are saved each time one of the color input's value changed
  Future<void> saveChanges(Color primaryColor, Color secondaryColor) async {
    final session = await sessionRepository.get();
    if (session != null &&
        (session.preferences.primaryColor != primaryColor || session.preferences.secondaryColor != secondaryColor)) {
      session.preferences.primaryColor = primaryColor;
      session.preferences.secondaryColor = secondaryColor;
      await userRepository.update(session.user.value!);
      await session.user.save();
    }
  }
}
