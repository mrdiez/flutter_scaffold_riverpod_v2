// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'settings_color_section_logic.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$settingsColorSectionLogicHash() =>
    r'b9a82537400f482a2502f3ddb023771d9f24e004';

/// Logic class
///
/// Copied from [SettingsColorSectionLogic].
@ProviderFor(SettingsColorSectionLogic)
final settingsColorSectionLogicProvider =
    AutoDisposeNotifierProvider<SettingsColorSectionLogic, void>.internal(
  SettingsColorSectionLogic.new,
  name: r'settingsColorSectionLogicProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$settingsColorSectionLogicHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$SettingsColorSectionLogic = AutoDisposeNotifier<void>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member
