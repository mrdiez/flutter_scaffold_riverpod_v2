import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_scaffold_riverpod_v2/config/theme/sizes.dart';
import 'package:flutter_scaffold_riverpod_v2/data/access/session_repository.dart';
import 'package:flutter_scaffold_riverpod_v2/modules/settings/color_section/settings_color_section_logic.dart';
import 'package:flutter_scaffold_riverpod_v2/utils/context.dart';
import 'package:flutter_scaffold_riverpod_v2/widgets/inputs/color_input.dart';
import 'package:flutter_scaffold_riverpod_v2/widgets/texts/section_title.dart';

/// View class
class SettingsColorSectionView extends ConsumerStatefulWidget {
  const SettingsColorSectionView({super.key});

  @override
  ConsumerState<SettingsColorSectionView> createState() => _SettingsColorSectionViewState();
}

class _SettingsColorSectionViewState extends ConsumerState<SettingsColorSectionView> {
  // We're retrieving the instance of our logic class
  late final logic = ref.read(settingsColorSectionLogicProvider.notifier);

  // Controllers for our view's (Stateful) widget
  final ColorInputController primaryColorController = ColorInputController();
  final ColorInputController secondaryColorController = ColorInputController();

  void _saveChanges(_) => logic.saveChanges(primaryColorController.value, secondaryColorController.value);

  @override
  void initState() {
    super.initState();
    ref.read(startedSessionStreamProvider).whenData((state) {
      primaryColorController.value = state.preferences.primaryColor;
      secondaryColorController.value = state.preferences.secondaryColor;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SectionTitle(text: context.translation.ThemeColors),
        $Gap.s,
        Row(
          children: [
            Expanded(
              child: ColorInput(
                labelText: context.translation.Primary,
                controller: primaryColorController,
                onChanged: _saveChanges,
              ),
            ),
            $Gap.m,
            Expanded(
              child: ColorInput(
                labelText: context.translation.Secondary,
                controller: secondaryColorController,
                onChanged: _saveChanges,
              ),
            ),
          ],
        ),
      ],
    );
  }
}
