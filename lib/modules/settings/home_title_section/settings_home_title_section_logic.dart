import 'package:flutter/material.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:flutter_scaffold_riverpod_v2/data/access/global_preferences_repository.dart';

part 'settings_home_title_section_logic.g.dart';

// To generate @riverpod classes (in filename.g.dart) run:
// flutter pub run build_runner build --delete-conflicting-outputs

// The Riverpod's generator will create a Provider for our logic class
// We can inject dependencies by using the "ref" property (Riverpod's WidgetRef)
// Note that our logic class will be automatically re-instantiated
// if we're using ref.watch and if this "watcher" is triggered

/// Logic class
@riverpod
class SettingsHomeTitleSectionLogic extends _$SettingsHomeTitleSectionLogic {
  // This method is called each time this logic class is built
  @override
  void build() {
    // We retrieve our object's repository instance
    globalPreferencesRepository = ref.read(globalPreferencesRepositoryProvider);
  }

  // Instance of repository which will handle network and/or local data
  late final GlobalPreferencesRepository globalPreferencesRepository;

  // A controller for one of our view's (Stateful) widget
  final TextEditingController homeTitleController = TextEditingController();

  /// A method to update our object then to store it in our database
  /// Note: here changes are saved when we left the page
  void saveChanges() async {
    final globalPreferences = await globalPreferencesRepository.get();
    if (globalPreferences.homeTitle != homeTitleController.text) {
      globalPreferences.homeTitle = homeTitleController.text;
      globalPreferencesRepository.update(globalPreferences);
    }
  }
}
