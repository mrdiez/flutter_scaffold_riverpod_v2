// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'settings_home_title_section_logic.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$settingsHomeTitleSectionLogicHash() =>
    r'ee00d19dce77b732f29abf367d4e9c0a1396981a';

/// Logic class
///
/// Copied from [SettingsHomeTitleSectionLogic].
@ProviderFor(SettingsHomeTitleSectionLogic)
final settingsHomeTitleSectionLogicProvider =
    AutoDisposeNotifierProvider<SettingsHomeTitleSectionLogic, void>.internal(
  SettingsHomeTitleSectionLogic.new,
  name: r'settingsHomeTitleSectionLogicProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$settingsHomeTitleSectionLogicHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$SettingsHomeTitleSectionLogic = AutoDisposeNotifier<void>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member
