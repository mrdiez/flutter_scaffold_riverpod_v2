import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_scaffold_riverpod_v2/config/theme/sizes.dart';
import 'package:flutter_scaffold_riverpod_v2/data/access/global_preferences_repository.dart';
import 'package:flutter_scaffold_riverpod_v2/modules/settings/home_title_section/settings_home_title_section_logic.dart';
import 'package:flutter_scaffold_riverpod_v2/utils/context.dart';
import 'package:flutter_scaffold_riverpod_v2/widgets/misc/stream_provider_consumer.dart';
import 'package:flutter_scaffold_riverpod_v2/widgets/texts/section_title.dart';

/// View class
class SettingsHomeTitleSectionView extends ConsumerStatefulWidget {
  const SettingsHomeTitleSectionView({super.key});

  @override
  ConsumerState<SettingsHomeTitleSectionView> createState() => _SettingsHomeTitleSectionViewState();
}

class _SettingsHomeTitleSectionViewState extends ConsumerState<SettingsHomeTitleSectionView> {
  // We're retrieving the instance of our logic class
  late final logic = ref.read(settingsHomeTitleSectionLogicProvider.notifier);

  @override
  void dispose() {
    // Store changes into the database when closing this screen
    logic.saveChanges();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SectionTitle(text: context.translation.HomePageTitle),
        $Gap.s,
        Text(context.translation.ThisValueWillBeStoredInGlobalPrefsTable),
        $Gap.s,
        StreamProviderConsumer(
          globalPreferencesStreamProvider,
          builder: (context, state) => TextFormField(
            controller: logic.homeTitleController..text = state.homeTitle,
            decoration: InputDecoration(hintText: context.translation.Welcome),
            onFieldSubmitted: (value) => logic.saveChanges(),
          ),
        ),
      ],
    );
  }
}
