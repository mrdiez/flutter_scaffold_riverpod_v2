import 'package:flutter/material.dart';
import 'package:flutter_scaffold_riverpod_v2/config/theme/sizes.dart';
import 'package:flutter_scaffold_riverpod_v2/modules/settings/color_section/settings_color_section_view.dart';
import 'package:flutter_scaffold_riverpod_v2/modules/settings/home_title_section/settings_home_title_section_view.dart';
import 'package:flutter_scaffold_riverpod_v2/modules/settings/widgets/settings_app_bar.dart';

class SettingsView extends StatelessWidget {
  const SettingsView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: SettingsAppBar(),
      body: const Padding(
        padding: EdgeInsets.all($Padding.m),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SettingsHomeTitleSectionView(),
            $Gap.m,
            SettingsColorSectionView(),
          ],
        ),
      ),
    );
  }
}
