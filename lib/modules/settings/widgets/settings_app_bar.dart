import 'package:flutter/material.dart';
import 'package:flutter_scaffold_riverpod_v2/utils/context.dart';

/// An AppBar widget
///
/// Why don't you directly put this into the View instead of creating a dedicated widget ???
///
/// Because we want our AppBar to be contained in an Hero widget
/// but we can't pass an Hero widget to a Scaffold's appBar property :/
class SettingsAppBar extends AppBar {
  SettingsAppBar({super.key});

  @override
  State<SettingsAppBar> createState() => _SettingsAppBarState();
}

class _SettingsAppBarState extends State<SettingsAppBar> {
  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: context.translation.Settings,
      child: AppBar(
        leading: const BackButton(),
        titleSpacing: 0,
        title: Text(context.translation.Settings),
      ),
    );
  }
}
