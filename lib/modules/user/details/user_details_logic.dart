import 'dart:async';

import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:flutter_scaffold_riverpod_v2/config/translations/gen/l10n.dart';
import 'package:flutter_scaffold_riverpod_v2/data/access/user_repository.dart';
import 'package:flutter_scaffold_riverpod_v2/models/user.dart';
import 'package:url_launcher/url_launcher.dart';

part 'user_details_logic.g.dart';

// To generate @riverpod classes (in filename.g.dart) run:
// flutter pub run build_runner build --delete-conflicting-outputs

// The Riverpod's generator will create a Provider for our logic class
// We can inject dependencies by using the "ref" property (Riverpod's WidgetRef)
// Note that our logic class will be automatically re-instantiated
// if we're using ref.watch and if this "watcher" is triggered

/// Logic class
@riverpod
class UserDetailsLogic extends _$UserDetailsLogic {
  // This method is called each time this logic class is built
  @override
  Stream<User?> build(int userId) {
    // We retrieve our object's repository instance
    final userRepository = ref.read(userRepositoryProvider);
    // Then we start listening changes on our stored object
    return userRepository.watch(userId);
  }

  final _defaultEmail = <String, String>{
    'subject': '',
    'body': '${Translation.current.Hello}\n\n',
  }
      .entries
      .map((MapEntry<String, String> e) => '${Uri.encodeComponent(e.key)}=${Uri.encodeComponent(e.value)}')
      .join('&');
  void mail(String email) {
    launchUrl(Uri(scheme: 'mailto', path: email, query: _defaultEmail));
  }
}
