// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_details_logic.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$userDetailsLogicHash() => r'1f2faaf6edbf9aeb2f7a8ea8fd060d80c36c1bc7';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

abstract class _$UserDetailsLogic
    extends BuildlessAutoDisposeStreamNotifier<User?> {
  late final int userId;

  Stream<User?> build(
    int userId,
  );
}

/// Logic class
///
/// Copied from [UserDetailsLogic].
@ProviderFor(UserDetailsLogic)
const userDetailsLogicProvider = UserDetailsLogicFamily();

/// Logic class
///
/// Copied from [UserDetailsLogic].
class UserDetailsLogicFamily extends Family<AsyncValue<User?>> {
  /// Logic class
  ///
  /// Copied from [UserDetailsLogic].
  const UserDetailsLogicFamily();

  /// Logic class
  ///
  /// Copied from [UserDetailsLogic].
  UserDetailsLogicProvider call(
    int userId,
  ) {
    return UserDetailsLogicProvider(
      userId,
    );
  }

  @override
  UserDetailsLogicProvider getProviderOverride(
    covariant UserDetailsLogicProvider provider,
  ) {
    return call(
      provider.userId,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'userDetailsLogicProvider';
}

/// Logic class
///
/// Copied from [UserDetailsLogic].
class UserDetailsLogicProvider
    extends AutoDisposeStreamNotifierProviderImpl<UserDetailsLogic, User?> {
  /// Logic class
  ///
  /// Copied from [UserDetailsLogic].
  UserDetailsLogicProvider(
    this.userId,
  ) : super.internal(
          () => UserDetailsLogic()..userId = userId,
          from: userDetailsLogicProvider,
          name: r'userDetailsLogicProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$userDetailsLogicHash,
          dependencies: UserDetailsLogicFamily._dependencies,
          allTransitiveDependencies:
              UserDetailsLogicFamily._allTransitiveDependencies,
        );

  final int userId;

  @override
  bool operator ==(Object other) {
    return other is UserDetailsLogicProvider && other.userId == userId;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, userId.hashCode);

    return _SystemHash.finish(hash);
  }

  @override
  Stream<User?> runNotifierBuild(
    covariant UserDetailsLogic notifier,
  ) {
    return notifier.build(
      userId,
    );
  }
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member
