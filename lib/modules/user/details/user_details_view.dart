import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:flutter_scaffold_riverpod_v2/config/routes.dart';
import 'package:flutter_scaffold_riverpod_v2/config/theme/sizes.dart';
import 'package:flutter_scaffold_riverpod_v2/models/user.dart';
import 'package:flutter_scaffold_riverpod_v2/modules/user/details/user_details_logic.dart';
import 'package:flutter_scaffold_riverpod_v2/modules/user/details/widgets/user_details_app_bar.dart';
import 'package:flutter_scaffold_riverpod_v2/utils/context.dart';
import 'package:flutter_scaffold_riverpod_v2/utils/string.dart';
import 'package:flutter_scaffold_riverpod_v2/widgets/layout/rounded_container.dart';
import 'package:flutter_scaffold_riverpod_v2/widgets/texts/section_title.dart';

/// View class
class UserDetailsView extends StatelessWidget {
  const UserDetailsView(this.user, {super.key});

  // You're wondering why we have to pass an instance of our object to our view while our logic class
  // is already providing us the same instance glad to our amazing repository class
  //
  // It's because our repository provides data asynchronously and so on the very first frame of our view's rendering
  // this data will probably be null and still loading but we want our data to be displayed at the very first frame
  // in order to avoid a useless loading screen and to get our heroes' animations working
  final User user;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: UserDetailsAppBar(user),
      floatingActionButton: Consumer(
        builder: (context, ref, child) {
          final state = ref.watch(userDetailsLogicProvider(user.id));
          return FloatingActionButton(
            onPressed: () => context.go(Routes.userEdit(user.id), extra: {'user': state.value ?? user}),
            child: child,
          );
        },
        child: const Icon(Icons.edit_rounded, size: $Icon.s),
      ),
      body: Padding(
        padding: const EdgeInsets.all($Padding.m),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SectionTitle(text: context.translation.MainInfo, textAsHeroTag: true),
            $Gap.s,
            RoundedContainer(
              heroTag: 'user_${user.id}_main_info',
              child: SingleChildScrollView(
                // This scrollable is used to avoid an overflow error during the hero animation
                physics: const NeverScrollableScrollPhysics(),
                child: Column(
                  children: [
                    Row(
                      children: [
                        Icon(Icons.perm_identity_rounded, color: context.primaryColor, size: $Icon.s),
                        $Gap.s,
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.only(top: $Padding.xxxs, bottom: $Padding.xxs),
                            child: Consumer(
                              builder: (context, ref, child) {
                                // Here we're watching only the admin property of our User
                                final isAdmin = ref.watch(
                                        userDetailsLogicProvider(user.id).select((state) => state.value?.admin)) ??
                                    user.admin;
                                return Text(isAdmin ? context.translation.Admin : context.translation.User);
                              },
                            ),
                          ),
                        ),
                      ],
                    ),
                    $Gap.xs,
                    Consumer(
                      builder: (context, ref, child) {
                        final logic = ref.read(userDetailsLogicProvider(user.id).notifier);
                        // Here we're watching only the email property of our User
                        final email =
                            ref.watch(userDetailsLogicProvider(user.id).select((state) => state.value?.email)) ??
                                user.email;
                        return InkWell(
                          onTap: email.isEmail ? () => logic.mail(email) : null,
                          child: Row(
                            children: [
                              Icon(Icons.email_rounded, color: context.primaryColor, size: $Icon.s),
                              $Gap.s,
                              Expanded(
                                child: Padding(
                                  padding: const EdgeInsets.only(top: $Padding.xxxs, bottom: $Padding.xxs),
                                  child: Text(email),
                                ),
                              ),
                            ],
                          ),
                        );
                      },
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
