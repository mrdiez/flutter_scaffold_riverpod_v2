import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_scaffold_riverpod_v2/models/user.dart';
import 'package:flutter_scaffold_riverpod_v2/modules/user/details/user_details_logic.dart';
import 'package:flutter_scaffold_riverpod_v2/utils/context.dart';

/// An AppBar widget
///
/// Why don't you directly put this into the View instead of creating a dedicated widget ???
///
/// For the same reason described in the SettingsAppBar comment ;)
///
/// And also here because the View's build method starts to be long
class UserDetailsAppBar extends AppBar {
  UserDetailsAppBar(this.user, {super.key});

  final User user;

  @override
  State<UserDetailsAppBar> createState() => _UserDetailsAppBarState();
}

class _UserDetailsAppBarState extends State<UserDetailsAppBar> {
  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: 'user_${widget.user.id}',
      child: AppBar(
        leading: const BackButton(),
        titleSpacing: 0,
        title: Consumer(builder: (context, ref, child) {
          final state = ref.watch(userDetailsLogicProvider(widget.user.id));
          final user = state.value ?? widget.user;
          return Row(
            children: [
              AutoSizeText(
                '${user.firstName} ',
                style: context.textTheme.titleMedium,
                maxLines: 1,
              ),
              AutoSizeText(
                user.lastName,
                style: context.textTheme.titleMedium,
                maxLines: 1,
              ),
            ],
          );
        }),
      ),
    );
  }
}
