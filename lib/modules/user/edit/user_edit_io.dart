import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter_scaffold_riverpod_v2/abstract/interfaces/error_event.dart';
import 'package:flutter_scaffold_riverpod_v2/abstract/mixins/event_provider.dart';
import 'package:flutter_scaffold_riverpod_v2/models/user.dart';

part 'user_edit_io.freezed.dart';

// To generate @freezed classes (in filename.freezed.dart) run:
// flutter pub run build_runner build --delete-conflicting-outputs

/// View states
@freezed
class UserEditState with _$UserEditState {
  const factory UserEditState({
    User? user,
    String? firstName,
    String? lastName,
    @Default(false) bool admin,
    String? email,
    @Default(false) bool loading,
    @Default(false) bool error,
  }) = _UserEditState;
}

/// Logic events
/// Define here input/output events which are coming to/from this module's logic class
@freezed
class UserEditEvent with _$UserEditEvent, EventProvider<UserEditEvent> {
  UserEditEvent._() {
    trigger();
  }

  // Here is our logic's output events
  factory UserEditEvent.leave() = UserEditEventLeave;
  factory UserEditEvent.toggleCancelConfirmationModal() = UserEditEventToggleCancelConfirmationModal;

  // Error events can implement ErrorEvent in order to be able to check: if (event is ErrorEvent)
  @Implements<ErrorEvent>()
  factory UserEditEvent.unknownError([String? message]) = UserEditEventUnknownError;
}
