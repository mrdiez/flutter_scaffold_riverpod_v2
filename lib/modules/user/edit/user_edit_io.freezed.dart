// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'user_edit_io.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$UserEditState {
  User? get user => throw _privateConstructorUsedError;
  String? get firstName => throw _privateConstructorUsedError;
  String? get lastName => throw _privateConstructorUsedError;
  bool get admin => throw _privateConstructorUsedError;
  String? get email => throw _privateConstructorUsedError;
  bool get loading => throw _privateConstructorUsedError;
  bool get error => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $UserEditStateCopyWith<UserEditState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UserEditStateCopyWith<$Res> {
  factory $UserEditStateCopyWith(
          UserEditState value, $Res Function(UserEditState) then) =
      _$UserEditStateCopyWithImpl<$Res, UserEditState>;
  @useResult
  $Res call(
      {User? user,
      String? firstName,
      String? lastName,
      bool admin,
      String? email,
      bool loading,
      bool error});
}

/// @nodoc
class _$UserEditStateCopyWithImpl<$Res, $Val extends UserEditState>
    implements $UserEditStateCopyWith<$Res> {
  _$UserEditStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? user = freezed,
    Object? firstName = freezed,
    Object? lastName = freezed,
    Object? admin = null,
    Object? email = freezed,
    Object? loading = null,
    Object? error = null,
  }) {
    return _then(_value.copyWith(
      user: freezed == user
          ? _value.user
          : user // ignore: cast_nullable_to_non_nullable
              as User?,
      firstName: freezed == firstName
          ? _value.firstName
          : firstName // ignore: cast_nullable_to_non_nullable
              as String?,
      lastName: freezed == lastName
          ? _value.lastName
          : lastName // ignore: cast_nullable_to_non_nullable
              as String?,
      admin: null == admin
          ? _value.admin
          : admin // ignore: cast_nullable_to_non_nullable
              as bool,
      email: freezed == email
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String?,
      loading: null == loading
          ? _value.loading
          : loading // ignore: cast_nullable_to_non_nullable
              as bool,
      error: null == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_UserEditStateCopyWith<$Res>
    implements $UserEditStateCopyWith<$Res> {
  factory _$$_UserEditStateCopyWith(
          _$_UserEditState value, $Res Function(_$_UserEditState) then) =
      __$$_UserEditStateCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {User? user,
      String? firstName,
      String? lastName,
      bool admin,
      String? email,
      bool loading,
      bool error});
}

/// @nodoc
class __$$_UserEditStateCopyWithImpl<$Res>
    extends _$UserEditStateCopyWithImpl<$Res, _$_UserEditState>
    implements _$$_UserEditStateCopyWith<$Res> {
  __$$_UserEditStateCopyWithImpl(
      _$_UserEditState _value, $Res Function(_$_UserEditState) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? user = freezed,
    Object? firstName = freezed,
    Object? lastName = freezed,
    Object? admin = null,
    Object? email = freezed,
    Object? loading = null,
    Object? error = null,
  }) {
    return _then(_$_UserEditState(
      user: freezed == user
          ? _value.user
          : user // ignore: cast_nullable_to_non_nullable
              as User?,
      firstName: freezed == firstName
          ? _value.firstName
          : firstName // ignore: cast_nullable_to_non_nullable
              as String?,
      lastName: freezed == lastName
          ? _value.lastName
          : lastName // ignore: cast_nullable_to_non_nullable
              as String?,
      admin: null == admin
          ? _value.admin
          : admin // ignore: cast_nullable_to_non_nullable
              as bool,
      email: freezed == email
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String?,
      loading: null == loading
          ? _value.loading
          : loading // ignore: cast_nullable_to_non_nullable
              as bool,
      error: null == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$_UserEditState implements _UserEditState {
  const _$_UserEditState(
      {this.user,
      this.firstName,
      this.lastName,
      this.admin = false,
      this.email,
      this.loading = false,
      this.error = false});

  @override
  final User? user;
  @override
  final String? firstName;
  @override
  final String? lastName;
  @override
  @JsonKey()
  final bool admin;
  @override
  final String? email;
  @override
  @JsonKey()
  final bool loading;
  @override
  @JsonKey()
  final bool error;

  @override
  String toString() {
    return 'UserEditState(user: $user, firstName: $firstName, lastName: $lastName, admin: $admin, email: $email, loading: $loading, error: $error)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_UserEditState &&
            (identical(other.user, user) || other.user == user) &&
            (identical(other.firstName, firstName) ||
                other.firstName == firstName) &&
            (identical(other.lastName, lastName) ||
                other.lastName == lastName) &&
            (identical(other.admin, admin) || other.admin == admin) &&
            (identical(other.email, email) || other.email == email) &&
            (identical(other.loading, loading) || other.loading == loading) &&
            (identical(other.error, error) || other.error == error));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, user, firstName, lastName, admin, email, loading, error);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_UserEditStateCopyWith<_$_UserEditState> get copyWith =>
      __$$_UserEditStateCopyWithImpl<_$_UserEditState>(this, _$identity);
}

abstract class _UserEditState implements UserEditState {
  const factory _UserEditState(
      {final User? user,
      final String? firstName,
      final String? lastName,
      final bool admin,
      final String? email,
      final bool loading,
      final bool error}) = _$_UserEditState;

  @override
  User? get user;
  @override
  String? get firstName;
  @override
  String? get lastName;
  @override
  bool get admin;
  @override
  String? get email;
  @override
  bool get loading;
  @override
  bool get error;
  @override
  @JsonKey(ignore: true)
  _$$_UserEditStateCopyWith<_$_UserEditState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$UserEditEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() leave,
    required TResult Function() toggleCancelConfirmationModal,
    required TResult Function(String? message) unknownError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? leave,
    TResult? Function()? toggleCancelConfirmationModal,
    TResult? Function(String? message)? unknownError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? leave,
    TResult Function()? toggleCancelConfirmationModal,
    TResult Function(String? message)? unknownError,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(UserEditEventLeave value) leave,
    required TResult Function(UserEditEventToggleCancelConfirmationModal value)
        toggleCancelConfirmationModal,
    required TResult Function(UserEditEventUnknownError value) unknownError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(UserEditEventLeave value)? leave,
    TResult? Function(UserEditEventToggleCancelConfirmationModal value)?
        toggleCancelConfirmationModal,
    TResult? Function(UserEditEventUnknownError value)? unknownError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(UserEditEventLeave value)? leave,
    TResult Function(UserEditEventToggleCancelConfirmationModal value)?
        toggleCancelConfirmationModal,
    TResult Function(UserEditEventUnknownError value)? unknownError,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UserEditEventCopyWith<$Res> {
  factory $UserEditEventCopyWith(
          UserEditEvent value, $Res Function(UserEditEvent) then) =
      _$UserEditEventCopyWithImpl<$Res, UserEditEvent>;
}

/// @nodoc
class _$UserEditEventCopyWithImpl<$Res, $Val extends UserEditEvent>
    implements $UserEditEventCopyWith<$Res> {
  _$UserEditEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$UserEditEventLeaveCopyWith<$Res> {
  factory _$$UserEditEventLeaveCopyWith(_$UserEditEventLeave value,
          $Res Function(_$UserEditEventLeave) then) =
      __$$UserEditEventLeaveCopyWithImpl<$Res>;
}

/// @nodoc
class __$$UserEditEventLeaveCopyWithImpl<$Res>
    extends _$UserEditEventCopyWithImpl<$Res, _$UserEditEventLeave>
    implements _$$UserEditEventLeaveCopyWith<$Res> {
  __$$UserEditEventLeaveCopyWithImpl(
      _$UserEditEventLeave _value, $Res Function(_$UserEditEventLeave) _then)
      : super(_value, _then);
}

/// @nodoc

class _$UserEditEventLeave extends UserEditEventLeave {
  _$UserEditEventLeave() : super._();

  @override
  String toString() {
    return 'UserEditEvent.leave()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$UserEditEventLeave);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() leave,
    required TResult Function() toggleCancelConfirmationModal,
    required TResult Function(String? message) unknownError,
  }) {
    return leave();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? leave,
    TResult? Function()? toggleCancelConfirmationModal,
    TResult? Function(String? message)? unknownError,
  }) {
    return leave?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? leave,
    TResult Function()? toggleCancelConfirmationModal,
    TResult Function(String? message)? unknownError,
    required TResult orElse(),
  }) {
    if (leave != null) {
      return leave();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(UserEditEventLeave value) leave,
    required TResult Function(UserEditEventToggleCancelConfirmationModal value)
        toggleCancelConfirmationModal,
    required TResult Function(UserEditEventUnknownError value) unknownError,
  }) {
    return leave(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(UserEditEventLeave value)? leave,
    TResult? Function(UserEditEventToggleCancelConfirmationModal value)?
        toggleCancelConfirmationModal,
    TResult? Function(UserEditEventUnknownError value)? unknownError,
  }) {
    return leave?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(UserEditEventLeave value)? leave,
    TResult Function(UserEditEventToggleCancelConfirmationModal value)?
        toggleCancelConfirmationModal,
    TResult Function(UserEditEventUnknownError value)? unknownError,
    required TResult orElse(),
  }) {
    if (leave != null) {
      return leave(this);
    }
    return orElse();
  }
}

abstract class UserEditEventLeave extends UserEditEvent {
  factory UserEditEventLeave() = _$UserEditEventLeave;
  UserEditEventLeave._() : super._();
}

/// @nodoc
abstract class _$$UserEditEventToggleCancelConfirmationModalCopyWith<$Res> {
  factory _$$UserEditEventToggleCancelConfirmationModalCopyWith(
          _$UserEditEventToggleCancelConfirmationModal value,
          $Res Function(_$UserEditEventToggleCancelConfirmationModal) then) =
      __$$UserEditEventToggleCancelConfirmationModalCopyWithImpl<$Res>;
}

/// @nodoc
class __$$UserEditEventToggleCancelConfirmationModalCopyWithImpl<$Res>
    extends _$UserEditEventCopyWithImpl<$Res,
        _$UserEditEventToggleCancelConfirmationModal>
    implements _$$UserEditEventToggleCancelConfirmationModalCopyWith<$Res> {
  __$$UserEditEventToggleCancelConfirmationModalCopyWithImpl(
      _$UserEditEventToggleCancelConfirmationModal _value,
      $Res Function(_$UserEditEventToggleCancelConfirmationModal) _then)
      : super(_value, _then);
}

/// @nodoc

class _$UserEditEventToggleCancelConfirmationModal
    extends UserEditEventToggleCancelConfirmationModal {
  _$UserEditEventToggleCancelConfirmationModal() : super._();

  @override
  String toString() {
    return 'UserEditEvent.toggleCancelConfirmationModal()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$UserEditEventToggleCancelConfirmationModal);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() leave,
    required TResult Function() toggleCancelConfirmationModal,
    required TResult Function(String? message) unknownError,
  }) {
    return toggleCancelConfirmationModal();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? leave,
    TResult? Function()? toggleCancelConfirmationModal,
    TResult? Function(String? message)? unknownError,
  }) {
    return toggleCancelConfirmationModal?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? leave,
    TResult Function()? toggleCancelConfirmationModal,
    TResult Function(String? message)? unknownError,
    required TResult orElse(),
  }) {
    if (toggleCancelConfirmationModal != null) {
      return toggleCancelConfirmationModal();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(UserEditEventLeave value) leave,
    required TResult Function(UserEditEventToggleCancelConfirmationModal value)
        toggleCancelConfirmationModal,
    required TResult Function(UserEditEventUnknownError value) unknownError,
  }) {
    return toggleCancelConfirmationModal(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(UserEditEventLeave value)? leave,
    TResult? Function(UserEditEventToggleCancelConfirmationModal value)?
        toggleCancelConfirmationModal,
    TResult? Function(UserEditEventUnknownError value)? unknownError,
  }) {
    return toggleCancelConfirmationModal?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(UserEditEventLeave value)? leave,
    TResult Function(UserEditEventToggleCancelConfirmationModal value)?
        toggleCancelConfirmationModal,
    TResult Function(UserEditEventUnknownError value)? unknownError,
    required TResult orElse(),
  }) {
    if (toggleCancelConfirmationModal != null) {
      return toggleCancelConfirmationModal(this);
    }
    return orElse();
  }
}

abstract class UserEditEventToggleCancelConfirmationModal
    extends UserEditEvent {
  factory UserEditEventToggleCancelConfirmationModal() =
      _$UserEditEventToggleCancelConfirmationModal;
  UserEditEventToggleCancelConfirmationModal._() : super._();
}

/// @nodoc
abstract class _$$UserEditEventUnknownErrorCopyWith<$Res> {
  factory _$$UserEditEventUnknownErrorCopyWith(
          _$UserEditEventUnknownError value,
          $Res Function(_$UserEditEventUnknownError) then) =
      __$$UserEditEventUnknownErrorCopyWithImpl<$Res>;
  @useResult
  $Res call({String? message});
}

/// @nodoc
class __$$UserEditEventUnknownErrorCopyWithImpl<$Res>
    extends _$UserEditEventCopyWithImpl<$Res, _$UserEditEventUnknownError>
    implements _$$UserEditEventUnknownErrorCopyWith<$Res> {
  __$$UserEditEventUnknownErrorCopyWithImpl(_$UserEditEventUnknownError _value,
      $Res Function(_$UserEditEventUnknownError) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? message = freezed,
  }) {
    return _then(_$UserEditEventUnknownError(
      freezed == message
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$UserEditEventUnknownError extends UserEditEventUnknownError {
  _$UserEditEventUnknownError([this.message]) : super._();

  @override
  final String? message;

  @override
  String toString() {
    return 'UserEditEvent.unknownError(message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$UserEditEventUnknownError &&
            (identical(other.message, message) || other.message == message));
  }

  @override
  int get hashCode => Object.hash(runtimeType, message);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$UserEditEventUnknownErrorCopyWith<_$UserEditEventUnknownError>
      get copyWith => __$$UserEditEventUnknownErrorCopyWithImpl<
          _$UserEditEventUnknownError>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() leave,
    required TResult Function() toggleCancelConfirmationModal,
    required TResult Function(String? message) unknownError,
  }) {
    return unknownError(message);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? leave,
    TResult? Function()? toggleCancelConfirmationModal,
    TResult? Function(String? message)? unknownError,
  }) {
    return unknownError?.call(message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? leave,
    TResult Function()? toggleCancelConfirmationModal,
    TResult Function(String? message)? unknownError,
    required TResult orElse(),
  }) {
    if (unknownError != null) {
      return unknownError(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(UserEditEventLeave value) leave,
    required TResult Function(UserEditEventToggleCancelConfirmationModal value)
        toggleCancelConfirmationModal,
    required TResult Function(UserEditEventUnknownError value) unknownError,
  }) {
    return unknownError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(UserEditEventLeave value)? leave,
    TResult? Function(UserEditEventToggleCancelConfirmationModal value)?
        toggleCancelConfirmationModal,
    TResult? Function(UserEditEventUnknownError value)? unknownError,
  }) {
    return unknownError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(UserEditEventLeave value)? leave,
    TResult Function(UserEditEventToggleCancelConfirmationModal value)?
        toggleCancelConfirmationModal,
    TResult Function(UserEditEventUnknownError value)? unknownError,
    required TResult orElse(),
  }) {
    if (unknownError != null) {
      return unknownError(this);
    }
    return orElse();
  }
}

abstract class UserEditEventUnknownError extends UserEditEvent
    implements ErrorEvent {
  factory UserEditEventUnknownError([final String? message]) =
      _$UserEditEventUnknownError;
  UserEditEventUnknownError._() : super._();

  String? get message;
  @JsonKey(ignore: true)
  _$$UserEditEventUnknownErrorCopyWith<_$UserEditEventUnknownError>
      get copyWith => throw _privateConstructorUsedError;
}
