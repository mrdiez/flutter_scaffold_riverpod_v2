import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:flutter_scaffold_riverpod_v2/data/access/user_repository.dart';
import 'package:flutter_scaffold_riverpod_v2/models/user.dart';
import 'package:flutter_scaffold_riverpod_v2/modules/user/edit/user_edit_io.dart';
import 'package:flutter_scaffold_riverpod_v2/utils/form.dart';

part 'user_edit_logic.g.dart';

// To generate @riverpod classes (in filename.g.dart) run:
// flutter pub run build_runner build --delete-conflicting-outputs

// The Riverpod's generator will create a Provider for our logic class
// We can inject dependencies by using the "ref" property (Riverpod's WidgetRef)
// Note that our logic class will be automatically re-instantiated
// if we're using ref.watch and if this "watcher" is triggered

/// Logic class
@riverpod
class UserEditLogic extends _$UserEditLogic {
  // This method is called each time this logic class is built
  @override
  UserEditState build(int? userId) {
    // This line will keep our WidgetRef alive until we don't need it anymore (until we leave the page)
    // without this line our generated logicProvider would be disposed right after having completed his future
    // and so this method would be called each time we're reading the associated generated provider
    // But we want to reuse the same provided object and avoid to do useless calls
    _keepAliveLink = ref.keepAlive();
    // We retrieve our object's repository instance
    _userRepository = ref.read(userRepositoryProvider);
    // If an id have been provided
    if (userId != null) {
      // Then we retrieve our stored object with the given id
      _userRepository.get(userId).then((user) {
        // And we update our view's state
        if (user != null) {
          state = state.copyWith(user: user);
        } else {
          state = state.copyWith(error: true);
          UserEditEvent.unknownError();
        }
        state = state.copyWith(loading: false);
      });
    }
    return const UserEditState(loading: true);
  }

  // A way to avoid our Riverpod's Provider to be disposed right after having been build
  late final KeepAliveLink _keepAliveLink;

  // The instance of the repository which will handle network and/or local data
  late final UserRepository _userRepository;

  // The form key to access form's state to perform a validation check on save
  final formKey = GlobalKey<FormBuilderState>();

  // Map inputs' values with their respective state's property
  void saveFirstName(String? firstname) => state = state.copyWith(firstName: firstname?.trim());
  void saveLastName(String? lastName) => state = state.copyWith(lastName: lastName?.trim());
  void saveEmail(String? email) => state = state.copyWith(email: email?.trim());
  void saveAdmin(bool admin) => state = state.copyWith(admin: admin);

  // Called when leaving the page
  bool canCancel() {
    // Just to clean up unwanted spaces typed by the user
    formKey.currentState?.trim();
    // Trigger a form save to ensure all our state's properties have been updated
    formKey.currentState?.save();
    bool canCancel = state.user == null;
    // If we're editing an existing user
    if (!canCancel) {
      // We have to check if the edited user have been modified
      final originalUserProperties = [
        state.user!.admin,
        state.user!.firstName,
        state.user!.lastName,
        state.user!.email,
      ];
      final newUserProperties = [state.admin, state.firstName, state.lastName, state.email];
      canCancel = listEquals(originalUserProperties, newUserProperties);
    }
    // Else if we're NOT editing an existing user OR if no properties have been changed
    if (canCancel) {
      close();
      return true;
    }
    // If some values have been modified we're asking what's the next step with a modal
    UserEditEvent.toggleCancelConfirmationModal();
    return false;
  }

  Future<void> save() async {
    // Just to clean up unwanted spaces typed by the user
    formKey.currentState?.trim();
    // Check if all fields are correct
    if (!(formKey.currentState?.saveAndValidate() ?? false)) return;
    // Save all changes
    final user = state.user ?? User.empty();
    user.admin = state.admin;
    user.firstName = state.firstName!;
    user.lastName = state.lastName!;
    user.email = state.email!;
    user.preferences.value = state.user?.preferences.value;
    // Update our database
    await _userRepository.update(user);
    close();
  }

  Future<void> close() async {
    // We can now dispose our Riverpod's Provider
    _keepAliveLink.close();
    // Trigger an event to ask our view to go back to the previous page
    UserEditEvent.leave();
  }
}
