// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_edit_logic.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$userEditLogicHash() => r'2a851e071295cb3bb0029b8b91717609ccb5a90c';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

abstract class _$UserEditLogic
    extends BuildlessAutoDisposeNotifier<UserEditState> {
  late final int? userId;

  UserEditState build(
    int? userId,
  );
}

/// Logic class
///
/// Copied from [UserEditLogic].
@ProviderFor(UserEditLogic)
const userEditLogicProvider = UserEditLogicFamily();

/// Logic class
///
/// Copied from [UserEditLogic].
class UserEditLogicFamily extends Family<UserEditState> {
  /// Logic class
  ///
  /// Copied from [UserEditLogic].
  const UserEditLogicFamily();

  /// Logic class
  ///
  /// Copied from [UserEditLogic].
  UserEditLogicProvider call(
    int? userId,
  ) {
    return UserEditLogicProvider(
      userId,
    );
  }

  @override
  UserEditLogicProvider getProviderOverride(
    covariant UserEditLogicProvider provider,
  ) {
    return call(
      provider.userId,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'userEditLogicProvider';
}

/// Logic class
///
/// Copied from [UserEditLogic].
class UserEditLogicProvider
    extends AutoDisposeNotifierProviderImpl<UserEditLogic, UserEditState> {
  /// Logic class
  ///
  /// Copied from [UserEditLogic].
  UserEditLogicProvider(
    this.userId,
  ) : super.internal(
          () => UserEditLogic()..userId = userId,
          from: userEditLogicProvider,
          name: r'userEditLogicProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$userEditLogicHash,
          dependencies: UserEditLogicFamily._dependencies,
          allTransitiveDependencies:
              UserEditLogicFamily._allTransitiveDependencies,
        );

  final int? userId;

  @override
  bool operator ==(Object other) {
    return other is UserEditLogicProvider && other.userId == userId;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, userId.hashCode);

    return _SystemHash.finish(hash);
  }

  @override
  UserEditState runNotifierBuild(
    covariant UserEditLogic notifier,
  ) {
    return notifier.build(
      userId,
    );
  }
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member
