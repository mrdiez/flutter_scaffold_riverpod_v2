import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:flutter_scaffold_riverpod_v2/abstract/interfaces/error_event.dart';
import 'package:flutter_scaffold_riverpod_v2/abstract/mixins/event_provider.dart';
import 'package:flutter_scaffold_riverpod_v2/config/theme/sizes.dart';
import 'package:flutter_scaffold_riverpod_v2/data/access/session_repository.dart';
import 'package:flutter_scaffold_riverpod_v2/models/user.dart';
import 'package:flutter_scaffold_riverpod_v2/modules/user/edit/user_edit_io.dart';
import 'package:flutter_scaffold_riverpod_v2/modules/user/edit/user_edit_logic.dart';
import 'package:flutter_scaffold_riverpod_v2/modules/user/edit/widgets/user_edit_app_bar.dart';
import 'package:flutter_scaffold_riverpod_v2/modules/user/edit/widgets/user_edit_cancel_confirmation_modal.dart';
import 'package:flutter_scaffold_riverpod_v2/utils/context.dart';
import 'package:flutter_scaffold_riverpod_v2/utils/form.dart';
import 'package:flutter_scaffold_riverpod_v2/widgets/inputs/animated_text_input.dart';
import 'package:flutter_scaffold_riverpod_v2/widgets/inputs/labelled_checkbox.dart';
import 'package:flutter_scaffold_riverpod_v2/widgets/layout/rounded_container.dart';
import 'package:flutter_scaffold_riverpod_v2/widgets/misc/stream_provider_consumer.dart';
import 'package:flutter_scaffold_riverpod_v2/widgets/texts/section_title.dart';

/// View class
class UserEditView extends ConsumerStatefulWidget {
  const UserEditView(this.user, {super.key});

  // If the user is null then we're creating a new user
  // else we're editing the given user
  final User? user;

  @override
  ConsumerState<UserEditView> createState() => _UserEditViewState();
}

class _UserEditViewState extends ConsumerState<UserEditView> {
  // Our text fields controllers
  late final emailController = TextEditingController(text: widget.user?.email);
  late final checkboxController = LabelledCheckboxController(widget.user?.admin ?? false);

  // Our text fields focus nodes (to auto focus next field on submit)
  final emailFocusNode = FocusNode();

  // Handle logic's output's events
  void onEvent(UserEditEvent e, UserEditLogic logic) {
    e.maybeWhen(
      toggleCancelConfirmationModal: () => context.showModal(
        UserEditCancelConfirmationModal(onSave: logic.save, onLeave: logic.close),
      ),
      leave: context.pop,
      orElse: () {
        if (e is ErrorEvent) {
          return context.showErrorSnackBar((e as ErrorEvent).message ?? context.translation.UnknownError);
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    // Here we retrieve our logic's instance
    final logic = ref.read(userEditLogicProvider(widget.user?.id).notifier);
    // Starts listening our logic's output events
    EventProvider.listen<UserEditEvent>(ref, onEvent: (e) => onEvent(e, logic));

    return WillPopScope(
      // This will block the ability to go back if false is returned
      onWillPop: () async => logic.canCancel(),
      child: FormBuilder(
        key: logic.formKey,
        child: Scaffold(
          appBar: UserEditAppBar(
            widget.user,
            onLastFieldSubmitted: (_) => emailFocusNode.requestFocus(),
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: logic.save,
            child: const Icon(Icons.save_rounded, size: $Icon.s),
          ),
          body: Padding(
            padding: const EdgeInsets.all($Padding.m),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SectionTitle(text: context.translation.MainInfo, textAsHeroTag: true),
                $Gap.s,
                RoundedContainer(
                  heroTag: 'user_${widget.user?.id}_main_info',
                  child: SingleChildScrollView(
                    // This scrollable is used to avoid an overflow error during the hero animation
                    physics: const NeverScrollableScrollPhysics(),
                    child: Column(
                      children: [
                        StreamProviderConsumer(
                          startedSessionStreamProvider,
                          builder: (context, state) => LabelledCheckbox(
                            labelText: context.translation.Admin,
                            controller: checkboxController,
                            onSaved: logic.saveAdmin,
                            enabled: state.admin,
                          ),
                        ),
                        $Gap.xs,
                        Row(
                          children: [
                            Icon(Icons.alternate_email_rounded, color: context.primaryColor, size: $Icon.s),
                            $Gap.s,
                            Expanded(
                              child: AnimatedTextInput(
                                controller: emailController,
                                focusNode: emailFocusNode,
                                validator: FormValidators.email,
                                decoration: InputDecoration(hintText: context.translation.Email),
                                onSaved: logic.saveEmail,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
