import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_scaffold_riverpod_v2/config/theme/sizes.dart';
import 'package:flutter_scaffold_riverpod_v2/models/user.dart';
import 'package:flutter_scaffold_riverpod_v2/modules/user/edit/user_edit_logic.dart';
import 'package:flutter_scaffold_riverpod_v2/utils/context.dart';
import 'package:flutter_scaffold_riverpod_v2/utils/form.dart';
import 'package:flutter_scaffold_riverpod_v2/widgets/inputs/animated_text_input.dart';

/// An AppBar widget
///
/// Why don't you directly put this into the View instead of creating a dedicated widget ???
///
/// For the same reasons described in the UserDetailsAppBar comment ;)
class UserEditAppBar extends AppBar {
  UserEditAppBar(this.user, {super.key, this.onLastFieldSubmitted});

  final User? user;
  final ValueChanged<String?>? onLastFieldSubmitted;

  @override
  State<UserEditAppBar> createState() => _UserEditAppBarState();
}

class _UserEditAppBarState extends State<UserEditAppBar> {
  // Our text fields controllers
  late final firstNameController = TextEditingController(text: widget.user?.firstName);
  late final lastNameController = TextEditingController(text: widget.user?.lastName);

  // Our text fields focus nodes (to auto focus next field on submit)
  final lastNameFocusNode = FocusNode();

  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: 'user_${widget.user?.id}',
      child: AppBar(
        leading: const BackButton(),
        titleSpacing: 0,
        title: Consumer(builder: (context, ref, child) {
          final logic = ref.read(userEditLogicProvider(widget.user?.id).notifier);
          return Row(
            children: [
              Expanded(
                child: AnimatedTextInput(
                  style: context.textTheme.titleMedium,
                  controller: firstNameController,
                  validator: FormValidators.firstName,
                  onFieldSubmitted: (_) => lastNameFocusNode.requestFocus(),
                  onSaved: logic.saveFirstName,
                  decoration: InputDecoration(hintText: context.translation.Firstname),
                ),
              ),
              $Gap.s,
              Expanded(
                child: AnimatedTextInput(
                  style: context.textTheme.titleMedium,
                  controller: lastNameController,
                  focusNode: lastNameFocusNode,
                  validator: FormValidators.lastName,
                  onSaved: logic.saveLastName,
                  onFieldSubmitted: widget.onLastFieldSubmitted,
                  decoration: InputDecoration(hintText: context.translation.Lastname),
                ),
              ),
            ],
          );
        }),
      ),
    );
  }
}
