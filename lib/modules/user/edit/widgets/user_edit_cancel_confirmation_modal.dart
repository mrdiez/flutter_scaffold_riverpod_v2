import 'package:flutter/material.dart';
import 'package:flutter_scaffold_riverpod_v2/utils/context.dart';
import 'package:flutter_scaffold_riverpod_v2/widgets/buttons/common_icon_button.dart';
import 'package:flutter_scaffold_riverpod_v2/widgets/modals/multiple_icon_button_modal.dart';

/// A modal widget
/// It is supposed to be displayed when the user tried to leaves the edit page without having saved his modifications
class UserEditCancelConfirmationModal extends StatelessWidget {
  const UserEditCancelConfirmationModal({
    super.key,
    required this.onSave,
    required this.onLeave,
  });

  final VoidCallback onSave;
  final VoidCallback onLeave;

  @override
  Widget build(BuildContext context) {
    return MultipleIconButtonModal(
      buttons: [
        CommonIconButton(
          icon: Icons.backspace_rounded,
          text: context.translation.Leave,
          onTap: () {
            onLeave();
            Navigator.pop(context);
          },
        ),
        CommonIconButton(
          icon: Icons.save_rounded,
          text: context.translation.Save,
          onTap: () {
            onSave();
            Navigator.pop(context);
          },
        ),
        CommonIconButton(
          icon: Icons.edit_rounded,
          text: context.translation.Edit,
          onTap: () {
            Navigator.pop(context);
          },
        ),
      ],
    );
  }
}
