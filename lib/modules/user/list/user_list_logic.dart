import 'dart:async';

import 'package:dartx/dartx.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:rxdart/rxdart.dart';
import 'package:flutter_scaffold_riverpod_v2/data/access/user_repository.dart';
import 'package:flutter_scaffold_riverpod_v2/models/user.dart';
import 'package:flutter_scaffold_riverpod_v2/modules/user/list/utils/user.dart';

part 'user_list_logic.g.dart';

// To generate @riverpod classes (in filename.g.dart) run:
// flutter pub run build_runner build --delete-conflicting-outputs

// The Riverpod's generator will create a Provider for our logic class
// We can inject dependencies by using the "ref" property (Riverpod's WidgetRef)
// Note that our logic class will be automatically re-instantiated
// if we're using ref.watch and if this "watcher" is triggered

/// Logic class
@riverpod
class UserListLogic extends _$UserListLogic {
  // This method is called each time this logic class is built
  @override
  Stream<List<User>> build() {
    // We retrieve our object's repository instance
    final userRepository = ref.read(userRepositoryProvider);
    // Then we start listening a combination of streams
    return CombineLatestStream<dynamic, List<User>>(
      [
        // We start listening changes on our stored object list
        userRepository.watchAll(),
        // We also start listening changes on our search input
        searchInputStream.stream,
      ],
      (combinedStreamValues) {
        final users = combinedStreamValues[0] as List<User>;
        final searchInputValue = combinedStreamValues[1] as String?;
        // Then we return our object list filtered by the search input value
        if (searchInputValue.isNullOrBlank) return users;
        return users.search(searchInputValue!);
      },
    );
  }

  // A stream controller to be able to catch our search input changes
  // Note "..add(null)" allow this stream to fire immediately a value
  // It's mandatory if we want our stream combination to be fired when our logic class is created
  final searchInputStream = StreamController<String?>()..add(null);
}
