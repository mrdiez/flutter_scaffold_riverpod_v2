// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_list_logic.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$userListLogicHash() => r'58cef474bd4dbd1a0b65046c6285487e2c7204fc';

/// Logic class
///
/// Copied from [UserListLogic].
@ProviderFor(UserListLogic)
final userListLogicProvider =
    AutoDisposeStreamNotifierProvider<UserListLogic, List<User>>.internal(
  UserListLogic.new,
  name: r'userListLogicProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$userListLogicHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$UserListLogic = AutoDisposeStreamNotifier<List<User>>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member
