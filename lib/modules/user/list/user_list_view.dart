import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:flutter_scaffold_riverpod_v2/config/routes.dart';
import 'package:flutter_scaffold_riverpod_v2/config/theme/sizes.dart';
import 'package:flutter_scaffold_riverpod_v2/modules/user/list/user_list_logic.dart';
import 'package:flutter_scaffold_riverpod_v2/modules/user/list/widgets/user_list_app_bar.dart';
import 'package:flutter_scaffold_riverpod_v2/modules/user/list/widgets/user_list_item.dart';
import 'package:flutter_scaffold_riverpod_v2/utils/context.dart';

/// View class
class UserListView extends StatelessWidget {
  const UserListView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: UserListAppBar(),
      floatingActionButton: FloatingActionButton(
        onPressed: () => context.go(Routes.userCreate),
        child: const Icon(Icons.add_rounded, size: $Icon.m),
      ),
      // The consumer widget allow us to access to Riverpod's WidgetRef in order to retrieve dependencies
      // Note that our view could also inherits directly from ConsumerWidget and ConsumerStatefulWidget
      // instead of StatelessWidget and StatefulWidget and so access to "ref" anywhere
      body: Consumer(
        builder: (context, ref, child) {
          // We're listening our data's stream
          // This part of the view will be rebuilded each time we'll receive a stream's entry
          final state = ref.watch(userListLogicProvider);
          return state.when(
            // The stream is still loading
            loading: () => const Center(child: CircularProgressIndicator()),
            // The stream has thrown an error
            error: (e, stackTrace) {
              debugPrint('[$runtimeType] ERROR\n$e\n$stackTrace');
              return Center(child: Text(context.translation.UnknownError));
            },
            // The stream returned some data
            data: (users) {
              return ListView.builder(
                itemCount: users.length,
                itemBuilder: (context, i) {
                  final user = users[i];
                  return Padding(
                    padding: i == 0
                        ? const EdgeInsets.fromLTRB($Padding.m, $Padding.m, $Padding.m, $Padding.s)
                        : const EdgeInsets.symmetric(vertical: $Padding.s, horizontal: $Padding.m),
                    child: UserListItem(
                      user,
                      onTap: () => context.go(
                        Routes.userDetails(user.id),
                        extra: {'user': user},
                      ),
                    ),
                  );
                },
              );
            },
          );
        },
      ),
    );
  }
}
