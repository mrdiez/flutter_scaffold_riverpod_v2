import 'package:flutter_scaffold_riverpod_v2/models/user.dart';
import 'package:flutter_scaffold_riverpod_v2/utils/string.dart';

/// An extension that provides useful methods (or not)
extension UserListUtils on List<User> {
  List<User> search(String str) {
    List<User> result;
    if (str.length == 1) {
      final searchedLetter = str.clean();
      result = where((c) {
        return c.firstName.clean().startsWith(searchedLetter) || c.lastName.clean().startsWith(searchedLetter);
      }).toList();
    } else {
      final searchedWords = str.cleanAndSplit();
      result = where((c) {
        List<String> foundWords = c.name.searchWords(searchedWords);
        if (foundWords.length == searchedWords.length) return true;
        return false;
      }).toList();
    }
    return result;
  }
}
