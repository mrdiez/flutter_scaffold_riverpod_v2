import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_scaffold_riverpod_v2/config/theme/sizes.dart';
import 'package:flutter_scaffold_riverpod_v2/modules/user/list/user_list_logic.dart';
import 'package:flutter_scaffold_riverpod_v2/utils/context.dart';

/// An AppBar widget
///
/// Why don't you directly put this into the View instead of creating a dedicated widget ???
///
/// For the same reason described in the SettingsAppBar comment ;)
class UserListAppBar extends AppBar {
  UserListAppBar({super.key});

  @override
  State<UserListAppBar> createState() => _UserListAppBarState();
}

class _UserListAppBarState extends State<UserListAppBar> {
  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: context.translation.Users,
      child: AppBar(
        leading: const BackButton(),
        titleSpacing: 0,
        title: Consumer(builder: (context, ref, child) {
          // We're retrieving the instance of our logic class
          final logic = ref.read(userListLogicProvider.notifier);
          return TextField(
            onChanged: logic.searchInputStream.add,
            style: context.textTheme.bodyLarge,
            decoration: InputDecoration(
              contentPadding: const EdgeInsets.only(right: $Padding.xxl),
              hintText: '${context.translation.Search} ${context.translation.a} ${context.translation.user}',
              hintStyle: context.textTheme.bodyMedium,
              border: InputBorder.none,
              enabledBorder: InputBorder.none,
              focusedBorder: InputBorder.none,
            ),
          );
        }),
      ),
    );
  }
}
