import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_scaffold_riverpod_v2/config/theme/sizes.dart';
import 'package:flutter_scaffold_riverpod_v2/models/user.dart';
import 'package:flutter_scaffold_riverpod_v2/utils/context.dart';
import 'package:flutter_scaffold_riverpod_v2/widgets/layout/rounded_container.dart';

// An amazing list item
class UserListItem extends StatelessWidget {
  const UserListItem(this.user, {super.key, this.onTap, this.dense = false});

  final User user;
  final VoidCallback? onTap;
  final bool dense;

  @override
  Widget build(BuildContext context) {
    return RoundedContainer(
      heroTag: 'user_${user.id}',
      innerPadding: EdgeInsets.zero,
      onTap: onTap,
      child: ListTile(
        leading: CircleAvatar(
          radius: dense ? $CircleAvatar.s : $CircleAvatar.m,
          backgroundColor: user.admin ? context.theme.colorScheme.secondary : context.theme.primaryColor,
          child: Text(
            user.initials,
            style: TextStyle(fontSize: dense ? $Font.s : $Font.m),
          ),
        ),
        title: Row(
          children: [
            AutoSizeText(
              '${user.firstName} ',
              style: context.textTheme.titleMedium,
              maxLines: 1,
            ),
            Expanded(
              child: AutoSizeText(
                user.lastName,
                style: context.textTheme.titleMedium,
                maxLines: 1,
              ),
            ),
          ],
        ),
        subtitle: !dense
            ? AutoSizeText(
                user.email,
                style: context.textTheme.labelMedium,
                maxLines: 1,
              )
            : null,
      ),
    );
  }
}
