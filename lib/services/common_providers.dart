import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:isar/isar.dart';

/// A property that defines the riverpod's provider to get our DB instance
/// Ok here it only throws an exception to ensure we didn't forget to initialize it in our main.dart file
final isarProvider = Provider<Isar>((_) {
  throw UnimplementedError();
});
