import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_scaffold_riverpod_v2/config/theme/sizes.dart';
import 'package:flutter_scaffold_riverpod_v2/config/theme/theme.dart';
import 'package:flutter_scaffold_riverpod_v2/config/translations/gen/l10n.dart';

/// An extension that provides useful methods (or not)
extension ContextUtils on BuildContext {
  // TRANSLATION
  Translation get translation => Translation.of(this);

  // THEME
  ThemeData get theme => Theme.of(this);
  TextTheme get textTheme => theme.textTheme;
  ColorScheme get colors => theme.colorScheme;
  Color get primaryColor => colors.primary;
  Color get secondaryColor => colors.secondary;

  // SIZES
  MediaQueryData get mediaQuery => MediaQuery.of(this);
  Size get viewSize => MediaQuery.of(this).size;
  EdgeInsets get viewInsets => MediaQuery.of(this).viewInsets;
  EdgeInsets get viewPadding => MediaQuery.of(this).viewPadding;
  Size get screenSize => Size(viewSize.width + viewPadding.horizontal + viewInsets.horizontal,
      viewSize.height + viewPadding.vertical + viewInsets.vertical);

  // MODAL
  Future<T?> showModal<T>(
    Widget content, {
    bool barrierDismissible = true,
    Future<bool> Function()? onWillPop,
    T? Function(T?)? onPop,
    Color barrierColor = const Color(0x80000000),
  }) async {
    return Navigator.of(this)
        .push<T>(PageRouteBuilder(
            opaque: false,
            barrierDismissible: barrierDismissible,
            barrierLabel: barrierDismissible ? 'Close' : null,
            barrierColor: barrierColor,
            pageBuilder: (BuildContext context, _, __) {
              debugPrint('[BuildContext] Modal displayed: ${content.runtimeType}');
              return onWillPop != null ? WillPopScope(onWillPop: onWillPop, child: content) : content;
            }))
        .then((T? value) {
      debugPrint('[BuildContext] Modal closed: ${content.runtimeType}');
      return onPop?.call(value);
    });
  }

  // SNACKBAR
  void showSuccessSnackBar(
    String text, {
    SnackBarAction? action,
    bool isInfinite = false,
  }) =>
      showCustomSnackBar(
        text: text,
        icon: Icon(Icons.check_circle_outline_rounded, color: theme.colorScheme.positive),
        action: action,
        isInfinite: isInfinite,
      );

  void showInformationSnackBar(
    String text, {
    SnackBarAction? action,
    bool isInfinite = false,
  }) =>
      showCustomSnackBar(
        text: text,
        icon: Icon(Icons.info_outline_rounded, color: theme.colorScheme.informative),
        action: action,
        isInfinite: isInfinite,
      );

  void showWarningSnackBar(
    String text, {
    SnackBarAction? action,
    bool isInfinite = false,
  }) =>
      showCustomSnackBar(
        text: text,
        icon: Icon(Icons.warning_amber_rounded, color: theme.colorScheme.warning),
        action: action,
        isInfinite: isInfinite,
      );

  void showErrorSnackBar(
    String text, {
    SnackBarAction? action,
    bool isInfinite = false,
  }) =>
      showCustomSnackBar(
        text: text,
        icon: Icon(Icons.error_outline_rounded, color: theme.colorScheme.negative),
        action: action,
        isInfinite: isInfinite,
      );

  void showCustomSnackBar({
    required String text,
    required Widget icon,
    maxLines = 3,
    SnackBarAction? action,
    bool isInfinite = false,
  }) =>
      ScaffoldMessenger.of(this).showSnackBar(
        SnackBar(
          content: Row(
            children: [
              icon,
              $Gap.m,
              Expanded(
                child: AutoSizeText(text, style: textTheme.bodyLarge, maxLines: maxLines),
              ),
            ],
          ),
          width: $Screen.size(screenSize.width) > s ? $MaxWidth.m : null,
          margin: $Screen.size(screenSize.width) <= s ? const EdgeInsets.all($Padding.m) : null,
          behavior: SnackBarBehavior.floating,
          backgroundColor: theme.canvasColor,
          action: action,
          duration: isInfinite ? const Duration(days: 365) : const Duration(seconds: 4),
          dismissDirection: isInfinite ? DismissDirection.none : DismissDirection.down,
        ),
      );
}
