import 'package:intl/intl.dart';
import 'package:flutter_scaffold_riverpod_v2/config/translations/gen/l10n.dart';

/// An extension that provides useful methods (or not)
extension DateTimeUtils on DateTime {
  static String toUtc(DateTime date) => date.toUtc().toString();
  String toFrenchDate({
    bool withYear = true,
  }) =>
      withYear ? DateFormat('dd/MM/yy').format(this) : DateFormat('dd/MM').format(this);
  String toFrenchDateTime({
    bool withYear = true,
    bool withSeconds = true,
  }) =>
      withYear
          ? withSeconds
              ? DateFormat('dd/MM/yy ${Translation.current.at} HH:mm:ss').format(this)
              : DateFormat('dd/MM/yy ${Translation.current.at} HH:mm').format(this)
          : withSeconds
              ? DateFormat('dd/MM ${Translation.current.at} HH:mm:ss').format(this)
              : DateFormat('dd/MM ${Translation.current.at} HH:mm').format(this);
}
