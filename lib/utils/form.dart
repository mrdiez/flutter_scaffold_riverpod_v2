import 'package:dartx/dartx.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_scaffold_riverpod_v2/config/translations/gen/l10n.dart';
import 'package:flutter_scaffold_riverpod_v2/utils/string.dart';

/// An extension that provides useful methods (or not)
extension FormBuilderUtils on FormBuilderState {
  void trim() {
    for (final field in fields.values) {
      if (field.value is String) {
        field.setValue(field.value.trim());
      }
    }
  }
}

/// An class that provides useful methods (or not)
class FormValidators {
  static String? firstName(String? value) {
    if (value.isNullOrBlank) return Translation.current.ThisFieldIsRequired;
    return null;
  }

  static String? lastName(String? value) {
    if (value.isNullOrBlank) return Translation.current.ThisFieldIsRequired;
    return null;
  }

  static String? email(String? value) {
    if (value.isNullOrBlank) return Translation.current.ThisFieldIsRequired;
    if (!value!.isEmail) return Translation.current.NotValidEmail;
    return null;
  }

  static String? username(String? value) {
    if (value.isNullOrBlank) return Translation.current.ThisFieldIsRequired;
    if (value!.length < 4) return Translation.current.nbCharactersMinimum(4);
    return null;
  }

  static String? password(String? value) {
    if (value.isNullOrBlank) return Translation.current.ThisFieldIsRequired;
    if (value!.length < 4) return Translation.current.nbCharactersMinimum(4);
    return null;
  }
}
