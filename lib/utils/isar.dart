import 'package:isar/isar.dart';
import 'package:flutter_scaffold_riverpod_v2/abstract/interfaces/isar_model.dart';

/// An extension that provides useful methods (or not)
extension IsarModelListUtils on List<IsarModel> {
  List<int> get ids => map((model) => model.id).toList();
}

/// An extension that provides useful methods (or not)
extension IsarModelLinksUtils on IsarLinks<IsarModel> {
  List<int> get ids => map((model) => model.id).toList();
}
