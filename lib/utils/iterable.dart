import 'package:dartx/dartx.dart';

/// An extension that provides useful methods (or not)
extension ListUtils on List {
  void addAllNotContained(List<dynamic> entries) => addAll(entries.where((e) => !contains(e)));
}

/// An extension that provides useful methods (or not)
extension IterableUtils<T> on Iterable<T> {
  List<T> clone() => [...this].toList();
  T get lastNotNull => [...this].where((e) => e != null).last;
  List<T> get withoutNull => mapNotNull((e) => e).toList();
}

/// An extension that provides useful methods (or not)
extension IterableNullableUtils on Iterable? {
  bool get isNotNullOrEmpty => this != null && this!.isNotEmpty;
  bool get isNullOrEmpty => !isNotNullOrEmpty;
}
