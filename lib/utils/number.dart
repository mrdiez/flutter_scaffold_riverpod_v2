import 'package:flutter_scaffold_riverpod_v2/utils/string.dart';

/// An extension that provides useful methods (or not)
extension IntUtils on int {
  String addZeroPrefixUnder10() => this > 0 && this < 10 ? '0$this' : toString();
  String toHex({bool leadingHash = true}) => '${leadingHash ? '#' : ''}${toRadixString(16).toUpperCase()}';
}

/// An extension that provides useful methods (or not)
extension DoubleUtils on double {
  String limitDecimals([int i = 1]) {
    return toStringAsFixed(i).removeLastZeros();
  }
}
