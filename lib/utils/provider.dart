import 'package:flutter_riverpod/flutter_riverpod.dart';

/// An extension that provides useful methods (or not)
extension ProviderUtils<State> on ProviderBase<State> {
  ProviderListenable<Selected> multiSelect<Selected>(
    List<dynamic> Function(State value) selector,
  ) {
    return select(
      (state) => selector(state).map((s) {
        if (s is List) return s.fold('', (previous, next) => '$previous${next.hashCode}');
        return s.hashCode;
      }).toString() as Selected,
    );
  }
}
