import 'dart:async';

import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

/// An extension that provides useful methods (or not)
extension RouteUtils<State> on GoRoute {
  GoRoute copyWith({
    String? path,
    String? name,
    Widget Function(BuildContext context, GoRouterState state)? builder,
    Page<dynamic> Function(BuildContext context, GoRouterState state)? pageBuilder,
    GlobalKey<NavigatorState>? parentNavigatorKey,
    FutureOr<String?> Function(BuildContext context, GoRouterState state)? redirect,
    List<RouteBase>? routes,
  }) =>
      GoRoute(
        name: name ?? this.name,
        path: path ?? this.path,
        redirect: redirect ?? this.redirect,
        builder: builder ?? this.builder,
        pageBuilder: pageBuilder ?? this.pageBuilder,
        parentNavigatorKey: parentNavigatorKey ?? this.parentNavigatorKey,
        routes: routes ?? this.routes,
      );
}
