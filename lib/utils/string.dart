import 'package:dartx/dartx.dart';
import 'package:diacritic/diacritic.dart' as diacritic;
import 'package:fzregex/fzregex.dart';
import 'package:fzregex/utils/pattern.dart';
import 'package:path/path.dart' as p;

/// An extension that provides useful methods (or not)
extension StringUtils on String {
  bool get isValidName => RegExp(r"^[\p{L} ,.'-]*$", caseSensitive: false, unicode: true, dotAll: true).hasMatch(this);
  bool get isValidUrl {
    if (isEmpty || isBlank) return false;
    final uri = Uri.tryParse(this);
    return uri != null && uri.hasAbsolutePath && uri.scheme.startsWith('http');
  }

  double? parseAsDouble() => double.tryParse(replaceAll(',', '.'));

  String removeDiacritics() => diacritic.removeDiacritics(this);

  String clean() => toLowerCase().removeDiacritics();

  List<String> splitWords() => replaceAll(r'[^\p{IsAlphabetic}]', ' ').split(' ');

  List<String> cleanAndSplit() => clean().splitWords();

  bool get isEmail => Fzregex.hasMatch(this, FzPattern.email);
  bool get isPhone => Fzregex.hasMatch(this, FzPattern.phone);
  bool get isPostalCode => Fzregex.hasMatch(this, FzPattern.postalCode);
  bool get isValidFileName => Fzregex.hasMatch(this, r'^[a-zA-Z0-9_-]+$');

  String getAllAfterFirst(String target) {
    if (!contains(target)) return this;
    return substring(indexOf(target) + target.length);
  }

  String getAllAfterLast(String target) {
    if (!contains(target)) return this;
    return substring(lastIndexOf(target) + target.length);
  }

  List<String> splitAsPath() => p.split(this);

  String removeLastCharacter() => substring(0, length - 1);

  String removeLastZeros() {
    String result = this;
    if ((endsWith('0') && contains('.')) || endsWith('.') || endsWith(',') || endsWith(' ')) {
      result = result.removeLastCharacter();
      return result.removeLastZeros();
    } else {
      return result;
    }
  }

  List<String> searchWords(List<String> searchedWords) {
    List<String> foundWords = [];
    final words = cleanAndSplit();
    if (words.join().contains(searchedWords.join())) return searchedWords;
    for (final w in words) {
      for (final sw in searchedWords) {
        final result = w.contains(sw);
        if (result && !foundWords.contains(sw)) foundWords.add(sw);
      }
    }
    return foundWords;
  }
}
