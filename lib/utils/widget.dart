import 'package:flutter/material.dart';

/// An extension that provides useful methods (or not)
extension WidgetUtils on Widget {
  Widget wrapWithHeroIfTagNotNull(String? tag) => tag != null ? Hero(tag: tag, child: this) : this;
}
