import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_scaffold_riverpod_v2/config/theme/sizes.dart';

/// A simple button widget
class CommonButton extends StatelessWidget {
  const CommonButton({super.key, required this.text, this.leading, this.onTap});

  final VoidCallback? onTap;
  final String text;
  final Widget? leading;

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: onTap,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          if (leading != null) ...[leading!, $Gap.s],
          Flexible(child: AutoSizeText(text, maxLines: 1)),
        ],
      ),
    );
  }
}
