import 'package:auto_size_text/auto_size_text.dart';
import 'package:dartx/dartx.dart';
import 'package:flutter/material.dart';
import 'package:flutter_scaffold_riverpod_v2/config/theme/sizes.dart';
import 'package:flutter_scaffold_riverpod_v2/widgets/layout/rounded_container.dart';

/// A simple icon button widget
class CommonIconButton extends StatelessWidget {
  const CommonIconButton({super.key, this.text, required this.icon, this.onTap});

  final VoidCallback? onTap;
  final IconData icon;
  final String? text;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        RoundedContainer(
          onTap: onTap,
          child: Icon(icon, size: $Icon.m),
        ),
        if (text.isNotNullOrBlank)
          Padding(
            padding: const EdgeInsets.only(top: $Padding.xs),
            child: AutoSizeText(text!, maxLines: 1),
          ),
      ],
    );
  }
}
