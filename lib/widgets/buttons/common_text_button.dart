import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

/// A simple link widget
class CommonTextButton extends StatelessWidget {
  const CommonTextButton({super.key, required this.text, this.onTap});

  final VoidCallback? onTap;
  final String text;

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: onTap,
      child: AutoSizeText(text, maxLines: 1),
    );
  }
}
