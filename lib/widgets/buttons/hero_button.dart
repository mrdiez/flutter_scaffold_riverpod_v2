import 'package:flutter/material.dart';
import 'package:flutter_scaffold_riverpod_v2/widgets/buttons/common_button.dart';

/// A simple button widget contained in an Hero widget
class HeroButton extends CommonButton {
  const HeroButton({super.key, required super.text, super.leading, super.onTap});

  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: text,
      child: super.build(context),
    );
  }
}
