import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_scaffold_riverpod_v2/widgets/misc/shake_on_trigger.dart';

/// A radio button list input able to animate itself on validation error
class AnimatedRadioGroup<T> extends StatefulWidget {
  const AnimatedRadioGroup({
    super.key,
    required this.valuesByLabel,
    this.name,
    this.labelText,
    this.initialValue,
    this.onChanged,
    this.onSaved,
    this.validator,
    this.decoration,
  });

  final String? name;
  final T? initialValue;
  final String? labelText;
  final Map<String, T> valuesByLabel;
  final Function(dynamic)? onChanged;
  final Function(dynamic)? onSaved;
  final String? Function(dynamic)? validator;
  final InputDecoration? decoration;

  @override
  State<AnimatedRadioGroup> createState() => AnimatedRadioGroupState<T>();
}

class AnimatedRadioGroupState<T> extends State<AnimatedRadioGroup> {
  final shakeKey = GlobalKey<ShakeOnTriggerState>();
  final radioGroupKey = GlobalKey<FormBuilderFieldState>();
  late final List<FormBuilderFieldOption<T?>> radios;

  T? get value => radioGroupKey.currentState?.value as T?;

  @override
  void initState() {
    super.initState();
    radios = widget.valuesByLabel.entries
        .map((entry) => FormBuilderFieldOption<T>(
              value: entry.value,
              child: Text(widget.valuesByLabel[entry.key]),
            ))
        .toList();
  }

  @override
  Widget build(BuildContext context) {
    return ShakeOnTrigger(
      key: shakeKey,
      child: FormBuilderRadioGroup<T?>(
        key: radioGroupKey,
        name: widget.name ?? '',
        initialValue: widget.initialValue,
        wrapAlignment: WrapAlignment.spaceAround,
        decoration: widget.decoration ??
            const InputDecoration(
              border: InputBorder.none,
              enabledBorder: InputBorder.none,
              disabledBorder: InputBorder.none,
              errorBorder: InputBorder.none,
              focusedBorder: InputBorder.none,
              focusedErrorBorder: InputBorder.none,
            ),
        onChanged: widget.onChanged,
        onSaved: widget.onSaved,
        validator: (value) {
          final error = widget.validator?.call(value);
          if (error != null) shakeKey.currentState?.toggle();
          return error;
        },
        options: radios,
      ),
    );
  }
}
