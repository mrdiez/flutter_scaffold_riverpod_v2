import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_scaffold_riverpod_v2/widgets/misc/shake_on_trigger.dart';

/// A text input able to animate itself on validation error
class AnimatedTextInput extends StatefulWidget {
  const AnimatedTextInput({
    super.key,
    this.name,
    this.style,
    this.controller,
    this.focusNode,
    required this.validator,
    this.onChanged,
    this.onSaved,
    this.onFieldSubmitted,
    this.decoration,
    this.obscureText = false,
    this.keyboard,
  });

  final String? name;
  final TextStyle? style;
  final TextEditingController? controller;
  final FocusNode? focusNode;
  final FormFieldValidator<String?> validator;
  final ValueChanged<String?>? onChanged;
  final ValueChanged<String?>? onSaved;
  final ValueChanged<String?>? onFieldSubmitted;
  final InputDecoration? decoration;
  final bool obscureText;
  final TextInputType? keyboard;

  @override
  State<AnimatedTextInput> createState() => _AnimatedTextInputState();
}

class _AnimatedTextInputState extends State<AnimatedTextInput> {
  final shakeKey = GlobalKey<ShakeOnTriggerState>();

  @override
  Widget build(BuildContext context) {
    return ShakeOnTrigger(
      key: shakeKey,
      child: FormBuilderTextField(
        // The name attribute must be unique
        name: widget.name ?? widget.decoration?.labelText ?? widget.decoration?.hintText ?? '',
        style: widget.style,
        controller: widget.controller,
        focusNode: widget.focusNode,
        obscureText: widget.obscureText,
        validator: (value) {
          final error = widget.validator(value);
          if (error != null) shakeKey.currentState?.toggle();
          return error;
        },
        keyboardType: widget.keyboard,
        onChanged: widget.onChanged,
        onSaved: widget.onSaved,
        onSubmitted: widget.onFieldSubmitted,
        decoration: widget.decoration ?? const InputDecoration(),
      ),
    );
  }
}
