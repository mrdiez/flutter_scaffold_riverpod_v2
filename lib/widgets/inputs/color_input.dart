import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:go_router/go_router.dart';
import 'package:flutter_scaffold_riverpod_v2/config/theme/sizes.dart';

import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_scaffold_riverpod_v2/utils/context.dart';
import 'package:flutter_scaffold_riverpod_v2/widgets/buttons/common_button.dart';
import 'package:flutter_scaffold_riverpod_v2/widgets/buttons/common_text_button.dart';
import 'package:flutter_scaffold_riverpod_v2/widgets/modals/common_modal.dart';

/// A color input
class ColorInput extends FormBuilderField<Color> {
  static const defaultValue = Colors.white;

  final String labelText;
  late final ColorInputController? controller;

  ColorInput({
    super.key,
    void Function(Color)? onChanged,
    void Function(Color)? onSaved,
    this.controller,
    required this.labelText,
    super.enabled = true,
  }) : super(
          name: labelText,
          onChanged: onChanged != null ? (value) => onChanged(value ?? defaultValue) : null,
          onSaved: onSaved != null ? (value) => onSaved(value ?? defaultValue) : null,
          builder: (FormFieldState<Color?> field) {
            final state = field as _FormBuilderColorInputState;
            return InkWell(
              onTap: state.enabled
                  ? () async {
                      final value = await _pickColor(
                        context: field.context,
                        defaultValue: state.value ?? ColorInput.defaultValue,
                        heroTag: labelText,
                      );
                      controller?.value = value ?? defaultValue;
                    }
                  : null,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  AutoSizeText(
                    labelText,
                    maxLines: 1,
                    style: state.enabled
                        ? field.context.theme.textTheme.bodyMedium
                        : field.context.theme.textTheme.bodyMedium?.copyWith(color: field.context.theme.disabledColor),
                  ),
                  $Gap.s,
                  Hero(
                    tag: labelText,
                    child: CircleAvatar(backgroundColor: state.value ?? ColorInput.defaultValue),
                  ),
                ],
              ),
            );
          },
        );

  static Future<Color?> _pickColor({
    required BuildContext context,
    required Color defaultValue,
    required String heroTag,
  }) {
    Color value = defaultValue;
    return context.showModal<Color>(
      CommonModal(
        heroTag: heroTag,
        child: Column(
          children: [
            ColorPicker(
              pickerColor: defaultValue,
              pickerAreaBorderRadius: const BorderRadius.all($Radius.xs),
              pickerAreaHeightPercent: 0.5,
              colorPickerWidth: $MaxWidth.xs,
              onColorChanged: (color) => value = color,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Flexible(
                  child: CommonTextButton(
                    text: context.translation.Cancel,
                    onTap: () => context.pop(defaultValue),
                  ),
                ),
                Flexible(
                  child: CommonButton(
                    text: context.translation.OK,
                    onTap: () => context.pop(value),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
      onPop: (color) => color ?? defaultValue,
    );
  }

  @override
  FormBuilderFieldState<ColorInput, Color> createState() => _FormBuilderColorInputState();
}

class _FormBuilderColorInputState extends FormBuilderFieldState<ColorInput, Color> {
  late final ColorInputController _effectiveController;

  void _handleControllerChanged() {
    if (widget.controller!.value != value) {
      didChange(_effectiveController.value);
    }
  }

  @override
  void initState() {
    super.initState();
    _effectiveController = widget.controller ?? ColorInputController(value ?? ColorInput.defaultValue);
    setValue(_effectiveController.value);
    widget.controller?.addListener(_handleControllerChanged);
  }

  @override
  void dispose() {
    widget.controller?.removeListener(_handleControllerChanged);
    super.dispose();
  }
}

class ColorInputController extends ValueNotifier<Color> {
  ColorInputController([super.value = ColorInput.defaultValue]);
}
