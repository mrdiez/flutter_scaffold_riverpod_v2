import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_scaffold_riverpod_v2/config/theme/sizes.dart';

import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_scaffold_riverpod_v2/utils/context.dart';

/// A checkbox input
class LabelledCheckbox extends FormBuilderField<bool> {
  static const defaultValue = false;

  final String labelText;
  late final LabelledCheckboxController? controller;

  LabelledCheckbox({
    super.key,
    void Function(bool)? onChanged,
    void Function(bool)? onSaved,
    this.controller,
    required this.labelText,
    super.enabled = true,
  }) : super(
          name: labelText,
          onChanged: onChanged != null ? (value) => onChanged(value ?? defaultValue) : null,
          onSaved: onSaved != null ? (value) => onSaved(value ?? defaultValue) : null,
          builder: (FormFieldState<bool?> field) {
            final state = field as _FormBuilderCheckboxState;
            return Row(
              children: [
                SizedBox(
                  height: $Icon.s,
                  width: $Icon.s,
                  child: Checkbox(
                    value: state.value ?? defaultValue,
                    onChanged: state.enabled
                        ? (value) {
                            controller?.value = value ?? defaultValue;
                            state.didChange(value ?? defaultValue);
                          }
                        : null,
                  ),
                ),
                $Gap.s,
                Flexible(
                  child: InkWell(
                    onTap: state.enabled ? () => state.didChange(!(state.value ?? defaultValue)) : null,
                    child: AutoSizeText(
                      labelText,
                      maxLines: 1,
                      style: state.enabled
                          ? field.context.theme.textTheme.bodyMedium
                          : field.context.theme.textTheme.bodyMedium
                              ?.copyWith(color: field.context.theme.disabledColor),
                    ),
                  ),
                ),
              ],
            );
          },
        );

  @override
  FormBuilderFieldState<LabelledCheckbox, bool> createState() => _FormBuilderCheckboxState();
}

class _FormBuilderCheckboxState extends FormBuilderFieldState<LabelledCheckbox, bool> {
  late final LabelledCheckboxController _effectiveController;

  void _handleControllerChanged() {
    if (widget.controller!.value != value) {
      didChange(_effectiveController.value);
    }
  }

  @override
  void initState() {
    super.initState();
    _effectiveController = widget.controller ?? LabelledCheckboxController(value ?? LabelledCheckbox.defaultValue);
    setValue(_effectiveController.value);
    widget.controller?.addListener(_handleControllerChanged);
  }

  @override
  void dispose() {
    widget.controller?.removeListener(_handleControllerChanged);
    super.dispose();
  }
}

class LabelledCheckboxController extends ValueNotifier<bool> {
  LabelledCheckboxController([super.value = false]);
}
