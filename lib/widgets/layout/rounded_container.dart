import 'package:flutter/material.dart';
import 'package:flutter_scaffold_riverpod_v2/config/theme/sizes.dart';
import 'package:flutter_scaffold_riverpod_v2/utils/context.dart';
import 'package:flutter_scaffold_riverpod_v2/utils/widget.dart';

/// A common rounded container
class RoundedContainer extends StatelessWidget {
  const RoundedContainer({
    Key? key,
    this.child,
    this.heroTag,
    this.color,
    this.innerPadding,
    this.borderRadius,
    this.boxShadow,
    this.onTap,
    this.onLongPress,
  }) : super(key: key);

  final Widget? child;
  final String? heroTag;
  final Color? color;
  final EdgeInsets? innerPadding;
  final BorderRadius? borderRadius;
  final List<BoxShadow>? boxShadow;
  final VoidCallback? onTap;
  final VoidCallback? onLongPress;

  @override
  Widget build(BuildContext context) {
    Widget content = Padding(
      padding: innerPadding ?? const EdgeInsets.all($Padding.s),
      child: child ?? Container(),
    );
    if (onTap != null || onLongPress != null) {
      content = InkWell(
        onTap: onTap,
        onLongPress: onLongPress,
        borderRadius: borderRadius ?? const BorderRadius.all($Radius.xs),
        child: content,
      );
    }
    return Material(
      color: color ?? context.theme.canvasColor,
      borderRadius: borderRadius ?? const BorderRadius.all($Radius.xs),
      child: content,
    ).wrapWithHeroIfTagNotNull(heroTag);
  }
}
