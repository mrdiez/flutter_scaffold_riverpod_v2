import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_scaffold_riverpod_v2/abstract/mixins/custom_animation.dart';
import 'package:flutter_scaffold_riverpod_v2/config/theme/sizes.dart';
import 'package:vector_math/vector_math_64.dart';

/// A animation widget able to shake itself when triggered
class ShakeOnTrigger extends StatefulWidget {
  const ShakeOnTrigger({Key? key, required this.child}) : super(key: key);

  final Widget child;

  @override
  ShakeOnTriggerState createState() => ShakeOnTriggerState();
}

class ShakeOnTriggerState extends State<ShakeOnTrigger> with SingleTickerProviderStateMixin, CustomAnimationMixin {
  late Matrix4 _translation = _shake();

  @override
  final duration = $Duration.veryLonger;

  @override
  void initState() {
    super.initState();
    animation.addListener(() => setState(() => _translation = _shake()));
  }

  Matrix4 _shake() {
    var progress = animation.value;
    var offset = sin(progress * pi * 10.0);
    return Matrix4.translation(Vector3(offset * 4, 0.0, 0.0));
  }

  @override
  Widget build(BuildContext context) => Transform(
        transform: _translation,
        child: widget.child,
      );
}
