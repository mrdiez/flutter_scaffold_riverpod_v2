import 'package:flutter/material.dart';
import 'package:flutter_scaffold_riverpod_v2/config/theme/sizes.dart';
import 'package:flutter_scaffold_riverpod_v2/config/theme/theme.dart';
import 'package:flutter_scaffold_riverpod_v2/utils/context.dart';
import 'package:shimmer/shimmer.dart';

/// A shimmer loader widget
class ShimmerLoading extends StatelessWidget {
  const ShimmerLoading({
    super.key,
    this.height,
    this.width,
  });

  final double? height;
  final double? width;

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      baseColor: context.colors.shimmerBase,
      highlightColor: context.colors.shimmerHighlight,
      child: Container(
        height: height ?? $ShimmerHeight.m,
        width: width ?? double.infinity,
        color: context.theme.canvasColor,
      ),
    );
  }
}
