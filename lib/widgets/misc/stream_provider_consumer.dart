import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_scaffold_riverpod_v2/utils/context.dart';
import 'package:flutter_scaffold_riverpod_v2/widgets/misc/shimmer_loading.dart';

/// A Stream Consumer widget that display a default Shimmer loader when loading
/// and a default error message on error
class StreamProviderConsumer<T> extends StatelessWidget {
  const StreamProviderConsumer(
    this.provider, {
    super.key,
    this.loadingBuilder,
    this.errorBuilder,
    required this.builder,
  });

  final AutoDisposeStreamProvider<T> provider;
  final Widget Function(BuildContext)? loadingBuilder;
  final Widget Function(BuildContext, Object)? errorBuilder;
  final Widget Function(BuildContext, T) builder;

  @override
  Widget build(BuildContext context) => Consumer(builder: (context, ref, child) {
        // We're listening our data's stream
        // This part of the view will be rebuilded each time we'll receive a stream's entry
        final state = ref.watch(provider);
        return state.when(
          // The stream is still loading
          loading: () => loadingBuilder?.call(context) ?? const ShimmerLoading(),
          // The stream has thrown an error
          error: (e, stackTrace) {
            debugPrint('[$runtimeType] ERROR\n$e\n$stackTrace');
            return errorBuilder?.call(context, e) ?? AutoSizeText(context.translation.UnknownError, maxLines: 1);
          },
          // The stream returned some data
          data: (state) => builder(context, state),
        );
      });
}
