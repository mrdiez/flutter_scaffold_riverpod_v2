import 'package:dartx/dartx.dart';
import 'package:flutter/material.dart';
import 'package:flutter_scaffold_riverpod_v2/config/theme/sizes.dart';
import 'package:flutter_scaffold_riverpod_v2/utils/context.dart';
import 'package:flutter_scaffold_riverpod_v2/widgets/layout/rounded_container.dart';

/// A base modal widget
class CommonModal extends StatelessWidget {
  const CommonModal(
      {super.key, this.title, required this.child, this.heroTag, this.horizontalAxisSize = MainAxisSize.max});

  final String? title;
  final String? heroTag;
  final Widget child;
  final MainAxisSize horizontalAxisSize;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: $Padding.m),
        child: LayoutBuilder(builder: (context, constraints) {
          return RoundedContainer(
            heroTag: heroTag,
            color: context.theme.snackBarTheme.backgroundColor?.withOpacity(0.95),
            child: SingleChildScrollView(
              physics: const NeverScrollableScrollPhysics(),
              child: SingleChildScrollView(
                physics: const NeverScrollableScrollPhysics(),
                scrollDirection: Axis.horizontal,
                child: Column(
                  children: [
                    if (title.isNotNullOrBlank)
                      Padding(
                        padding: const EdgeInsets.only(bottom: $Padding.s),
                        child: Text(title!, style: context.textTheme.labelLarge),
                      ),
                    ConstrainedBox(
                      constraints: BoxConstraints(maxWidth: $MaxWidth.getMaxUnder(constraints.maxWidth - $Padding.m)),
                      child: child,
                    ),
                  ],
                ),
              ),
            ),
          );
        }),
      ),
    );
  }
}
