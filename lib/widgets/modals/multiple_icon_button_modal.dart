import 'package:dartx/dartx.dart';
import 'package:flutter/material.dart';
import 'package:flutter_scaffold_riverpod_v2/config/theme/sizes.dart';
import 'package:flutter_scaffold_riverpod_v2/widgets/buttons/common_icon_button.dart';
import 'package:flutter_scaffold_riverpod_v2/widgets/modals/common_modal.dart';

class MultipleIconButtonModal extends StatelessWidget {
  const MultipleIconButtonModal({super.key, this.title, required this.buttons});

  final String? title;
  final List<CommonIconButton> buttons;

  @override
  Widget build(BuildContext context) {
    return CommonModal(
      horizontalAxisSize: MainAxisSize.min,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: buttons
            .mapIndexed((i, button) {
              return [
                button,
                if (i < buttons.length - 1) $Gap.m,
              ];
            })
            .expand((elements) => elements)
            .toList(),
      ),
    );
  }
}
