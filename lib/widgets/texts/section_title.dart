import 'package:auto_size_text/auto_size_text.dart';
import 'package:dartx/dartx.dart';
import 'package:flutter/material.dart';
import 'package:flutter_scaffold_riverpod_v2/config/theme/sizes.dart';
import 'package:flutter_scaffold_riverpod_v2/utils/context.dart';
import 'package:flutter_scaffold_riverpod_v2/utils/widget.dart';

class SectionTitle extends StatelessWidget {
  const SectionTitle({
    super.key,
    this.title,
    this.text,
    this.dividerColor,
    this.textAsHeroTag = false,
  }) : assert((title != null && text == null) || (title == null && text != null));

  final Widget? title;
  final String? text;
  final bool textAsHeroTag;
  final Color? dividerColor;

  final dividerHeight = $DividerHeight.m;
  final dividerPaddingTop = $Padding.xxs;

  TextStyle getTextTheme(BuildContext context) => context.textTheme.headlineMedium!;
  double getTextHeight(BuildContext context) =>
      (getTextTheme(context).fontSize ?? 0) * (getTextTheme(context).height ?? 1);
  double getDividerWidth(BuildContext context) => getTextHeight(context) * 3;
  double getHeight(BuildContext context) => getTextHeight(context) + dividerHeight + dividerPaddingTop;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: getHeight(context),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if (title != null) title!,
          if (text.isNotNullOrBlank) AutoSizeText(text!, maxLines: 1, style: getTextTheme(context)),
          Padding(
            padding: EdgeInsets.only(top: dividerPaddingTop),
            child: Container(
              height: dividerHeight,
              width: getDividerWidth(context),
              decoration: BoxDecoration(
                color: dividerColor ?? context.secondaryColor,
                borderRadius: const BorderRadius.all($Radius.l),
              ),
            ),
          ),
        ],
      ),
    ).wrapWithHeroIfTagNotNull(textAsHeroTag ? text : null);
  }
}
