import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_scaffold_riverpod_v2/modules/home/home_view.dart';

void main() {
  testWidgets('Home test', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget(const MaterialApp(home: HomeView()));

    // Verify that the title is displayed
    expect(find.text('Hello world'), findsOneWidget);
  });
}
